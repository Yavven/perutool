﻿using PeruBackTool.BLL;
using PeruBackTool.UI;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using YWCommonUtil;

namespace PeruBackTool
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public static string AppPath;
        public static bool IsRoot;
        public static string LoginLocation;
        public static MessageBll GlobalPeruMessageBll;
        public static PeruMessageHandler GlobalPeruMessageHandler;
        public static PeruTcpClient TcpClient;
        public static Thread TcpClientThread;
        public static LoginWindow MyLoginWindow;
        public static MainWindow GlobalMainWindow;
        public static ReportWindow ProfitReportWindow;
        public static ReportWindow FinanceReportWindow;
        public static ReportWindow BalanceRecordWindow;
        public static ReportWindow FinanceRecordWindow;
        public static ReportWindow CaisherLogWindow;
        public static ReportWindow CardGameRecordWindow;
        public static ReportWindow ComputerRecordWindow;
        public static AllGameConfigWindow MyAllGameConfigWindow;
        public static CardBalanceFilterWindow MyCardBalanceFilterWindow;
        public static List<AdminBack> Locations;
        public static string LastSendLocation;
        public static Dictionary<int, string> AccountTypeNames;
        public static Dictionary<int, string> GameNames;
        public static Dictionary<int, string> TransferTypeName;
        public static Dictionary<int, string> BalanceLogTypeName;

        public App()
        {
            string serverHostnameKey = "ServerHostname";
            string serverPortKey = "ServerPort";

            if(IsDebug)
            {
                serverHostnameKey = "Test" + serverHostnameKey;
                serverPortKey = "Test" + serverPortKey;
            }

            AppPath = AppDomain.CurrentDomain.BaseDirectory;
            string serverHostname = Convert.ToString(ConfigUtil.GetAppSettingsValue(serverHostnameKey));
            int serverHPort = Convert.ToInt32(ConfigUtil.GetAppSettingsValue(serverPortKey));
            GlobalPeruMessageHandler = new PeruMessageHandler();
            GlobalPeruMessageBll = new MessageBll();
            GlobalPeruMessageHandler.SetMessageBLL(GlobalPeruMessageBll);
            TcpClient = new PeruTcpClient();
            TcpClient.SetHostnameAndPort(serverHostname, serverHPort);
            TcpClient.MessageHandler = GlobalPeruMessageHandler;
            TcpClientThread = new Thread(TcpClient.Connect);
            TcpClientThread.IsBackground = true;
            TcpClientThread.Start();
            AccountTypeNames = new Dictionary<int, string>();
            AccountTypeNames.Add(AdminTypes.ADMIN_CARD, "Card Admin");
            AccountTypeNames.Add(AdminTypes.FINANCE, "Cashier");
            AccountTypeNames.Add(AdminTypes.PLAYER_COMPUTER, "Computer");
            AccountTypeNames.Add(AdminTypes.BACK, "Location");
            TransferTypeName = new Dictionary<int, string>();
            TransferTypeName.Add(1, "Deposit");
            TransferTypeName.Add(2, "Withdraw");
            TransferTypeName.Add(3, "Game");

            BalanceLogTypeName = new Dictionary<int, string>();
            BalanceLogTypeName.Add(1, "AddMoney");
            BalanceLogTypeName.Add(2, "PayMoney");
            BalanceLogTypeName.Add(3, "PlayGame");

            GameNames = new Dictionary<int, string>();
            GameNames.Add(1, "Poker Comodin");
            GameNames.Add(2, "2 Comodin");
            GameNames.Add(3, "Classic Bonus");
            GameNames.Add(4, "Poker Comodin");
            GameNames.Add(5, "Four Deck Draw");
            GameNames.Add(11, "Siete Rojo");
            GameNames.Add(12, "Gran Tiburon");
            GameNames.Add(13, "Tesoro Inca");
            GameNames.Add(30, "Keno");
            GameNames.Add(31, "Power Keno");
            GameNames.Add(32, "Highroller Keno");
            GameNames.Add(33, "Alley Cat Keno");
            GameNames.Add(34, "Campanitas");
            GameNames.Add(35, "Polly's Gold");
            GameNames.Add(36, "Keno Wild");
            GameNames.Add(37, "Abejitas");
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)                 
                  return true;
                #else                
                  return false;
                #endif
            }
        }
    }
}
