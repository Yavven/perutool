﻿using PeruBackTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruBackTool.UI
{
    /// <summary>
    /// ReportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AllGameConfigWindow
    {
        DataTable dataTable;

        public void OnMessageResponse(object responseObject)
        {
            dataTable.Clear();
            GetAllGameConfigResponse response = responseObject as GetAllGameConfigResponse;

            foreach (SampleGameConfig config in response.Configs)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["Game"] = App.GameNames[config.GameId];
                dataRow["Game Status"] = config.OpenStatus == StatusTypes.DISABLE ? "Close" : "Open";
                dataRow["Recharge Rate"] = config.RechargeRate;
                dataRow["Tax Rate"] = config.TaxRate;
                dataRow["Max Bet"] = config.MaxBet;
                dataRow["Max Paid"] = config.MaxPaid;
                dataRow["Paytable"] = config.CurIniName;
                dataTable.Rows.Add(dataRow);
            }

            MyDataGrid.ItemsSource = dataTable.AsDataView();
        }

        public AllGameConfigWindow()
        {
            InitializeComponent();

            dataTable = new DataTable();
            dataTable.Columns.Add("Game");
            dataTable.Columns.Add("Game Status");
            dataTable.Columns.Add("Recharge Rate");
            dataTable.Columns.Add("Tax Rate");
            dataTable.Columns.Add("Max Bet");
            dataTable.Columns.Add("Max Paid");
            dataTable.Columns.Add("Paytable");        
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.GetAllGameConfig();
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            Common.ExportExcel(dataTable);
        }
    }
}
