﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeruToolClassLibrary.Response;
using PeruBackTool.BLL;
using YWCommonUtil;

namespace PeruBackTool.UI
{
    /// <summary>
    /// CompanyInfoUi.xaml 的交互逻辑
    /// </summary>
    public partial class CompanyInfoUi : UserControl
    {
        public void OnMessageResponse(object responseObject)
        {
            CompanyInfoSearchResponse response = responseObject as CompanyInfoSearchResponse;
            NameTextBox.Text = response.Name;
            HowToGetTextBox.Text = response.HowToGet;
            AddressTextBox.Text = response.Address;
            TelphoneTextBox.Text = response.Telphone;
        }

        public CompanyInfoUi()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(NameTextBox.Text))
            {
                MessageBox.Show("Name is empty");
                return;
            }

            if (string.IsNullOrEmpty(HowToGetTextBox.Text))
            {
                MessageBox.Show("How to get is empty");
                return;
            }

            if (string.IsNullOrEmpty(AddressTextBox.Text))
            {
                MessageBox.Show("Address is empty");
                return;
            }

            if (string.IsNullOrEmpty(TelphoneTextBox.Text))
            {
                MessageBox.Show("Telphone is empty");
                return;
            }

            string name = NameTextBox.Text;
            string howToGet = HowToGetTextBox.Text;
            string address = AddressTextBox.Text;
            string telphone = TelphoneTextBox.Text;
            RequestBll.CompanyInfoEdit(name, howToGet, address, telphone);
        }

        private void TelphoneTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsInteger(e.Text))
            {
                e.Handled = true;
            }
        }

        private void CommonTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }

        private void CommonTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}
