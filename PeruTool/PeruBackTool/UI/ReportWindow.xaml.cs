﻿using PeruBackTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruBackTool.UI
{
    /// <summary>
    /// ReportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ReportWindow
    {
        DataTable dataTable;
        int type;

        public void OnMessageResponse(object response)
        {
            dataTable.Clear();
            int sequence = 1;
            int count = 0;

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    ReportProfitResponse profitReportResponse = response as ReportProfitResponse;
                    double totalMoneyPlayed = 0.00;
                    double totalMoneyWon = 0.00;
                    double totalMoneyTaxed = 0.00;
                    double totalProfit = 0.00;
                    double totalTeso = 0.00;

                    foreach (ReportProfit report in profitReportResponse.Reports)
                    {
                        totalMoneyPlayed += report.AllBet;
                        totalMoneyWon += report.AllPaid;
                        totalMoneyTaxed += report.AllTax;
                        totalProfit += (report.AllBet - report.AllPaid + report.AllTax);
                        totalTeso += report.TesoAllBet;

                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Location"] = report.Location;
                        dataRow["Computer"] = report.Computer;
                        dataRow["Money played"] = report.AllBet.ToString("0.00");
                        dataRow["Money won"] = report.AllPaid.ToString("0.00");
                        dataRow["Money taxed"] = report.AllTax.ToString("0.00");
                        dataRow["Profit"] = (report.AllBet - report.AllPaid + report.AllTax).ToString("0.00");
                        dataRow["Teso"] = report.TesoAllBet.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }


                    DataRow totalDataRow = dataTable.NewRow();
                    totalDataRow["Location"] = "Total";
                    totalDataRow["Money played"] = totalMoneyPlayed.ToString("0.00");
                    totalDataRow["Money won"] = totalMoneyWon.ToString("0.00");
                    totalDataRow["Money taxed"] = totalMoneyTaxed.ToString("0.00");
                    totalDataRow["Profit"] = totalProfit.ToString("0.00");
                    totalDataRow["Teso"] = totalTeso.ToString("0.00");
                    dataTable.Rows.Add(totalDataRow);

                    break;

                case ReportTypes.REPORT_FINANCE:
                    FinanceReportSearchResponse financeReportResponse = response as FinanceReportSearchResponse;

                    foreach (FinanceReport report in financeReportResponse.Reports)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Location"] = report.Location;
                        dataRow["Money in"] = report.Deposit.ToString("0.00");
                        dataRow["Money out"] = report.Withdraw.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    break;


                case ReportTypes.RECORD_BALANCE:
                    RecordBalanceResponse balanceRecordResponse = response as RecordBalanceResponse;

                    foreach (BalanceRecord record in balanceRecordResponse.Records)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = sequence++;
                        dataRow["Station"] = record.Operator;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow["Type"] = App.BalanceLogTypeName[record.Type];
                        dataRow["Balance"] = record.Balance.ToString("0.00");
                        dataRow["Amount"] = record.Amount.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.RECORD_FINANCE:
                    FinanceRecordResponse financeRecordResponse = response as FinanceRecordResponse;

                    foreach (FinanceRecord record in financeRecordResponse.Records)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Card"] = record.Card;
                        dataRow["Operator"] = record.Operator;
                        dataRow["Location"] = record.Location;
                        dataRow["Type"] = App.TransferTypeName[record.Type];
                        dataRow["Amount"] = record.Money.ToString("0.00");
                        dataRow["Balance"] = record.Balance.ToString("0.00");
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.RECORD_GAME:
                    GameRecordResponse cardGameReocrds = response as GameRecordResponse;

                    foreach (ReportGameRecord record in cardGameReocrds.Records)
                    {
                        count++;
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = sequence++;
                        dataRow["Computer"] = record.Name;
                        dataRow["Game"] = App.GameNames.ContainsKey(record.GameId) ? App.GameNames[record.GameId] : "Unknow";
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow["Status"] = "Normal";
                        dataRow["Win"] = record.Paid.ToString("0.00");
                        dataRow["Lose"] = record.Bet.ToString("0.00");
                        dataRow["Tax"] = record.Tax.ToString("0.00");
                        dataRow["Balance"] = record.Balance.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    Count.Content = count;

                    break;

                case ReportTypes.COMPUTER_GAME_RECORD:
                    GameRecordResponse computerGameRecordResponse = response as GameRecordResponse;

                    foreach (ReportGameRecord record in computerGameRecordResponse.Records)
                    {
                        count++;
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = sequence++;
                        dataRow["card ID"] = record.Name;
                        dataRow["Game"] = App.GameNames.ContainsKey(record.GameId) ? App.GameNames[record.GameId] : "Unknow";
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow["Status"] = "Normal";
                        dataRow["Win"] = record.Paid.ToString("0.00");
                        dataRow["Lose"] = record.Bet.ToString("0.00");
                        dataRow["Tax"] = record.Tax.ToString("0.00");
                        dataRow["Balance"] = record.Balance.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    Count.Content = count;

                    break;

                case ReportTypes.CAISHER_LOG:
                    FinanceRecordResponse caisherLogResponse = response as FinanceRecordResponse;

                    double deposit = 0.00;
                    double withdraw = 0.00;
                    double balance = 0.00;

                    foreach (FinanceRecord record in caisherLogResponse.Records)
                    {
                        if (record.Type == TransferTypes.DEPOSIT)
                        {
                            deposit += record.Money;
                        }
                        else
                        {
                            withdraw += record.Money;
                        }

                        balance += record.Balance;
                        count++;

                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = sequence++;
                        dataRow["CardID"] = record.Card;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow["Type"] = App.TransferTypeName[record.Type];
                        dataRow["Amount"] = record.Money.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    Deposit.Content = deposit.ToString("0.00");
                    Withdraw.Content = withdraw.ToString("0.00");
                    Balance.Content = (deposit + withdraw).ToString("0.00");
                    Count.Content = count.ToString();

                    break;
            }

            ReportDataGrid.ItemsSource = dataTable.AsDataView();
        }

        public ReportWindow(int reportType)
        {
            InitializeComponent();

            StartDateTimePicker.Value = DateTimeUtil.GetTodayStartDateTime();
            EndDateTimePicker.Value = DateTime.Now;

            dataTable = new DataTable();
            type = reportType;

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    Title = "Computer Report";
                    dataTable.Columns.Add("Location");
                    dataTable.Columns.Add("Computer");
                    dataTable.Columns.Add("Money played");
                    dataTable.Columns.Add("Money won");
                    dataTable.Columns.Add("Money taxed");
                    dataTable.Columns.Add("Profit");
                    dataTable.Columns.Add("Teso");
                    MyStatusBar.Visibility = Visibility.Collapsed;
                    ReportDataGrid.Height += MyStatusBar.Height;
                    break;

                case ReportTypes.REPORT_FINANCE:
                    Title = "Money Report";
                    dataTable.Columns.Add("Location");
                    dataTable.Columns.Add("Money in");
                    dataTable.Columns.Add("Money out");
                    MyStatusBar.Visibility = Visibility.Collapsed;
                    ReportDataGrid.Height += MyStatusBar.Height;
                    break;


                case ReportTypes.RECORD_BALANCE:
                    Title = "Balance Log";
                    PlayerLabel.Content = "Card number:";
                    dataTable.Columns.Add("Sequence");
                    dataTable.Columns.Add("Station");
                    dataTable.Columns.Add("Time");
                    dataTable.Columns.Add("Type");
                    dataTable.Columns.Add("Balance");
                    dataTable.Columns.Add("Amount");
                    MyStatusBar.Visibility = Visibility.Collapsed;
                    ReportDataGrid.Height += MyStatusBar.Height;
                    break;

                case ReportTypes.RECORD_FINANCE:
                    Title = "Finance Record";
                    PlayerLabel.Content = "Card ID";
                    dataTable.Columns.Add("Card");
                    dataTable.Columns.Add("Operator");
                    dataTable.Columns.Add("Location");
                    dataTable.Columns.Add("Type");
                    dataTable.Columns.Add("Amount");
                    dataTable.Columns.Add("Balance");
                    dataTable.Columns.Add("Time");
                    break;

                case ReportTypes.RECORD_GAME:
                    Title = "Game Log";
                    PlayerLabel.Content = "Card";
                    dataTable.Columns.Add("Sequence");
                    dataTable.Columns.Add("Computer");
                    dataTable.Columns.Add("Game");
                    dataTable.Columns.Add("Time");
                    dataTable.Columns.Add("Status");
                    dataTable.Columns.Add("Win");
                    dataTable.Columns.Add("Lose");
                    dataTable.Columns.Add("Tax");
                    dataTable.Columns.Add("Balance");
                    MyStatusBar.Visibility = Visibility.Visible;
                    DepositTile.Visibility = Deposit.Visibility = Visibility.Collapsed;
                    WithdrawTitle.Visibility = Withdraw.Visibility = Visibility.Collapsed;
                    BalanceTitle.Visibility = Balance.Visibility = Visibility.Collapsed;
                    CountTitle.Margin = new Thickness(0, 0, 0, 0);
                    break;

                case ReportTypes.COMPUTER_GAME_RECORD:
                    Title = "Computer Game Log";
                    PlayerLabel.Content = "Computer";
                    dataTable.Columns.Add("Sequence");
                    dataTable.Columns.Add("card ID");
                    dataTable.Columns.Add("Game");
                    dataTable.Columns.Add("Time");
                    dataTable.Columns.Add("Status");
                    dataTable.Columns.Add("Win");
                    dataTable.Columns.Add("Lose");
                    dataTable.Columns.Add("Tax");
                    dataTable.Columns.Add("Balance");
                    MyStatusBar.Visibility = Visibility.Visible;
                    DepositTile.Visibility = Deposit.Visibility = Visibility.Collapsed;
                    WithdrawTitle.Visibility = Withdraw.Visibility = Visibility.Collapsed;
                    BalanceTitle.Visibility = Balance.Visibility = Visibility.Collapsed;
                    CountTitle.Margin = new Thickness(0, 0, 0, 0);
                    break;

                case ReportTypes.CAISHER_LOG:
                    Title = "Caisher Log";
                    PlayerLabel.Content = "Card ID";
                    dataTable.Columns.Add("Sequence");
                    dataTable.Columns.Add("CardId");
                    dataTable.Columns.Add("Time");
                    dataTable.Columns.Add("Type");
                    dataTable.Columns.Add("Amount");
                    MyStatusBar.Visibility = Visibility.Visible;
                    break;
            }

            if (reportType <= ReportTypes.RECORD_FINANCE)
            {
                LocationLabel.Visibility = Visibility.Visible;
                LocationComboBox.Visibility = Visibility.Visible;

                if (App.IsRoot)
                {
                    foreach (AdminBack location in App.Locations)
                    {
                        LocationComboBox.Items.Add(location.Name);
                    }
                }
                else
                {
                    LocationComboBox.Items.Add(App.LoginLocation);
                }

                LocationComboBox.SelectedIndex = 0;
            }
            else
            {
                LocationLabel.Visibility = Visibility.Collapsed;
                LocationComboBox.Visibility = Visibility.Collapsed;
            }

            if (reportType >= ReportTypes.RECORD_FINANCE)
            {
                PlayerLabel.Visibility = Visibility.Visible;
                PlayerTextBox.Visibility = Visibility.Visible;
            }
            else
            {
                PlayerLabel.Visibility = Visibility.Collapsed;
                PlayerTextBox.Visibility = Visibility.Collapsed;
            }

            if (reportType == ReportTypes.CAISHER_LOG && App.IsRoot)
            {
                LocationLabel.Visibility = Visibility.Visible;
                LocationComboBox.Visibility = Visibility.Visible;
                PlayerLabel.Visibility = Visibility.Collapsed;
                PlayerTextBox.Visibility = Visibility.Collapsed;

                foreach (AdminBack location in App.Locations)
                {
                    LocationComboBox.Items.Add(location.Name);
                }
            }

            LocationComboBox.SelectedIndex = 0;
        }

        private void ReportGetButton_Click(object sender, RoutedEventArgs e)
        {
            dataTable.Clear();
            ReportDataGrid.ItemsSource = null;
            string location = string.Empty;
            string player = string.Empty;
            int startTime = DateTimeUtil.ConvertDateTimeToInt(StartDateTimePicker.Value.Value);
            int endTime = DateTimeUtil.ConvertDateTimeToInt(EndDateTimePicker.Value.Value);

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    if (App.IsRoot)
                    {
                        location = LocationComboBox.Text;
                    }

                    RequestBll.ReportProfit(location, startTime, endTime);
                    break;

                case ReportTypes.REPORT_FINANCE:
                    if (App.IsRoot)
                    {
                        location = LocationComboBox.Text;
                    }

                    RequestBll.ReportFinance(location, startTime, endTime);
                    break;

                case ReportTypes.RECORD_BALANCE:
                    player = PlayerTextBox.Text;
                    RequestBll.ReportBalanceChange(player, startTime, endTime);
                    break;

                case ReportTypes.RECORD_GAME:
                    player = PlayerTextBox.Text;
                    RequestBll.ReportCardGameRecordGet(player, startTime, endTime);
                    break;

                case ReportTypes.COMPUTER_GAME_RECORD:
                    player = PlayerTextBox.Text;
                    RequestBll.ReportComputerGameRecordGet(player, startTime, endTime);
                    break;

                case ReportTypes.RECORD_FINANCE:
                    if (App.IsRoot)
                    {
                        location = LocationComboBox.Text;
                    }

                    player = PlayerTextBox.Text;
                    RequestBll.ReportFinanceRecordGet(location, player, startTime, endTime);
                    break;

                case ReportTypes.CAISHER_LOG:
                    if (App.IsRoot)
                    {
                        location = LocationComboBox.Text;
                    }

                    RequestBll.GetCashierLog(location, startTime, endTime);
                    break;
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            Common.ExportExcel(dataTable);
        }
    }
}
