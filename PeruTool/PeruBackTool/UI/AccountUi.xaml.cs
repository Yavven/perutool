﻿using PeruBackTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruBackTool.UI
{
    /// <summary>
    /// LocationUi.xaml 的交互逻辑
    /// </summary>
    public partial class LocationUi : UserControl
    {
        DataTable accountDataTable;
        bool isEnable;
        bool isEnable10C;
        string locationName = string.Empty;

        int type;

        public void OnMessageResponse(object response)
        {
            int id = 1;
            accountDataTable.Clear();

            switch (type)
            {
                case AdminTypes.ADMIN_CARD:
                    CommonListResponse cardAdminListResponse = response as CommonListResponse;

                    foreach (CommonAccount cardAdmin in cardAdminListResponse.Cards)
                    {
                        if (cardAdmin.Name == "root")
                        {
                            continue;
                        }

                        DataRow dataRow = accountDataTable.NewRow();
                        dataRow["Name"] = cardAdmin.Name;
                        dataRow["Status"] = cardAdmin.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(cardAdmin.TimeStamp);
                        accountDataTable.Rows.Add(dataRow);
                    }

                    break;

                case AdminTypes.FINANCE:
                    AdminFinanceSearchResponse financeAdminListResponse = response as AdminFinanceSearchResponse;

                    foreach (AdminFinance financeAdmin in financeAdminListResponse.Admins)
                    {
                        if (financeAdmin.Name == "root")
                        {
                            continue;
                        }

                        DataRow dataRow = accountDataTable.NewRow();
                        dataRow["Type"] = "Cashier";
                        dataRow["LocationName"] = locationName; 
                        dataRow["User Name"] = financeAdmin.Name;
                        dataRow["Create Time"] = DateTimeUtil.ConvertTimeStampToTimeString(financeAdmin.TimeStamp);
                        dataRow["Status"] = financeAdmin.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                        accountDataTable.Rows.Add(dataRow);
                    }

                    break;

                case AdminTypes.PLAYER_COMPUTER:
                    ComputerListResponse computerListResponse = response as ComputerListResponse;

                    foreach (Computer computer in computerListResponse.Computers)
                    {
                        if (computer.Name == "root")
                        {
                            continue;
                        }

                        DataRow dataRow = accountDataTable.NewRow();
                        dataRow["Type"] = "Computer";
                        dataRow["LocationName"] = locationName;
                        dataRow["User Name"] = computer.Name;
                        dataRow["Create Time"] = DateTimeUtil.ConvertTimeStampToTimeString(computer.TimeStamp);
                        dataRow["Status"] = computer.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                        dataRow["10 Status"] = computer.ShowStatus == StatusTypes.ENABLE ? "Enable" : "Disable";
                        accountDataTable.Rows.Add(dataRow);
                    }

                    break;

                case AdminTypes.BACK:
                    AdminBackSearchResponse locationAdminListResponse = response as AdminBackSearchResponse;

                    foreach (AdminBack locationAdmin in locationAdminListResponse.Admins)
                    {
                        if (locationAdmin.Name == "root")
                        {
                            continue;
                        }

                        DataRow dataRow = accountDataTable.NewRow();
                        dataRow["Id"] = id++;
                        dataRow["Location Name"] = locationAdmin.Name;
                        dataRow["Create Time"] = DateTimeUtil.ConvertTimeStampToTimeString(locationAdmin.TimeStamp);
                        dataRow["Status"] = locationAdmin.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                        accountDataTable.Rows.Add(dataRow);
                    }

                    break;

            }

            AccountDataGrid.ItemsSource = accountDataTable.AsDataView();
        }

        public void ShowLocation(bool isShow)
        {
            if (isShow)
            {
                LocationLabel.Visibility = Visibility.Visible;
                LocationComboBox.Visibility = Visibility.Visible;
                LocationComboBox.SelectedIndex = 0;
            }
            else
            {
                LocationLabel.Visibility = Visibility.Collapsed;
                LocationComboBox.Visibility = Visibility.Collapsed;
            }
        }

        public void SetType(int accountType)
        {
            TitleLabel.Content = App.AccountTypeNames[accountType] + " List";
            LocationComboBox.Items.Clear();
            accountDataTable = new DataTable();
            AccountDataGrid.ItemsSource = null;
            type = accountType;

            switch (accountType)
            {
                case AdminTypes.BACK:
                    accountDataTable.Columns.Add("Id");
                    accountDataTable.Columns.Add("Location Name");
                    accountDataTable.Columns.Add("Create Time");
                    accountDataTable.Columns.Add("Status");
                    Enable10CButton.Visibility = Visibility.Collapsed;

                    if(App.IsRoot)
                    {
                        ChangePasswordButton.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ChangePasswordButton.Visibility = Visibility.Collapsed;
                    }

                    ShowLocation(false);
                    break;

                case AdminTypes.ADMIN_CARD:
                    accountDataTable.Columns.Add("Name");
                    accountDataTable.Columns.Add("Status");
                    accountDataTable.Columns.Add("Time");
                    Enable10CButton.Visibility = Visibility.Collapsed;           
                    ChangePasswordButton.Visibility = Visibility.Collapsed;  
                    ShowLocation(false);
                    break;

                case AdminTypes.FINANCE:
                    accountDataTable.Columns.Add("Type");
                    accountDataTable.Columns.Add("LocationName");
                    accountDataTable.Columns.Add("User Name");
                    accountDataTable.Columns.Add("Create Time");
                    accountDataTable.Columns.Add("Status");
                    Enable10CButton.Visibility = Visibility.Collapsed;

                    if (App.IsRoot)
                    {
                        ChangePasswordButton.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        ChangePasswordButton.Visibility = Visibility.Collapsed;
                    }

                    if (App.MyLoginWindow.IsRoot)
                    {
                        foreach (AdminBack location in App.Locations)
                        {
                            LocationComboBox.Items.Add(location.Name);
                        }
                    }
                    else
                    {
                        LocationComboBox.Items.Add(App.LoginLocation);
                    }

                    ShowLocation(true);
                    break;

                case AdminTypes.PLAYER_COMPUTER:
                    accountDataTable.Columns.Add("Type");
                    accountDataTable.Columns.Add("LocationName");
                    accountDataTable.Columns.Add("User Name");
                    accountDataTable.Columns.Add("Create Time");
                    accountDataTable.Columns.Add("Status");
                    accountDataTable.Columns.Add("10 Status");
                    Enable10CButton.Visibility = Visibility.Visible;
                    ChangePasswordButton.Visibility = Visibility.Collapsed;  

                    if (App.MyLoginWindow.IsRoot)
                    {
                        foreach (AdminBack location in App.Locations)
                        {
                            LocationComboBox.Items.Add(location.Name);
                        }
                    }
                    else
                    {
                        LocationComboBox.Items.Add(App.LoginLocation);
                    }

                    ShowLocation(true);
                    break;
            }
        }

        public LocationUi()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AccountAddWindow accountAddWindow = new AccountAddWindow(type);
            accountAddWindow.Show();
        }

        private void EnableButton_Click(object sender, RoutedEventArgs e)
        {
            if (AccountDataGrid.SelectedItem == null)
            {
                return;
            }

            DataRowView selectedDataRowView = (DataRowView)AccountDataGrid.SelectedItem;
            string name = selectedDataRowView.Row["Name"].ToString();

            switch (type)
            {
                case AdminTypes.ADMIN_CARD:
                    RequestBll.CardEnable(!isEnable, name);
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.CasherEnable(!isEnable, name);
                    break;

                case AdminTypes.PLAYER_COMPUTER:
                    RequestBll.ComputerEnable(!isEnable, name);
                    break;

                case AdminTypes.BACK:
                    RequestBll.LocationEnable(!isEnable, name);
                    break;
            }
        }

        private void AccountDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AccountDataGrid.SelectedItem == null)
            {
                return;
            }

            DataRowView selectedDataRowView = null;

            try
            {
                selectedDataRowView = (DataRowView)AccountDataGrid.SelectedItem;
            }
            catch
            {
                return;
            }

            string status = selectedDataRowView.Row["Status"].ToString();

            if (status == "Enable")
            {
                EnableButton.Content = "Disable";
                isEnable = true;
            }
            else
            {
                EnableButton.Content = "Enable";
                isEnable = false;
            }


            if (type == AdminTypes.PLAYER_COMPUTER)
            {
                string tenStatus = selectedDataRowView.Row["10C Status"].ToString();

                if (tenStatus == "Enable")
                {
                    Enable10CButton.Content = "Hide 10C";
                    isEnable10C = true;
                }
                else
                {
                    Enable10CButton.Content = "Show 10C";
                    isEnable10C = false;
                }
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            string location = LocationComboBox.Text;
            locationName = location;

            switch (type)
            {
                case AdminTypes.ADMIN_CARD:
                    RequestBll.CardSearch();
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.FinanceAdminGet(location);
                    break;

                case AdminTypes.PLAYER_COMPUTER:
                    RequestBll.ComputerSearch(location);
                    break;

                case AdminTypes.BACK:
                    RequestBll.Location();
                    break;
            }
        }

        private void Enable10CButton_Click(object sender, RoutedEventArgs e)
        {
            if (AccountDataGrid.SelectedItem == null)
            {
                return;
            }

            DataRowView selectedDataRowView = (DataRowView)AccountDataGrid.SelectedItem;
            string name = selectedDataRowView.Row["Name"].ToString();
            RequestBll.ComputerEnable10C(!isEnable10C, name);
        }

        private void ChangePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            if (AccountDataGrid.SelectedItem == null)
            {
                return;
            }
            else
            {
                DataRowView selectedDataRowView = (DataRowView)AccountDataGrid.SelectedItem;
                string name = selectedDataRowView.Row["Name"].ToString();
                ChangetPasswordWindow changePasswordWindow = new ChangetPasswordWindow(type, name);
                changePasswordWindow.Show();
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            Common.ExportExcel(accountDataTable);
        }
    }
}
