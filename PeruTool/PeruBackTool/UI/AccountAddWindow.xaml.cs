﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary;
using PeruBackTool.BLL;
using PeruToolClassLibrary.Model;
using YWCommonUtil;

namespace PeruBackTool.UI
{
    /// <summary>
    /// AddWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AccountAddWindow
    {
        int type;

        public AccountAddWindow(int accountType)
        {
            InitializeComponent();

            type = accountType;
            Title = "Add " + App.AccountTypeNames[accountType];

            if(accountType > AdminTypes.ADMIN_CARD)
            {
                LocationLabel.Visibility = Visibility.Visible;
                LocationComboBox.Visibility = Visibility.Visible;
                LocationComboBox.Items.Clear();

                if(App.IsRoot)
                {
                    foreach (AdminBack location in App.Locations)
                    {
                        if(location.Name != "root" && location.Status == StatusTypes.ENABLE)
                        {
                            LocationComboBox.Items.Add(location.Name);
                        }
                    }
                }
                else
                {
                    LocationComboBox.Items.Add(App.LoginLocation);
                }

                LocationComboBox.SelectedIndex = 0;
            }
            else
            {
                LocationLabel.Visibility = Visibility.Collapsed;
                LocationComboBox.Visibility = Visibility.Collapsed;
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Text;
            string locationName = string.Empty;

            if(string.IsNullOrEmpty(username))
            {
                MessageBox.Show("Username is empty!");
                return;
            }


            if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password is empty!");
                return;
            }

            if(type > AdminTypes.ADMIN_CARD)
            {
                locationName = LocationComboBox.Text;
            }

            switch(type)
            {
                case AdminTypes.BACK:
                    RequestBll.LocationAdd(password, username);
                    break;

                case AdminTypes.ADMIN_CARD:
                    RequestBll.CardAdd(username, password);
                    break;

                case AdminTypes.PLAYER_COMPUTER:
                    RequestBll.ComputerAdd(locationName, username, password);
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.FinanceAdminAdd(locationName, username, password);
                    break;
            }

            Hide();
        }
    }
}
