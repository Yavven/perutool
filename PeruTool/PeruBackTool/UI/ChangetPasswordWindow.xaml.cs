﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary;
using PeruBackTool.BLL;
using PeruToolClassLibrary.Model;
using YWCommonUtil;

namespace PeruBackTool.UI
{
    /// <summary>
    /// AddWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ChangetPasswordWindow
    {
        int type;
        string name;

        public ChangetPasswordWindow(int accountType, string username)
        {
            InitializeComponent();

            type = accountType;
            name = username;

            UsernameTextBox.Text = username;
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            string password = PasswordPasswordBox.Password;
            string passwordConfirm = PasswordConfirmPasswordBox.Password;

            if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password is empty!");
                return;
            }

            if(!string.Equals(password, passwordConfirm))
            {
                MessageBox.Show("Password do not match!");
                return;
            }

            switch(type)
            {
                case AdminTypes.BACK:
                    RequestBll.ChangeLocationPassword(name, password);
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.ChangeCashierPassword(name, password);
                    break;
            }

            Hide();
        }
    }
}
