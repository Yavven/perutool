﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary.Response;
using PeruToolClassLibrary.Model;
using YWCommonUtil;
using PeruBackTool.BLL;

namespace PeruBackTool.UI
{
    /// <summary>
    /// CardBalanceFilterWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CardBalanceFilterWindow
    {
        DataTable dataTable = new DataTable();

        public void OnMessageResponse(object responseObject)
        {
            double totalBalance = 0;
            CardBalanceSearchResponse response = responseObject as CardBalanceSearchResponse;

            foreach (CardBalance cardBalance in response.CardBalances)
            {
                totalBalance += cardBalance.Balance;
                DataRow dataRow = dataTable.NewRow();
                dataRow["Card"] = cardBalance.CardId;
                dataRow["Balance"] = cardBalance.Balance;
                dataTable.Rows.Add(dataRow);
            }

            MyDataGrid.ItemsSource = dataTable.AsDataView();
            TotalBalance.Content = totalBalance.ToString("0.00");
        }

        public CardBalanceFilterWindow()
        {
            InitializeComponent();

            dataTable.Columns.Add("Card");
            dataTable.Columns.Add("Balance");
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            double balance = 0;
            dataTable.Clear();

            if(BalanceTextBox.Text!= string.Empty)
            {
                balance = Convert.ToDouble(BalanceTextBox.Text);
            }

            RequestBll.CardBalanceFilter(balance);
        }

        private void BalanceTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsNumber(e.Text))
            {
                e.Handled = true;
            }
        }

        private void BalanceTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void BalanceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(BalanceTextBox.Text != string.Empty)
            {
                try
                {
                    double balance = Convert.ToDouble(BalanceTextBox.Text);

                    if (balance < 0)
                    {
                        BalanceTextBox.Text = "0";
                    }
                }
                catch
                {
                    BalanceTextBox.Text = "0";
                }
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            Common.ExportExcel(dataTable);
        }
    }
}
