﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Request;

namespace PeruBackTool.BLL
{
    class LoginBll
    {
        public static void Login(string password, string username)
        {
            LoginRequest request = new LoginRequest(username, password);
            App.TcpClient.Send(BackToolMessageId.LOGIN, request.GetBytes());
        }
    }
}
