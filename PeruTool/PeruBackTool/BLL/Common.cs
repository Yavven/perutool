﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using YWCommonUtil;

namespace PeruBackTool.BLL
{
    class Common
    {
        public static void ExportExcel(System.Data.DataTable dataTable)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "*.xls|*.xls";
            //设置默认文件类型显示顺序
            saveFileDialog.FilterIndex = 2;
            //保存对话框是否记忆上次打开的目录
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == true)
            {
                if (Excel.DataTableToExcel(dataTable, saveFileDialog.FileName))
                {
                    MessageBox.Show("success");
                }
                else
                {
                    MessageBox.Show("fail");
                }
            }
        }
    }
}
