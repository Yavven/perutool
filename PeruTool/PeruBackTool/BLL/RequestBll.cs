﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Request;
using PeruToolClassLibrary;

namespace PeruBackTool.BLL
{
    class RequestBll
    {
        #region Location
        public static void Location()
        {
            App.TcpClient.Send(BackToolMessageId.ADMIN_LOCATION);
        }

        public static void LocationAdd(string password, string username)
        {
            AdminBackCreateRequest request = new AdminBackCreateRequest(username, password);
            App.TcpClient.Send(BackToolMessageId.ADMIN_LOCATION_ADD, request.GetBytes());
        }

        public static void LocationEnable(bool enable, string username)
        {
            CommonEnableRequest request = new CommonEnableRequest(username, enable);
            App.TcpClient.Send(BackToolMessageId.ADMIN_LOCATION_ENABLE, request.GetBytes());
        }

        public static void ChangeLocationPassword(string username, string password)
        {
            TowStringRequest request = new TowStringRequest(username, password);
            App.TcpClient.Send(BackToolMessageId.CHANGE_LOCATION_PASSWORD, request.GetBytes());
        }
        #endregion

        public static void FinanceAdminGet(string locationAdminName)
        {
            App.LastSendLocation = locationAdminName;
            CommonSearchRequest request = new CommonSearchRequest(locationAdminName);
            App.TcpClient.Send(BackToolMessageId.ADMIN_FINANCE, request.GetBytes());
        }

        public static void FinanceAdminAdd(string locationAdminName, string username, string password)
        {
            CommonCreateRequest request = new CommonCreateRequest(locationAdminName, password, username);
            App.TcpClient.Send(BackToolMessageId.ADMIN_FINANCE_ADD, request.GetBytes());
        }

        public static void CasherEnable(bool enable, string name)
        {
            CommonEnableRequest request = new CommonEnableRequest(name, enable);
            App.TcpClient.Send(BackToolMessageId.ADMIN_FINANCE_ENABLE, request.GetBytes());
        }

        public static void ChangeCashierPassword(string username, string password)
        {
            TowStringRequest request = new TowStringRequest(username, password);
            App.TcpClient.Send(BackToolMessageId.CHANGE_FINANCE_PASSWORD, request.GetBytes());
        }

        public static void ComputerSearch(string locationName)
        {
            App.LastSendLocation = locationName;
            CommonSearchRequest request = new CommonSearchRequest(locationName);
            App.TcpClient.Send(BackToolMessageId.COMPUTER, request.GetBytes());
        }

        public static void ComputerAdd(string locationName, string name, string password)
        {
            CommonCreateRequest request = new CommonCreateRequest(locationName, password, name);
            App.TcpClient.Send(BackToolMessageId.COMPUTER_ADD, request.GetBytes());
        }

        public static void ComputerEnable(bool enable, string name)
        {
            CommonEnableRequest request = new CommonEnableRequest(name, enable);
            App.TcpClient.Send(BackToolMessageId.COMPUTER_ENABLE, request.GetBytes());
        }

        public static void ComputerEnable10C(bool enable, string name)
        {
            CommonEnableRequest request = new CommonEnableRequest(name, enable);
            App.TcpClient.Send(BackToolMessageId.COMPUTER_ENABLCE_10C, request.GetBytes());
        }

        public static void CardSearch()
        {
            App.TcpClient.Send(BackToolMessageId.ADMIN_CARD);
        }

        public static void CardAdd(string name, string password)
        {
            CardAdminAddRequest request = new CardAdminAddRequest(name, password);
            App.TcpClient.Send(BackToolMessageId.ADMIN_CARD_ADD, request.GetBytes());
        }

        public static void CardEnable(bool enable, string name)
        {
            CommonEnableRequest request = new CommonEnableRequest(name, enable);
            App.TcpClient.Send(BackToolMessageId.ADMIN_CARD_ENABLE, request.GetBytes());
        }

        public static void ReportGet(int messageId, string username, int startTime, int endTime)
        {
            ReportSearchRequest request = new ReportSearchRequest(username, startTime, endTime);
            App.TcpClient.Send(messageId, request.GetBytes());
        }

        public static void ReportProfit(string locationName, int startTime, int endTime)
        {
            ReportGet(BackToolMessageId.REPORT_PROFIT_SEARCH, locationName, startTime, endTime);
        }

        public static void ReportFinance(string locationName, int startTime, int endTime)
        {
            ReportGet(BackToolMessageId.REPORT_FINANCE_SEARCH, locationName, startTime, endTime);
        }

        public static void ReportBalanceChange(string cardId, int startTime, int endTime)
        {
            ReportGet(BackToolMessageId.REPORT_BALANCE_CHANGE_GET, cardId, startTime, endTime);
        }

        public static void ReportCardGameRecordGet(string cardId, int startTime, int endTime)
        {
            ReportGet(BackToolMessageId.RECORD_GAME_SEARCH_BY_CARD, cardId, startTime, endTime);
        }

        public static void ReportComputerGameRecordGet(string computerName, int startTime, int endTime)
        {
            ReportGet(BackToolMessageId.RECORD_GAME_SEARCH_BY_COMPUTER, computerName, startTime, endTime);
        }

        public static void ReportFinanceRecordGet(string location, string username, int startTime, int endTime)
        {
            ReportFinanceRecordGetRequest request = new ReportFinanceRecordGetRequest(location, username, startTime, endTime);
            App.TcpClient.Send(BackToolMessageId.RECORD_FINANCE_SEARCH, request.GetBytes());
        }

        public static void GetCashierLog(string location, int startTimeStamp, int endTimeStamp)
        {
            StringIntIntRequest request = new StringIntIntRequest(location, startTimeStamp, endTimeStamp);
            App.TcpClient.Send(BackToolMessageId.RECORD_CASHIER, request.GetBytes());
        }

        public static void GameConfig(int gameId)
        {
            GameConfigSearchRequest request = new GameConfigSearchRequest(gameId);
            App.TcpClient.Send(BackToolMessageId.GAME_CONFIG, request.GetBytes());
        }

        public static void GetAllGameConfig()
        {
            App.TcpClient.Send(BackToolMessageId.SEARCH_ALL_GAME_CONFIG);
        }

        public static void GameConfigEdit(int gameId,int[] flag, int openStatus, int rachargeRate, double taxRate, int maxBet, int maxPaid, int paytableIndex)
        {
            GameConfigEditRequest request = new GameConfigEditRequest(gameId, flag, openStatus, rachargeRate, taxRate, maxBet, maxPaid, paytableIndex);
            App.TcpClient.Send(BackToolMessageId.GAME_CONFIG_EDIT, request.GetBytes());
        }

        public static void GlobalGameConfig()
        {
            App.TcpClient.Send(BackToolMessageId.GAME_GLOBAL_CONFIG);
        }

        public static void GlobalGameConfigEdit(int[] flag, int taxEdge, int doubleStatus, int doubleMaxEachBet, int doubleMaxEachPaid)
        {
            GameConfigGlobalEditRequest request = new GameConfigGlobalEditRequest(flag, taxEdge, doubleStatus, doubleMaxEachBet, doubleMaxEachPaid);         
            App.TcpClient.Send(BackToolMessageId.GAME_GLOBAL_CONFIG_EDIT, request.GetBytes());
        }

        public static void CompanyInfo()
        {
            App.TcpClient.Send(BackToolMessageId.COMPANY_INFO);
        }

        public static void CompanyInfoEdit(string name, string howToGet, string address, string telphone)
        {
            CompanyInfoEditRequest request = new CompanyInfoEditRequest(name, howToGet, address, telphone);
            App.TcpClient.Send(BackToolMessageId.COMPANY_INFO_EDIT, request.GetBytes());
        }

        public static void CardBalanceFilter(double balance)
        {
            CardSearchRequest request = new CardSearchRequest(balance);
            App.TcpClient.Send(BackToolMessageId.CARD_BALANCE_FILTER, request.GetBytes());
        }
    }
}
