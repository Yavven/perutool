﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using YWNetworkEngine;

namespace PeruBackTool.BLL
{
    public class MessageBll:YWResponseBll
    {
        public override void OnMessageResponse(int messageId, byte[] messageBodyBytes)
        {
            object response = null;

            switch (messageId)
            {
                case BackToolMessageId.LOGIN:
                    App.MyLoginWindow.OnMessageResponse(new LoginResponse(messageBodyBytes));
                    break;

                case BackToolMessageId.ADMIN_LOCATION:
                    response = new AdminBackSearchResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_LOCATION_ADD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_LOCATION_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.CHANGE_LOCATION_PASSWORD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_FINANCE:
                    response = new AdminFinanceSearchResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_FINANCE_ADD:                            
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_FINANCE_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.CHANGE_FINANCE_PASSWORD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.COMPUTER:
                    response = new ComputerListResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.COMPUTER_ADD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.COMPUTER_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.COMPUTER_ENABLCE_10C:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_CARD:
                    response = new CommonListResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_CARD_ADD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.ADMIN_CARD_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.REPORT_BALANCE_CHANGE_GET:
                    response = new RecordBalanceResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.RECORD_FINANCE_SEARCH:
                    response = new FinanceRecordResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.REPORT_PROFIT_SEARCH:
                    response = new ReportProfitResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.REPORT_FINANCE_SEARCH:
                    response = new FinanceReportSearchResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.RECORD_GAME_SEARCH_BY_CARD:
                    response = new GameRecordResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.RECORD_GAME_SEARCH_BY_COMPUTER:
                    response = new GameRecordResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.RECORD_CASHIER:
                    response = new FinanceRecordResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.GAME_CONFIG_EDIT:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.GAME_CONFIG:
                    response = new GameConfigSearchResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.GAME_GLOBAL_CONFIG_EDIT:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.GAME_GLOBAL_CONFIG:
                    response = new GlobaGamelConfigSearchResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.SEARCH_ALL_GAME_CONFIG:
                    response = new GetAllGameConfigResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.COMPANY_INFO:
                    response = new CompanyInfoSearchResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.COMPANY_INFO_EDIT:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case BackToolMessageId.CARD_BALANCE_FILTER:
                    response = new CardBalanceSearchResponse(messageBodyBytes);
                    break;
            }

            if(response != null)
            {
                App.GlobalMainWindow.OnMessageResponse(messageId, response);
            }
        }
    }
}
