﻿#pragma checksum "..\..\..\UI\AccountUi - 复制.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8F2304F6FDEC7CFE5F3BA9E4208499A2"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.34014
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PeruBackTool.UI {
    
    
    /// <summary>
    /// LocationUi
    /// </summary>
    public partial class LocationUi : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid AccountDataGrid;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label LocationLabel;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TitleLabel;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddButton;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EnableButton;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Enable10CButton;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox LocationComboBox;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\UI\AccountUi - 复制.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SearchButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PeruBackTool;component/ui/accountui%20-%20%e5%a4%8d%e5%88%b6.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UI\AccountUi - 复制.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.AccountDataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 9 "..\..\..\UI\AccountUi - 复制.xaml"
            this.AccountDataGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AccountDataGrid_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.LocationLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.TitleLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.AddButton = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\UI\AccountUi - 复制.xaml"
            this.AddButton.Click += new System.Windows.RoutedEventHandler(this.AddButton_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.EnableButton = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\..\UI\AccountUi - 复制.xaml"
            this.EnableButton.Click += new System.Windows.RoutedEventHandler(this.EnableButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Enable10CButton = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\UI\AccountUi - 复制.xaml"
            this.Enable10CButton.Click += new System.Windows.RoutedEventHandler(this.Enable10CButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.LocationComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.SearchButton = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\UI\AccountUi - 复制.xaml"
            this.SearchButton.Click += new System.Windows.RoutedEventHandler(this.SearchButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

