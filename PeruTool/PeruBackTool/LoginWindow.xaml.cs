﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruBackTool.BLL;
using PeruToolClassLibrary.Response;
using YWCommonUtil;

namespace PeruBackTool
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow
    {
        public bool IsRoot;

        void Login()
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Password;
            App.LoginLocation = username;
            LoginBll.Login(password, username);
        }

        public void OnMessageResponse(LoginResponse response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if (response.IsSuccess())
                {
                    IsRoot = response.IsRoot();
                    App.IsRoot = response.IsRoot();
                    Hide();
                    App.GlobalMainWindow = new MainWindow();
                    App.GlobalMainWindow.Show();
                }
                else
                {
                    MessageBox.Show("fail");
                }
            }));
        }

        public LoginWindow()
        {
            InitializeComponent();

            App.MyLoginWindow = this;
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void PasswordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                Login();
            }
        }
    }
}
