﻿using PeruBackTool.BLL;
using PeruBackTool.UI;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeruBackTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        UserControl currentUi;

        public MainWindow()
        {
            InitializeComponent();

            if(App.IsRoot)
            {
                RequestBll.Location();
                ChangePasswordMenuItem.Visibility = Visibility.Collapsed;
            }
            else
            {
                LocationMenu.Visibility = Visibility.Collapsed;
                CardAdminMenu.Visibility = Visibility.Collapsed;
                GlobalGameConfigMenuItem.Visibility = Visibility.Collapsed;
                CardBalanceFilterMenuItem.Visibility = Visibility.Collapsed;
                CompanyInfoMenuItem.Visibility = Visibility.Collapsed;
                AllGameConfigMenuItem.Visibility = Visibility.Collapsed;
            }
        }

        public void OnMessageResponse(int messageId, object response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                switch(messageId)
                {
                    case BackToolMessageId.ADMIN_LOCATION:
                        App.Locations = (response as AdminBackSearchResponse).Admins;

                        if(Accounts.Visibility == Visibility.Visible)
                        {
                            Accounts.OnMessageResponse(response);
                        }

                        break;

                    case BackToolMessageId.ADMIN_LOCATION_ADD:
                        if((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                            RequestBll.Location();
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }
                        break;


                    case BackToolMessageId.CHANGE_LOCATION_PASSWORD:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }
                        break;

                    case BackToolMessageId.ADMIN_LOCATION_ENABLE:
                        RequestBll.Location();
                        break;

                    case BackToolMessageId.ADMIN_FINANCE:
                        if (Accounts.Visibility == Visibility.Visible)
                        {
                            Accounts.OnMessageResponse(response);
                        }

                        break;

                    case BackToolMessageId.ADMIN_FINANCE_ADD:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                            RequestBll.FinanceAdminGet(App.LastSendLocation);
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }
                        break;

                    case BackToolMessageId.ADMIN_FINANCE_ENABLE:                       
                        RequestBll.FinanceAdminGet(App.LastSendLocation);
                        break;

                    case BackToolMessageId.CHANGE_FINANCE_PASSWORD:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }
                        break;

                    case BackToolMessageId.COMPUTER:
                        if (Accounts.Visibility == Visibility.Visible)
                        {
                            Accounts.OnMessageResponse(response);
                        }

                        break;

                    case BackToolMessageId.COMPUTER_ADD:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                            RequestBll.ComputerSearch(App.LastSendLocation);
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }
                        break;

                    case BackToolMessageId.COMPUTER_ENABLE:              
                        RequestBll.ComputerSearch(App.LastSendLocation); 
                        break;

                    case BackToolMessageId.COMPUTER_ENABLCE_10C:
                        RequestBll.ComputerSearch(App.LastSendLocation);
                        break;

                    case BackToolMessageId.ADMIN_CARD:
                        if (Accounts.Visibility == Visibility.Visible)
                        {
                            Accounts.OnMessageResponse(response);
                        }

                        break;

                    case BackToolMessageId.ADMIN_CARD_ADD:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                            RequestBll.CardSearch();
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }
                        break;

                    case BackToolMessageId.ADMIN_CARD_ENABLE:         
                        RequestBll.CardSearch();
                        break;

                    case BackToolMessageId.REPORT_PROFIT_SEARCH:
                        App.ProfitReportWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.REPORT_FINANCE_SEARCH:
                        App.FinanceReportWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.RECORD_FINANCE_SEARCH:
                        App.FinanceRecordWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.RECORD_CASHIER:
                        App.CaisherLogWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.RECORD_GAME_SEARCH_BY_CARD:
                        App.CardGameRecordWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.RECORD_GAME_SEARCH_BY_COMPUTER:
                        App.ComputerRecordWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.REPORT_BALANCE_CHANGE_GET:
                        App.BalanceRecordWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.GAME_CONFIG:
                        MyGameConfigUi.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.GAME_CONFIG_EDIT:
                        if((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case BackToolMessageId.GAME_GLOBAL_CONFIG:
                        MyGlobalGameConfigUi.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.SEARCH_ALL_GAME_CONFIG:
                        App.MyAllGameConfigWindow.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.GAME_GLOBAL_CONFIG_EDIT:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case BackToolMessageId.COMPANY_INFO:
                        MyCompanyInfoUi.OnMessageResponse(response);
                        break;

                    case BackToolMessageId.COMPANY_INFO_EDIT:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case BackToolMessageId.CARD_BALANCE_FILTER:
                        App.MyCardBalanceFilterWindow.OnMessageResponse(response);
                        break;
                }
            }));           
        }

        public void ShowUi(UserControl ui)
        {
            if (currentUi != null)
            {
                currentUi.Visibility = Visibility.Collapsed;
            }

            ui.Visibility = Visibility.Visible;
            currentUi = ui;
        }

        private void LocationAdminMenu_Click(object sender, RoutedEventArgs e)
        {
            Accounts.SetType(AdminTypes.BACK);
            ShowUi(Accounts);
        }

        private void ComputerMenu_Click(object sender, RoutedEventArgs e)
        {
            Accounts.SetType(AdminTypes.PLAYER_COMPUTER);
            ShowUi(Accounts);
        }

        private void FinanceAdminMenu_Click(object sender, RoutedEventArgs e)
        {
            Accounts.SetType(AdminTypes.FINANCE);
            ShowUi(Accounts);
        }

        private void CardAdminMenu_Click(object sender, RoutedEventArgs e)
        {
            Accounts.SetType(AdminTypes.ADMIN_CARD);
            ShowUi(Accounts);
        }

        private void ProfitReportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.ProfitReportWindow = new ReportWindow(ReportTypes.REPORT_PROFIT);
            App.ProfitReportWindow.Show();
        }

        private void FinanceReportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.FinanceReportWindow = new ReportWindow(ReportTypes.REPORT_FINANCE);
            App.FinanceReportWindow.Show();
        }

        private void BalanceRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.BalanceRecordWindow = new ReportWindow(ReportTypes.RECORD_BALANCE);
            App.BalanceRecordWindow.Show();
        }

        private void FinanceRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.FinanceRecordWindow = new ReportWindow(ReportTypes.RECORD_FINANCE);
            App.FinanceRecordWindow.Show();
        }

        private void CardGameRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.CardGameRecordWindow = new ReportWindow(ReportTypes.RECORD_GAME);
            App.CardGameRecordWindow.Show();
        }

        private void ComputerGameRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.ComputerRecordWindow = new ReportWindow(ReportTypes.COMPUTER_GAME_RECORD);
            App.ComputerRecordWindow.Show();
        }

        private void GameConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ShowUi(MyGameConfigUi);
        }

        private void GlobalGameConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.GlobalGameConfig();
            ShowUi(MyGlobalGameConfigUi);
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void CardBalanceFilterMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.MyCardBalanceFilterWindow = new CardBalanceFilterWindow();
            App.MyCardBalanceFilterWindow.Show();
        }

        private void CompanyInfoMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.CompanyInfo();
            ShowUi(MyCompanyInfoUi);
        }

        private void ChangePasswordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            string username = App.LoginLocation;
            ChangetPasswordWindow changePasswordWindow = new ChangetPasswordWindow(AdminTypes.BACK, username);
            changePasswordWindow.Show();
        }

        private void CaisherLogMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.CaisherLogWindow = new ReportWindow(ReportTypes.CAISHER_LOG);
            App.CaisherLogWindow.Show();
        }

        private void AllGameConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.MyAllGameConfigWindow = new AllGameConfigWindow();
            App.MyAllGameConfigWindow.Show();
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
