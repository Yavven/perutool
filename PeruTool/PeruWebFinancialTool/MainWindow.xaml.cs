﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using PeruWebFinancialTool.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeruWebFinancialTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        public void OnMessageResponse(int messageId, object response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                switch (messageId)
                {
                    case WebFinanceToolMessageId.TRANSACTION_HISTORY_SEARCH:
                        App.MyTransactionHistoryWindow.OnMessageResponse(response);
                        break;

                    case WebFinanceToolMessageId.CODEUSER_SEARCH:
                        if(App.MyDepositWindow != null)
                        {
                            if (App.CurrentTransactionType == TransferTypes.DEPOSIT)
                            {
                                App.MyDepositWindow.OnMessageResponse(response);
                            }
                        }

                        if(App.MyWithdrawWindow != null)
                        {
                            if (App.CurrentTransactionType == TransferTypes.WITHDRAW)
                            {
                                App.MyWithdrawWindow.OnMessageResponse(response);
                            }
                        }

                        break;

                    case WebFinanceToolMessageId.TRANSACTION_CONFIRM:
                        if((response as TransactionConfirmResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;
                }
            }));
        }


        public MainWindow()
        {
            InitializeComponent();

            Title = App.MyLanguage.VirtualCashier();
            TransactionHistoryButton.Content = App.MyLanguage.TransactionHistory();
            ManageAccountButton.Content = App.MyLanguage.ManageAccount();
            LogOutButton.Content = App.MyLanguage.LogOut();
        }

        private void TransactionHistoryButton_Click(object sender, RoutedEventArgs e)
        {
            App.MyTransactionHistoryWindow = new TransactionHistoryWindow();
            App.MyTransactionHistoryWindow.Show();
        }

        private void ManageAccountButton_Click(object sender, RoutedEventArgs e)
        {
            App.MyManageAccountWindow = new ManageAccountWindow();
            App.MyManageAccountWindow.Show();
        }

        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
