﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using PeruWebFinancialTool.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruWebFinancialTool
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow
    {
        public void OnMessageResponse(LoginResponse response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if (response.IsSuccess())
                {
                    App.IsRoot = response.IsRoot();
                    App.MyMainWindow = new MainWindow();
                    App.MyMainWindow.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("fail");
                }
            }));
        }

        public LoginWindow()
        {
            InitializeComponent();

            App.MyLoginWindow = this;
            Title = App.MyLanguage.LoginPage();
            UserLabel.Content = App.MyLanguage.User();
            PasswordLabel.Content = App.MyLanguage.Password();
            LogInButton.Content = App.MyLanguage.LogIn();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            string password = PasswordPasswordBox.Password;    
            string username = UsernameTextBox.Text;
            RequestBll.Login(password, username);
        }

        private void UsernameTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }

        private void UsernameTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}
