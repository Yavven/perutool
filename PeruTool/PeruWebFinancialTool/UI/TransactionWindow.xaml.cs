﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using PeruWebFinancialTool.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PeruWebFinancialTool.UI
{
    /// <summary>
    /// DepositWindow.xaml 的交互逻辑
    /// </summary>
    public partial class TransactionWindow
    {
        int type;

        public void OnMessageResponse(object responseObject)
        {
            CodeUserSearchResponse response = responseObject as CodeUserSearchResponse;

            AddressTextBox.Text = response.Address;
            IdNumberTextBox.Text = response.IdNumber;
            NameTextBox.Text = response.Name;
            SurnameTextBox.Text = response.Surname;
            TotalAmountTextBox.Text = response.TotalAmount.ToString();
        }

        public TransactionWindow(int transferType)
        {
            InitializeComponent();

            type = transferType;

            if(transferType == TransferTypes.DEPOSIT)
            {
                Title = App.MyLanguage.Deposit();
                DepositButton.Content = App.MyLanguage.Deposit();
            }
            else
            {
                Title = App.MyLanguage.WithDraw();
                DepositButton.Content = App.MyLanguage.WithDraw();
            }

            CodeUserLabel.Content = App.MyLanguage.CodeUser();
            SearchButton.Content = App.MyLanguage.Search();
            AccountInformationLabel.Content = App.MyLanguage.AccountInformation();
            NameLabel.Content = App.MyLanguage.Name();
            SurnameLabel.Content = App.MyLanguage.Surname();
            AddressLabel.Content = App.MyLanguage.Address();
            IdNumberLabel.Content = App.MyLanguage.IdNumber();
            TotalAmountLabel.Content = App.MyLanguage.TotalAmount();
            CancelButton.Content = App.MyLanguage.Cancel();
            LogOutButton.Content = App.MyLanguage.LogOut();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            string username = CodeUserTextBox.Text;
            RequestBll.TransactionCodeUserSearch(username);
            App.CurrentTransactionType = type;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void DepositButton_Click(object sender, RoutedEventArgs e)
        {
            string username = CodeUserTextBox.Text;

            if(type == TransferTypes.DEPOSIT)
            {
                App.MyDepositConfirmWindow = new TransactionConfirmWindow(type, username);
                App.MyDepositConfirmWindow.Show();
            }
            else
            {
                App.MyWithdrawConfirmWindow = new TransactionConfirmWindow(type, username);
                App.MyWithdrawConfirmWindow.Show();
            }

            App.CurrentTransactionType = type;
        }
    }
}
