﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary;

namespace PeruWebFinancialTool.UI
{
    /// <summary>
    /// ManageAccountWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ManageAccountWindow
    {
        public ManageAccountWindow()
        {
            InitializeComponent();

            this.Title = App.MyLanguage.DepositWithDraw();
            this.DepositButton.Content = App.MyLanguage.Deposit();
            this.WithDrawButton.Content = App.MyLanguage.WithDraw();
            this.LogOutButton.Content = App.MyLanguage.LogOut();
        }

        private void DepositButton_Click(object sender, RoutedEventArgs e)
        {
            App.MyDepositWindow = new TransactionWindow(TransferTypes.DEPOSIT);
            App.MyDepositWindow.Show();
        }

        private void WithDrawButton_Click(object sender, RoutedEventArgs e)
        {
            App.MyWithdrawWindow = new TransactionWindow(TransferTypes.WITHDRAW);
            App.MyWithdrawWindow.Show();
        }

        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
