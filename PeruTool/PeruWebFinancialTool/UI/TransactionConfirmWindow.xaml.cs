﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary;
using PeruWebFinancialTool.BLL;
using YWCommonUtil;

namespace PeruWebFinancialTool.UI
{
    /// <summary>
    /// DepositConfirmWindow.xaml 的交互逻辑
    /// </summary>
    public partial class TransactionConfirmWindow
    {
        int type;
        string name;

        public TransactionConfirmWindow(int transferType, string username)
        {
            InitializeComponent();

            type = transferType;
            name = username;

            if(type == TransferTypes.DEPOSIT)
            {
                Title = App.MyLanguage.Deposit();
                ConfirmButton.Content = App.MyLanguage.Deposit();
            }
            else
            {
                Title = App.MyLanguage.WithDraw();
                ConfirmButton.Content = App.MyLanguage.WithDraw();
            }

            AmountLabel.Content = App.MyLanguage.Amount();
            CancelButton.Content = App.MyLanguage.Cancel();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            double amount = Convert.ToDouble(AmountTextBox.Text);

            if(amount <= 0)
            {
                MessageBox.Show("Amount must be greater than zero.");
                return;
            }

            RequestBll.TransactionConfirm(name, type, amount);
            Hide();
        }

        private void AmountTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsNumber(e.Text))
            {
                e.Handled = true;
            }
        }

        private void AmountTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}
