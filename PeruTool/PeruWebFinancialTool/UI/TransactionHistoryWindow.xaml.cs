﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;
using PeruWebFinancialTool.BLL;
using PeruToolClassLibrary.Response;
using System.Data;
using PeruToolClassLibrary.Model;

namespace PeruWebFinancialTool.UI
{
    /// <summary>
    /// TransactionHistoryWindow.xaml 的交互逻辑
    /// </summary>
    public partial class TransactionHistoryWindow
    {
        DataTable dataTable;

        public void OnMessageResponse(object responseObject)
        {
            TransactionHistorySearchResponse response = responseObject as TransactionHistorySearchResponse;

            foreach (TransactionHistory history in response.Historys)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow[App.MyLanguage.Sequence()] = history.WaterNumber;
                dataRow[App.MyLanguage.CardId()] = history.UserName;
                dataRow[App.MyLanguage.Time()] = DateTimeUtil.ConvertIntToDateTime(history.TimeStamp);
                dataRow[App.MyLanguage.Type()] = history.Type;
                dataRow[App.MyLanguage.Amount()] = history.Money;
                dataTable.Rows.Add(dataRow);
            }

            MyDataGrid.ItemsSource = dataTable.AsDataView();
        }

        public TransactionHistoryWindow()
        {
            InitializeComponent();

            Title = App.MyLanguage.TransactionHistory();
            FechaLabel.Content = App.MyLanguage.Fecha();
            CancelButton.Content = App.MyLanguage.Cancel();
            LogOutButton.Content = App.MyLanguage.LogOut();

            dataTable = new DataTable();
            dataTable.Columns.Add(App.MyLanguage.Sequence());
            dataTable.Columns.Add(App.MyLanguage.CardId());
            dataTable.Columns.Add(App.MyLanguage.Time());
            dataTable.Columns.Add(App.MyLanguage.Type());
            dataTable.Columns.Add(App.MyLanguage.Amount());

            StartDateTimePicker.Value = DateTimeUtil.GetTodayStartDateTime();
            EndDateTimePicker.Value = DateTime.Now;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Hide();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            // 清除旧数据
            dataTable.Clear();            
            MyDataGrid.ItemsSource = null;

            DateTime startDateTime = StartDateTimePicker.Value.Value;
            DateTime endDateTime = EndDateTimePicker.Value.Value;
            int endTimeStamp = DateTimeUtil.ConvertDateTimeToInt(endDateTime);
            int startTimeStamp = DateTimeUtil.ConvertDateTimeToInt(startDateTime);
            RequestBll.TransactionHistorySearch(startTimeStamp, endTimeStamp);
        }

        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
