﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using YWNetworkEngine;

namespace PeruWebFinancialTool.BLL
{
    public class PeruResponseBll:YWResponseBll
    {
        public override void OnMessageResponse(int messageId, byte[] messageBodyBytes)
        {
            object response = null;

            switch (messageId)
            {
                case WebFinanceToolMessageId.LOGIN:
                    App.MyLoginWindow.OnMessageResponse(new LoginResponse(messageBodyBytes));
                    break;

                case WebFinanceToolMessageId.TRANSACTION_HISTORY_SEARCH:
                    response = new TransactionHistorySearchResponse(messageBodyBytes);
                    break;

                case WebFinanceToolMessageId.CODEUSER_SEARCH:
                    response = new CodeUserSearchResponse(messageBodyBytes);
                    break;

                case WebFinanceToolMessageId.TRANSACTION_CONFIRM:
                    response = new TransactionConfirmResponse(messageBodyBytes);
                    break;
            }

            if(response != null)
            {
                App.MyMainWindow.OnMessageResponse(messageId, response);
            }
        }
    }
}
