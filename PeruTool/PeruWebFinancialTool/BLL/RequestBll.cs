﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruWebFinancialTool.BLL
{
    class RequestBll
    {
        public static void Login(string password, string username)
        {
            LoginRequest request = new LoginRequest(username, password);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.LOGIN, request.GetBytes());
        }

        public static void GetPlayerInfo(string player)
        {
            GetPlayerInfoRequest request = new GetPlayerInfoRequest(player);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.SEARCH_PLAYER, request.GetBytes());
        }

        public static void Deposit(string cardId, double amount)
        {
            CardTransferRequest request = new CardTransferRequest(cardId, 1, amount);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.TRANSFER, request.GetBytes());
        }

        public static void Withdraw(string cardId, double amount)
        {
            CardTransferRequest request = new CardTransferRequest(cardId, 2, amount);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.TRANSFER, request.GetBytes());
        }

        public static void TransactionHistorySearch(int startTimeStamp, int endTimeStamp)
        {
            TransactionHistorySearchRequest request = new TransactionHistorySearchRequest(startTimeStamp, endTimeStamp);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.SEARCH_TRANSFER_HISTORY, request.GetBytes());
        }

        public static void TransactionCodeUserSearch(string name)
        {
            CommonSearchRequest request = new CommonSearchRequest(name);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.SEARCH_PLAYER, request.GetBytes());
        }

        public static void TransactionConfirm(string username, int type, double amount)
        {
            TransactionConfirmRequest request = new TransactionConfirmRequest(username, type, amount);
            App.MyTcpClient.Send(FinanceToolWebVersionMessageId.TRANSFER, request.GetBytes());
        }
    }
}
