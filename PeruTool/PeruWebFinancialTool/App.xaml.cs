﻿using PeruToolClassLibrary;
using PeruWebFinancialTool.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using YWCommonUtil;
using PeruWebFinancialTool.UI;

namespace PeruWebFinancialTool
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        #region 数据
        public static string AppPath;
        public static bool IsRoot;
        public static PeruLanguage MyLanguage;
        #endregion

        #region 网络
        private static PeruMessageHandler MyMessageHandler;
        private static PeruResponseBll MyResponseBll;
        public static PeruTcpClient MyTcpClient;
        private static Thread MyTcpClientThread;
        #endregion

        #region 窗口
        public static int CurrentTransactionType = 0;
        public static TransactionWindow MyDepositWindow;
        public static TransactionConfirmWindow MyDepositConfirmWindow;
        public static MainWindow MyMainWindow;
        public static LoginWindow MyLoginWindow;
        public static ManageAccountWindow MyManageAccountWindow;
        public static TransactionHistoryWindow MyTransactionHistoryWindow;
        public static TransactionWindow MyWithdrawWindow;
        public static TransactionConfirmWindow MyWithdrawConfirmWindow;
        #endregion

        public App()
        {
            object languageId;
            string serverHostnameKey;
            string serverPortKey;
            string serverHostname;
            int serverHPort;

            #region 配置数据
            languageId = ConfigUtil.GetAppSettingsValue("LanguageId");
            AppPath = AppDomain.CurrentDomain.BaseDirectory;
            MyLanguage = new PeruLanguage(Convert.ToInt32(languageId));
            #endregion

            #region 配置网络
            serverHostnameKey = "ServerHostname";
            serverPortKey = "ServerPort";

            if (IsDebug)
            {
                serverHostnameKey = "Test" + serverHostnameKey;
                serverPortKey = "Test" + serverPortKey;
            }

            serverHostname = (string)ConfigUtil.GetAppSettingsValue(serverHostnameKey);
            serverHPort = Convert.ToInt32(ConfigUtil.GetAppSettingsValue(serverPortKey));
            MyMessageHandler = new PeruMessageHandler();
            MyResponseBll = new PeruResponseBll();
            MyMessageHandler.SetMessageBLL(MyResponseBll);
            MyTcpClient = new PeruTcpClient();
            MyTcpClient.SetHostnameAndPort(serverHostname, serverHPort);
            MyTcpClient.YWMessageHandler = MyMessageHandler;
            MyTcpClientThread = new Thread(MyTcpClient.Connect);
            MyTcpClientThread.IsBackground = true;
            MyTcpClientThread.Start();
            #endregion
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)
                    return true;
                #else
                    return false;
                #endif
            }
        }
    }
}
