﻿using PeruWebBackTool.BLL;
using PeruWebBackTool.UI;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using YWCommonUtil;

namespace PeruWebBackTool
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public static string AppPath;
        public static bool IsRoot;
        public static string LoginAdminName;
        public static MessageBll GlobalPeruMessageBll;
        public static PeruMessageHandler GlobalPeruMessageHandler;
        public static PeruTcpClient MyTcpClient;
        public static Thread TcpClientThread;
        public static LoginWindow MyLoginWindow;
        public static MainWindow GlobalMainWindow;
        public static ReportWindow ReportProfitWindow;
        public static ReportWindow ReportFinanceWindow;
        public static ReportWindow RecordBalanceWindow;
        public static ReportWindow RecordFinanceWindow;
        public static ReportWindow RecordGameWindow;
        public static ReportWindow ComputerRecordWindow;
        public static CardBalanceWindow MyCardBalanceWindow;
        public static List<AdminBack> BackAdmins;
        public static string LastSendLocation;
        public static Dictionary<int, string> AdminTypeNames;
        public static Dictionary<int, string> GameNames;
        public static Dictionary<int, string> TransferTypeName;
 
        public App()
        {
            string serverHostnameKey = "ServerHostname";
            string serverPortKey = "ServerPort";

            if(IsDebug)
            {
                serverHostnameKey = "Test" + serverHostnameKey;
                serverPortKey = "Test" + serverPortKey;
            }

            AppPath = AppDomain.CurrentDomain.BaseDirectory;
            string serverHostname = Convert.ToString(ConfigUtil.GetAppSettingsValue(serverHostnameKey));
            int serverHPort = Convert.ToInt32(ConfigUtil.GetAppSettingsValue(serverPortKey));
            GlobalPeruMessageHandler = new PeruMessageHandler();
            GlobalPeruMessageBll = new MessageBll();
            GlobalPeruMessageHandler.SetMessageBLL(GlobalPeruMessageBll);
            MyTcpClient = new PeruTcpClient();
            MyTcpClient.SetHostnameAndPort(serverHostname, serverHPort);
            MyTcpClient.YWMessageHandler = GlobalPeruMessageHandler;
            TcpClientThread = new Thread(MyTcpClient.Connect);
            TcpClientThread.IsBackground = true;
            TcpClientThread.Start();
            AdminTypeNames = new Dictionary<int, string>();
            AdminTypeNames.Add(AdminTypes.ADMIN_CARD, "Card Admin");
            AdminTypeNames.Add(AdminTypes.FINANCE, "Finance Admin");
            AdminTypeNames.Add(AdminTypes.PLAYER_COMPUTER, "Computer");
            AdminTypeNames.Add(AdminTypes.BACK, "Location Admin");
            TransferTypeName = new Dictionary<int, string>();
            TransferTypeName.Add(1, "Deposit");
            TransferTypeName.Add(2, "Withdraw");
            TransferTypeName.Add(3, "Game");

            GameNames = new Dictionary<int, string>();
            GameNames.Add(1, "Poker Comodin");
            GameNames.Add(2, "2 Comodin");
            GameNames.Add(3, "Classic Bonus");
            GameNames.Add(4, "Poker Comodin");
            GameNames.Add(5, "Four Deck Draw");
            GameNames.Add(11, "Siete Rojo");
            GameNames.Add(12, "Gran Tiburon");
            GameNames.Add(13, "Tesoro Inca");
            GameNames.Add(30, "Keno");
            GameNames.Add(31, "Power Keno");
            GameNames.Add(32, "Highroller Keno");
            GameNames.Add(33, "Alley Cat Keno");
            GameNames.Add(34, "Campanitas");
            GameNames.Add(35, "Polly's Gold");
            GameNames.Add(36, "Keno Wild");
            GameNames.Add(37, "Abejitas");
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)                 
                  return true;
                #else                
                  return false;
                #endif
            }
        }
    }
}
