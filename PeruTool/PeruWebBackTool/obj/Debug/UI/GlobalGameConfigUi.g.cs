﻿#pragma checksum "..\..\..\UI\GlobalGameConfigUi.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9F6063C0B93BDCD69C8F70AF44317E18"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.34014
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PeruWebBackTool.UI {
    
    
    /// <summary>
    /// GlobalGameConfigUi
    /// </summary>
    public partial class GlobalGameConfigUi : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\UI\GlobalGameConfigUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton OpenRadioButton;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\UI\GlobalGameConfigUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton CloseRadioButton;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\UI\GlobalGameConfigUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox DoubleMaxEachBetTextBox;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\UI\GlobalGameConfigUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox DoubleMaxEachPaidTextBox;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\UI\GlobalGameConfigUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TaxEdgeTextBox;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\UI\GlobalGameConfigUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PeruWebBackTool;component/ui/globalgameconfigui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UI\GlobalGameConfigUi.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OpenRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 2:
            this.CloseRadioButton = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 3:
            this.DoubleMaxEachBetTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 13 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.DoubleMaxEachBetTextBox.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CommonTextBox_PreviewKeyDown);
            
            #line default
            #line hidden
            
            #line 13 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.DoubleMaxEachBetTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.CommonTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DoubleMaxEachPaidTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 15 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.DoubleMaxEachPaidTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.CommonTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            
            #line 15 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.DoubleMaxEachPaidTextBox.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CommonTextBox_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.TaxEdgeTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 17 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.TaxEdgeTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.CommonTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            
            #line 17 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.TaxEdgeTextBox.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CommonTextBox_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 6:
            this.SaveButton = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\UI\GlobalGameConfigUi.xaml"
            this.SaveButton.Click += new System.Windows.RoutedEventHandler(this.SaveButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

