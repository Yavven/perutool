﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Request;
using PeruToolClassLibrary;

namespace PeruWebBackTool.BLL
{
    class RequestBll
    {
        #region 登陆
        public static void Login(string username, string password)
        {
            LoginRequest request = new LoginRequest(username, password);
            App.MyTcpClient.Send(WebBackMessageId.LOGIN, request.GetBytes());
        }
        #endregion

        #region 后台管理员
        public static void CreateBackAdmin(string username, string password)
        {
            AdminBackCreateRequest request = new AdminBackCreateRequest(username, password);
            App.MyTcpClient.Send(WebBackMessageId.ADMIN_BACK_CREATE, request.GetBytes());
        }

        public static void EnableBackAdmin(string username, bool enable)
        {
            CommonEnableRequest request = new CommonEnableRequest(username, enable);
            App.MyTcpClient.Send(WebBackMessageId.ADMIN_BACK_ENABLE, request.GetBytes());
        }

        public static void SearchBackAdmin()
        {
            App.MyTcpClient.Send(WebBackMessageId.ADMIN_BACK_SEARCH);
        }
        #endregion

        #region 财务管理员
        public static void CreateFinanceAdmin(string username, string password)
        {
            TowStringRequest request = new TowStringRequest(username, password);
            App.MyTcpClient.Send(WebBackMessageId.ADMIN_FINANCE_CREATE, request.GetBytes());
        }

        public static void EnableFinanceAdmin(string username, bool enable)
        {
            CommonEnableRequest request = new CommonEnableRequest(username, enable);
            App.MyTcpClient.Send(WebBackMessageId.ADMIN_FINANCE_ENABLE, request.GetBytes());
        }

        public static void SearchFinanceAdmin()
        {
            App.MyTcpClient.Send(WebBackMessageId.ADMIN_FINANCE_SEARCH);
        }
        #endregion

        #region 游戏设置
        public static void SearchGameGlobalConfig()
        {
            App.MyTcpClient.Send(WebBackMessageId.GLOBAL_GAME_CONFIG_SEARCH);
        }

        public static void EditGameGlobalConfig(int[] flag, int taxEdge, int doubleStatus, int doubleMaxEachBet, int doubleMaxEachPaid)
        {
            GameConfigGlobalEditRequest request = new GameConfigGlobalEditRequest(flag, taxEdge, doubleStatus, doubleMaxEachBet, doubleMaxEachPaid);
            App.MyTcpClient.Send(WebBackMessageId.GLOBAL_GAME_CONFIG_EDIT, request.GetBytes());
        }

        public static void SearchGameConfig(int gameId)
        {
            GameConfigSearchRequest request = new GameConfigSearchRequest(gameId);
            App.MyTcpClient.Send(WebBackMessageId.GAME_CONFIG_SEARCH, request.GetBytes());
        }

        public static void GameConfigEdit(int gameId, int[] flag, int openStatus, int rachargeRate, double taxRate, int maxBet, int maxPaid, int iniIndex)
        {
            GameConfigEditRequest request = new GameConfigEditRequest(gameId, flag, openStatus, rachargeRate, taxRate, maxBet, maxPaid, iniIndex);
            App.MyTcpClient.Send(WebBackMessageId.GAME_CONFIG_EDIT, request.GetBytes());
        }
        #endregion

        #region 公司信息
        public static void SearchCompanyInfo()
        {
            App.MyTcpClient.Send(WebBackMessageId.COMPANY_INFO_SEARCH);
        }

        public static void EditCompanyInfo(string name, string howToGet, string address, string telphone)
        {
            CompanyInfoEditRequest request = new CompanyInfoEditRequest(name, howToGet, address, telphone);
            App.MyTcpClient.Send(WebBackMessageId.COMPANY_INFO_EDIT, request.GetBytes());
        }
        #endregion

        #region 卡
        public static void SearchCardBalance(double balance)
        {
            CardSearchRequest request = new CardSearchRequest(balance);
            App.MyTcpClient.Send(WebBackMessageId.CARD_BALANCE_SEARCH, request.GetBytes());
        }
        #endregion

        #region 报表
        public static void SearchReport(int messageId, int startTimeStamp, int endTimeStamp)
        {
            TwoIntRequest request = new TwoIntRequest(startTimeStamp, endTimeStamp);
            App.MyTcpClient.Send(messageId, request.GetBytes());
        }

        public static void SearchProfitReport(int startTimeStamp, int endTimeStamp)
        {
            SearchReport(WebBackMessageId.REPORT_PROFIT_SEARCH, startTimeStamp, endTimeStamp);
        }

        public static void SearchFinanceReport(int startTimeStamp, int endTimeStamp)
        {
            SearchReport(WebBackMessageId.REPORT_FINANCE_SEARCH, startTimeStamp, endTimeStamp);
        }

        public static void SearchRecord(int messageId, string username, int startTimeStamp, int endTimeStamp)
        {
            StringIntIntRequest request = new StringIntIntRequest(username, startTimeStamp, endTimeStamp);
            App.MyTcpClient.Send(messageId, request.GetBytes());
        }

        public static void SearchBalanceRecord(string player, int startTimeStamp, int endTimeStamp)
        {
            SearchRecord(WebBackMessageId.RECORD_BALANCE_SEARCH, player, startTimeStamp, endTimeStamp);
        }

        public static void SearchFinanceRecord(string player, int startTimeStamp, int endTimeStamp)
        {
            SearchRecord(WebBackMessageId.RECORD_FINANCE_SEARCH, player, startTimeStamp, endTimeStamp);
        }
        
        public static void SearchGameRecord(string player, int startTimeStamp, int endTimeStamp)
        {
            SearchRecord(WebBackMessageId.RECORD_GAME_SEARCH, player, startTimeStamp, endTimeStamp);
        }
        #endregion
    }
}
