﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using YWNetworkEngine;

namespace PeruWebBackTool.BLL
{
    public class MessageBll:YWResponseBll
    {
        public override void OnMessageResponse(int messageId, byte[] messageBodyBytes)
        {
            object response = null;

            switch (messageId)
            {
                case WebBackMessageId.LOGIN:
                    App.MyLoginWindow.OnMessageResponse(new LoginResponse(messageBodyBytes));
                    break;

                case WebBackMessageId.ADMIN_BACK_SEARCH:
                    response = new AdminBackSearchResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.ADMIN_BACK_CREATE:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.ADMIN_BACK_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.ADMIN_FINANCE_SEARCH:
                    response = new AdminFinanceSearchResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.ADMIN_FINANCE_CREATE:                            
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.ADMIN_FINANCE_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.RECORD_BALANCE_SEARCH:
                    response = new RecordBalanceResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.RECORD_FINANCE_SEARCH:
                    response = new RecordFinanceSearchResponseWeb(messageBodyBytes);
                    break;

                case WebBackMessageId.REPORT_PROFIT_SEARCH:
                    response = new ReportProfitSearchResponseWeb(messageBodyBytes);
                    break;

                case WebBackMessageId.REPORT_FINANCE_SEARCH:
                    response = new ReportFinanceSearchResponseWeb(messageBodyBytes);
                    break;

                case WebBackMessageId.RECORD_GAME_SEARCH:
                    response = new RecordGameSearchResponseWeb(messageBodyBytes);
                    break;

                case WebBackMessageId.GAME_CONFIG_EDIT:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.GAME_CONFIG_SEARCH:
                    response = new GameConfigSearchResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.GLOBAL_GAME_CONFIG_EDIT:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.GLOBAL_GAME_CONFIG_SEARCH:
                    response = new GlobaGamelConfigSearchResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.COMPANY_INFO_SEARCH:
                    response = new CompanyInfoSearchResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.COMPANY_INFO_EDIT:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case WebBackMessageId.CARD_BALANCE_SEARCH:
                    response = new CardBalanceSearchResponse(messageBodyBytes);
                    break;
            }

            if(response != null)
            {
                App.GlobalMainWindow.OnMessageResponse(messageId, response);
            }
        }
    }
}
