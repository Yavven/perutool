﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using PeruWebBackTool.BLL;
using PeruWebBackTool.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeruWebBackTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        UserControl currentUi;

        public MainWindow()
        {
            InitializeComponent();

            if(!App.IsRoot)
            {
                AdminMenuItem.Visibility = Visibility.Collapsed;
                GameConfigGlobalMenuItem.Visibility = Visibility.Collapsed;
                GameConfigMenuItem.Visibility = Visibility.Collapsed;
            }
            else
            {
                RequestBll.SearchBackAdmin();
            }
        }

        public void OnMessageResponse(int messageId, object response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                switch(messageId)
                {
                    #region 管理员
                    case WebBackMessageId.ADMIN_BACK_SEARCH:
                        App.BackAdmins = (response as AdminBackSearchResponse).Admins;

                        if(MyAdminUi.Visibility == Visibility.Visible)
                        {
                            MyAdminUi.OnMessageResponse(response);
                        }

                        break;

                    case WebBackMessageId.ADMIN_BACK_CREATE:
                        if((response as CreateEditResponse).IsSuccess())
                        {
                            RequestBll.SearchBackAdmin();
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case WebBackMessageId.ADMIN_BACK_ENABLE:
                        RequestBll.SearchBackAdmin();
                        break;

                    case WebBackMessageId.ADMIN_FINANCE_SEARCH:
                        if (MyAdminUi.Visibility == Visibility.Visible)
                        {
                            MyAdminUi.OnMessageResponse(response);
                        }

                        break;

                    case WebBackMessageId.ADMIN_FINANCE_CREATE:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            RequestBll.SearchFinanceAdmin();
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case WebBackMessageId.ADMIN_FINANCE_ENABLE:                       
                        RequestBll.SearchFinanceAdmin();
                        break;
                    #endregion

                    #region 卡余额
                    case WebBackMessageId.CARD_BALANCE_SEARCH:
                        App.MyCardBalanceWindow.OnMessageResponse(response);
                        break;
                    #endregion

                    #region 游戏设置
                    case WebBackMessageId.GLOBAL_GAME_CONFIG_SEARCH:
                        MyGlobalGameConfigUi.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.GLOBAL_GAME_CONFIG_EDIT:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case WebBackMessageId.GAME_CONFIG_SEARCH:
                        MyGameConfigUi.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.GAME_CONFIG_EDIT:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;
                    #endregion

                    #region 公司信息
                    case WebBackMessageId.COMPANY_INFO_SEARCH:
                        MyCompanyInfoUi.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.COMPANY_INFO_EDIT:
                        if ((response as CreateEditResponse).IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;
                    #endregion 公司信息

                    #region 报表
                    case WebBackMessageId.REPORT_PROFIT_SEARCH:
                        App.ReportProfitWindow.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.REPORT_FINANCE_SEARCH:
                        App.ReportFinanceWindow.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.RECORD_FINANCE_SEARCH:
                        App.RecordFinanceWindow.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.RECORD_GAME_SEARCH:
                        App.RecordGameWindow.OnMessageResponse(response);
                        break;

                    case WebBackMessageId.RECORD_BALANCE_SEARCH:
                        App.RecordBalanceWindow.OnMessageResponse(response);
                        break;
                    #endregion
                }
            }));           
        }

        public void ShowUi(UserControl ui)
        {
            if (currentUi != null)
            {
                currentUi.Visibility = Visibility.Collapsed;
            }

            ui.Visibility = Visibility.Visible;
            currentUi = ui;
        }

        #region 管理员
        private void BackAdminMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MyAdminUi.SetType(AdminTypes.BACK);
            ShowUi(MyAdminUi);
        }

        private void FinanceAdminMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MyAdminUi.SetType(AdminTypes.FINANCE);
            ShowUi(MyAdminUi);
        }
        #endregion

        #region 报表
        private void ReportProfitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.ReportProfitWindow = new ReportWindow(ReportTypes.REPORT_PROFIT);
            App.ReportProfitWindow.Show();
        }

        private void ReportFinanceMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.ReportFinanceWindow = new ReportWindow(ReportTypes.REPORT_FINANCE);
            App.ReportFinanceWindow.Show();
        }

        private void RecordBalanceMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.RecordBalanceWindow = new ReportWindow(ReportTypes.RECORD_BALANCE);
            App.RecordBalanceWindow.Show();
        }

        private void RecordFinanceMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.RecordFinanceWindow = new ReportWindow(ReportTypes.RECORD_FINANCE);
            App.RecordFinanceWindow.Show();
        }

        private void RecordGameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.RecordGameWindow = new ReportWindow(ReportTypes.RECORD_GAME);
            App.RecordGameWindow.Show();
        }
        #endregion

        #region 游戏设置
        private void GlobalGameConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.SearchGameGlobalConfig();
            ShowUi(MyGlobalGameConfigUi);
        }

        private void GameConfigMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ShowUi(MyGameConfigUi);
        }
        #endregion

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void CardBalanceMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.MyCardBalanceWindow = new CardBalanceWindow();
            App.MyCardBalanceWindow.Show();
        }

        private void CompanyInfoMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.SearchCompanyInfo();
            ShowUi(MyCompanyInfoUi);
        }
    }
}
