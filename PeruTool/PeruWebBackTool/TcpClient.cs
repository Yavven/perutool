﻿using PeruToolClassLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YWNetworkEngine;

namespace PeruWebBackTool
{
    public class PeruTcpClient:YWTcpClient
    {
        public void Send(int messageId)
        {
            Send(messageId, null);
        }

        public void Send(int messageId, byte[] messageBodyBytes)
        {
            byte[] messageHeadBytes;
            byte[] messageIdBytes;
            byte[] randomNumberBytes;
            int randomNumber;
            MemoryStream myMemoryStream;
            Random myRandom;

            messageIdBytes = BitConverter.GetBytes(messageId);
            myRandom = new Random();
            randomNumber = myRandom.Next(32767);
            randomNumberBytes = BitConverter.GetBytes(randomNumber);

            if (messageBodyBytes != null)
            {
                messageHeadBytes = BitConverter.GetBytes(messageIdBytes.Length + randomNumberBytes.Length + messageBodyBytes.Length + MessageHead.MESSAGE_HEAD_LENGTH);
            }
            else
            {
                messageHeadBytes = BitConverter.GetBytes(messageIdBytes.Length + randomNumberBytes.Length + MessageHead.MESSAGE_HEAD_LENGTH);
            }

            Array.Reverse(messageIdBytes);
            Array.Reverse(randomNumberBytes);
            Array.Reverse(messageHeadBytes);
            myMemoryStream = new MemoryStream();
            myMemoryStream.Write(messageHeadBytes, 0, messageHeadBytes.Length);
            myMemoryStream.Write(messageIdBytes, 0, messageIdBytes.Length);
            myMemoryStream.Write(randomNumberBytes, 0, randomNumberBytes.Length);

            if (messageBodyBytes != null)
            {
                myMemoryStream.Write(messageBodyBytes, 0, messageBodyBytes.Length);
            }

            byte[] sendBytes = myMemoryStream.ToArray();
            Send(sendBytes);
            myMemoryStream.Dispose();
        }
    }
}
