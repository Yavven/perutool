﻿using PeruWebBackTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruWebBackTool.UI
{
    /// <summary>
    /// ReportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ReportWindow
    {
        DataTable reportDataTable;
        int type;

        public void OnMessageResponse(object response)
        {
            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    ReportProfitSearchResponseWeb profitReportResponse = response as ReportProfitSearchResponseWeb;

                    foreach (ReportProfitWeb report in profitReportResponse.Reports)
                    {
                        DataRow dataRow = reportDataTable.NewRow();
                        dataRow["AllBet"] = report.AllBet;
                        dataRow["AllPaid"] = report.AllPaid;
                        dataRow["AllTax"] = report.AllTax;
                        dataRow["TesoAllBet"] = report.TesoAllBet;
                        reportDataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.REPORT_FINANCE:
                    ReportFinanceSearchResponseWeb financeReportResponse = response as ReportFinanceSearchResponseWeb;

                    foreach (ReportFinanceWeb report in financeReportResponse.Reports)
                    {
                        DataRow dataRow = reportDataTable.NewRow();
                        dataRow["Deposit"] = report.Deposit;
                        dataRow["Withdraw"] = report.Withdraw;
                        reportDataTable.Rows.Add(dataRow);
                    }

                    break;


                case ReportTypes.RECORD_BALANCE:
                    RecordBalanceResponse balanceRecordResponse = response as RecordBalanceResponse;

                    foreach (BalanceRecord record in balanceRecordResponse.Records)
                    {
                        DataRow dataRow = reportDataTable.NewRow();
                        dataRow["Operator"] = record.Operator;
                        dataRow["Type"] = App.TransferTypeName[record.Type];
                        dataRow["Amount"] = record.Amount;
                        dataRow["Balance"] = record.Balance;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        reportDataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.RECORD_FINANCE:
                    RecordFinanceSearchResponseWeb financeRecordResponse = response as RecordFinanceSearchResponseWeb;

                    foreach (RecordFinanceWeb record in financeRecordResponse.Records)
                    {
                        DataRow dataRow = reportDataTable.NewRow();
                        dataRow["Card"] = record.Card;
                        dataRow["Operator"] = record.Operator;
                        dataRow["Type"] = App.TransferTypeName[record.Type];
                        dataRow["Amount"] = record.Money;
                        dataRow["Balance"] = record.Balance;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        reportDataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.RECORD_GAME:
                    RecordGameSearchResponseWeb cardGameReocrds = response as RecordGameSearchResponseWeb;

                    foreach (RecordGameWeb record in cardGameReocrds.Records)
                    {
                        DataRow dataRow = reportDataTable.NewRow();
                        dataRow["Game"] = App.GameNames[record.GameId];
                        dataRow["Bet"] = record.Bet;
                        dataRow["Paid"] = record.Paid;
                        dataRow["Tax"] = record.Tax;
                        dataRow["Balance"] = record.Balance;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        reportDataTable.Rows.Add(dataRow);
                    }

                    break;
            }

            ReportDataGrid.ItemsSource = reportDataTable.AsDataView();
        }

        public ReportWindow(int reportType)
        {
            InitializeComponent();

            StartDateTimePicker.Value = DateTimeUtil.GetTodayStartDateTime();
            EndDateTimePicker.Value = DateTime.Now;

            reportDataTable = new DataTable();
            type = reportType;

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    Title = "Profit Report";
                    reportDataTable.Columns.Add("AllBet");
                    reportDataTable.Columns.Add("AllPaid");
                    reportDataTable.Columns.Add("AllTax");
                    reportDataTable.Columns.Add("TesoAllBet");
                    break;

                case ReportTypes.REPORT_FINANCE:
                    Title = "Finance Report";
                    reportDataTable.Columns.Add("Deposit");
                    reportDataTable.Columns.Add("Withdraw");
                    break;


                case ReportTypes.RECORD_BALANCE:
                    Title = "Balance Record";
                    PlayerLabel.Content = "Card ID";
                    reportDataTable.Columns.Add("Operator");
                    reportDataTable.Columns.Add("Type");
                    reportDataTable.Columns.Add("Amount");
                    reportDataTable.Columns.Add("Balance");
                    reportDataTable.Columns.Add("Time");
                    break;

                case ReportTypes.RECORD_FINANCE:
                    Title = "Finance Record";
                    PlayerLabel.Content = "Card ID";
                    reportDataTable.Columns.Add("Card");
                    reportDataTable.Columns.Add("Operator");
                    reportDataTable.Columns.Add("Type");
                    reportDataTable.Columns.Add("Amount");
                    reportDataTable.Columns.Add("Balance");
                    reportDataTable.Columns.Add("Time");
                    break;

                case ReportTypes.RECORD_GAME:
                    Title = "Card Game Record";
                    PlayerLabel.Content = "Card ID";
                    reportDataTable.Columns.Add("Game");
                    reportDataTable.Columns.Add("Bet");
                    reportDataTable.Columns.Add("Paid");
                    reportDataTable.Columns.Add("Tax");
                    reportDataTable.Columns.Add("Balance");
                    reportDataTable.Columns.Add("Time");
                    break;
            }

            if (reportType <= ReportTypes.RECORD_FINANCE)
            {
                LocationLabel.Visibility = Visibility.Visible;
                LocationComboBox.Visibility = Visibility.Visible;

                if (App.IsRoot)
                {
                    foreach (AdminBack location in App.BackAdmins)
                    {
                        LocationComboBox.Items.Add(location.Name);
                    }
                }
                else
                {
                    LocationComboBox.Items.Add(App.LoginAdminName);
                }

                LocationComboBox.SelectedIndex = 0;
            }
            else
            {
                LocationLabel.Visibility = Visibility.Collapsed;
                LocationComboBox.Visibility = Visibility.Collapsed;
            }

            if (reportType > ReportTypes.REPORT_FINANCE)
            {
                PlayerLabel.Visibility = Visibility.Visible;
                PlayerTextBox.Visibility = Visibility.Visible;
            }
            else
            {
                PlayerLabel.Visibility = Visibility.Collapsed;
                PlayerTextBox.Visibility = Visibility.Collapsed;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            reportDataTable.Clear();
            ReportDataGrid.ItemsSource = null;

            string location = string.Empty;
            string player = string.Empty;
            int startTimeStamp = DateTimeUtil.ConvertDateTimeToInt(StartDateTimePicker.Value.Value);
            int endTimeStamp = DateTimeUtil.ConvertDateTimeToInt(EndDateTimePicker.Value.Value);

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    RequestBll.SearchProfitReport(startTimeStamp, endTimeStamp);
                    break;

                case ReportTypes.REPORT_FINANCE:
                    RequestBll.SearchFinanceReport(startTimeStamp, endTimeStamp);
                    break;

                case ReportTypes.RECORD_BALANCE:
                    player = PlayerTextBox.Text;
                    RequestBll.SearchBalanceRecord(player, startTimeStamp, endTimeStamp);
                    break;


                case ReportTypes.RECORD_FINANCE:
                    player = PlayerTextBox.Text;
                    RequestBll.SearchFinanceRecord(player, startTimeStamp, endTimeStamp);
                    break;

                case ReportTypes.RECORD_GAME:
                    player = PlayerTextBox.Text;
                    RequestBll.SearchGameRecord(player, startTimeStamp, endTimeStamp);
                    break;
            }
        }

        private void PlayerTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }

        private void PlayerTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}
