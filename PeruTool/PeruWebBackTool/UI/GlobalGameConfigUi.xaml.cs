﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeruToolClassLibrary.Response;
using PeruToolClassLibrary;
using PeruWebBackTool.BLL;
using YWCommonUtil;

namespace PeruWebBackTool.UI
{
    /// <summary>
    /// GameConfigUi.xaml 的交互逻辑
    /// </summary>
    public partial class GlobalGameConfigUi : UserControl
    {
        RadioButton[] radioButtons = new RadioButton[2];
        GlobaGamelConfigSearchResponse oldData;

        public void OnMessageResponse(object response)
        {
            oldData = response as GlobaGamelConfigSearchResponse;

            if (oldData.DoubleStatus == StatusTypes.DISABLE)
            {
                CloseRadioButton.IsChecked = true;
            }
            else
            {
                OpenRadioButton.IsChecked = true;
            }

            TaxEdgeTextBox.Text = oldData.TaxEdge.ToString();
            DoubleMaxEachBetTextBox.Text = oldData.DoubleMaxEachBet.ToString();
            DoubleMaxEachPaidTextBox.Text = oldData.DoubleMaxEachPaid.ToString();
        }

        public GlobalGameConfigUi()
        {
            InitializeComponent();

            radioButtons[0] = CloseRadioButton;
            radioButtons[1] = OpenRadioButton;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (TaxEdgeTextBox.Text == string.Empty)
            {
                return;
            }

            if (DoubleMaxEachBetTextBox.Text == string.Empty)
            {
                return;
            }

            if (OpenRadioButton.IsChecked == false && CloseRadioButton.IsChecked == false)
            {
                return;
            }

            if (DoubleMaxEachPaidTextBox.Text == string.Empty)
            {
                return;
            }

            int[] flag = new int[4];
            int taxEdge = Convert.ToInt32(TaxEdgeTextBox.Text);
            int openStatus = OpenRadioButton.IsChecked == true ? 1 : 0;
            int doubleMaxEachBet = Convert.ToInt32(DoubleMaxEachBetTextBox.Text);
            int doubleMaxEachPaid = Convert.ToInt32(DoubleMaxEachPaidTextBox.Text);

            if(oldData.TaxEdge != taxEdge)
            {
                flag[0] = 1;
            }

            if(oldData.DoubleStatus != openStatus)
            {
                flag[1] = 1;
            }

            if (oldData.DoubleMaxEachBet != doubleMaxEachBet)
            {
                flag[2] = 1;
            }

            if (oldData.DoubleMaxEachPaid != doubleMaxEachPaid)
            {
                flag[3] = 1;
            }

            RequestBll.EditGameGlobalConfig(flag, taxEdge, openStatus, doubleMaxEachBet, doubleMaxEachPaid);
        }

        private void CommonTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void CommonTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsInteger(e.Text))
            {
                e.Handled = true;
            }
        }
    }
}
