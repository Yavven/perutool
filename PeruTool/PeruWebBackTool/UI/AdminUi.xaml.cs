﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using PeruWebBackTool.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruWebBackTool.UI
{
    /// <summary>
    /// LocationUi.xaml 的交互逻辑
    /// </summary>
    public partial class AdminUi : UserControl
    {
        DataTable adminDataTable;
        bool isEnable;
        int type;

        public void OnMessageResponse(object response)
        {
            switch (type)
            {
                case AdminTypes.FINANCE:
                    AdminFinanceSearchResponse searchFinanceAdminResponse = response as AdminFinanceSearchResponse;

                    foreach (AdminFinance admin in searchFinanceAdminResponse.Admins)
                    {
                        if (admin.Name == "root")
                        {
                            continue;
                        }

                        DataRow dataRow = adminDataTable.NewRow();
                        dataRow["Name"] = admin.Name;
                        dataRow["Status"] = admin.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(admin.TimeStamp);
                        adminDataTable.Rows.Add(dataRow);
                    }

                    break;

                case AdminTypes.BACK:
                    AdminBackSearchResponse searchBackAdminResponse = response as AdminBackSearchResponse;

                    foreach (AdminBack backAdmin in searchBackAdminResponse.Admins)
                    {
                        if (backAdmin.Name == "root")
                        {
                            continue;
                        }

                        DataRow dataRow = adminDataTable.NewRow();
                        dataRow["Name"] = backAdmin.Name;
                        dataRow["Status"] = backAdmin.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(backAdmin.TimeStamp);
                        adminDataTable.Rows.Add(dataRow);
                    }

                    break;
            }

            DataGridAdmin.ItemsSource = adminDataTable.AsDataView();
        }

        public void SetType(int adminType)
        {
            type = adminType;

            TitleLabel.Content = App.AdminTypeNames[adminType];
            DataGridAdmin.ItemsSource = null;
        }

        public AdminUi()
        {
            InitializeComponent();

            adminDataTable = new DataTable();
            adminDataTable.Columns.Add("Name");
            adminDataTable.Columns.Add("Status");
            adminDataTable.Columns.Add("Time");
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AdminAddWindow accountAddWindow = new AdminAddWindow(type);
            accountAddWindow.Show();
        }

        private void EnableButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataGridAdmin.SelectedItem == null)
            {
                return;
            }

            DataRowView selectedDataRowView = (DataRowView)DataGridAdmin.SelectedItem;
            string username = selectedDataRowView.Row["Name"].ToString();

            switch (type)
            {
                case AdminTypes.BACK:
                    RequestBll.EnableBackAdmin(username, !isEnable);
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.EnableFinanceAdmin(username, !isEnable);
                    break;
            }
        }

        private void DataGridAdmin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataGridAdmin.SelectedItem == null)
            {
                return;
            }

            DataRowView selectedDataRowView = null;

            try
            {
                selectedDataRowView = (DataRowView)DataGridAdmin.SelectedItem;
            }
            catch
            {
                return;
            }

            string status = selectedDataRowView.Row["Status"].ToString();

            if (status == "Enable")
            {
                EnableButton.Content = "Disable";
                isEnable = true;
            }
            else
            {
                EnableButton.Content = "Enable";
                isEnable = false;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            adminDataTable.Clear();
            DataGridAdmin.ItemsSource = null;

            switch (type)
            {

                case AdminTypes.BACK:
                    RequestBll.SearchBackAdmin();
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.SearchFinanceAdmin();
                    break;
            }
        }
    }
}
