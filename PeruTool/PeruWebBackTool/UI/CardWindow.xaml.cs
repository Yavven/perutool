﻿using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using PeruWebBackTool.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruWebBackTool.UI
{
    /// <summary>
    /// CardBalanceFilterWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CardBalanceWindow
    {
        DataTable cardDataTable = new DataTable();

        public void OnMessageResponse(object responseObject)
        {
            CardBalanceSearchResponse response = responseObject as CardBalanceSearchResponse;

            foreach (CardBalance cardBalance in response.CardBalances)
            {
                DataRow dataRow = cardDataTable.NewRow();
                dataRow["Card"] = cardBalance.CardId;
                dataRow["Balance"] = cardBalance.Balance;
                cardDataTable.Rows.Add(dataRow);
            }

            CardBalanceDataGrid.ItemsSource = cardDataTable.AsDataView();
        }

        public CardBalanceWindow()
        {
            InitializeComponent();

            cardDataTable.Columns.Add("Card");
            cardDataTable.Columns.Add("Balance");
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            cardDataTable.Clear();
            CardBalanceDataGrid.ItemsSource = null;

            double balance = 0;

            if (BalanceTextBox.Text != string.Empty)
            {
                balance = Convert.ToDouble(BalanceTextBox.Text);
            }

            RequestBll.SearchCardBalance(balance);
        }

        private void BalanceTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsNumber(e.Text))
            {
                e.Handled = true;
            }
        }

        private void BalanceTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void BalanceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double balance = Convert.ToDouble(BalanceTextBox.Text);

                if (balance < 0)
                {
                    BalanceTextBox.Text = "0";
                }
            }
            catch
            {
                BalanceTextBox.Text = "0";
            }
        }
    }
}
