﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruWebBackTool.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruWebBackTool.UI
{
    /// <summary>
    /// AddWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AdminAddWindow
    {
        int type;

        public AdminAddWindow(int adminType)
        {
            InitializeComponent();

            type = adminType;

            Title = "New " + App.AdminTypeNames[adminType];
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Text;
            string location = string.Empty;

            if (string.IsNullOrEmpty(username))
            {
                MessageBox.Show("Username is empty!");
                return;
            }

            if (string.IsNullOrEmpty(password))
            {
                MessageBox.Show("Password is empty!");
                return;
            }

            switch (type)
            {
                case AdminTypes.BACK:
                    RequestBll.CreateBackAdmin(username, password);
                    break;

                case AdminTypes.FINANCE:
                    RequestBll.CreateFinanceAdmin(username, password);
                    break;
            }

            Hide();
        }

        private void UsernameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void UsernameTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }

        private void PasswordTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }
    }
}
