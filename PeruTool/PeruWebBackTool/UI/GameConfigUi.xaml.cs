﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeruToolClassLibrary.Response;
using PeruToolClassLibrary;
using PeruWebBackTool.BLL;
using YWCommonUtil;

namespace PeruWebBackTool.UI
{
    /// <summary>
    /// GameConfigUi.xaml 的交互逻辑
    /// </summary>
    public partial class GameConfigUi : UserControl
    {
        RadioButton[] radioButtons = new RadioButton[2];
        GameConfigSearchResponse oldData;

        public void OnMessageResponse(object response)
        {
            oldData = response as GameConfigSearchResponse;

            if (oldData.OpenStatus == StatusTypes.DISABLE)
            {
                CloseRadioButton.IsChecked = true;
            }
            else
            {
                OpenRadioButton.IsChecked = true;
            }

            RechargeRateTextBox.Text = oldData.RechargeRate.ToString();
            TaxRateTextBox.Text = oldData.TaxRate.ToString();
            MaxBetTextBox.Text = oldData.MaxBet.ToString();
            MaxPaidTextBox.Text = oldData.MaxPaid.ToString();
            PaytableComboBox.ItemsSource = oldData.Paytables;
            PaytableComboBox.SelectedValue = oldData.CurrentPaytableIndex;
        }

        public GameConfigUi()
        {
            InitializeComponent();

            radioButtons[0] = CloseRadioButton;
            radioButtons[1] = OpenRadioButton;

            GameComboBox.ItemsSource = App.GameNames;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if(GameComboBox.SelectedValue == null)
            {
                return;
            }
            else
            {
                if(RechargeRateTextBox.Text == string.Empty)
                {
                    return;
                }

                if (TaxRateTextBox.Text == string.Empty)
                {
                    return;
                }

                if (MaxBetTextBox.Text == string.Empty)
                {
                    return;
                }

                if (MaxPaidTextBox.Text == string.Empty)
                {
                    return;
                }
            }

            int gameId = (int)GameComboBox.SelectedValue;
            int[] flag = new int[6];
            int openStatus = OpenRadioButton.IsChecked == true ? 1:0;
            int rechargeRate = Convert.ToInt32(RechargeRateTextBox.Text);
            double taxRate = Convert.ToDouble(TaxRateTextBox.Text);
            int maxBet = Convert.ToInt32(MaxBetTextBox.Text);
            int maxPaid = Convert.ToInt32(MaxPaidTextBox.Text);
            int paytableIndex = (int)PaytableComboBox.SelectedValue;

            if(oldData.OpenStatus != openStatus)
            {
                flag[0] = 1;
            }

            if(oldData.RechargeRate != rechargeRate)
            {
                flag[1] = 1;
            }

            if (oldData.TaxRate != taxRate)
            {
                flag[2] = 1;
            }

            if (oldData.MaxBet != maxBet)
            {
                flag[3] = 1;
            }

            if (oldData.MaxPaid != maxPaid)
            {
                flag[4] = 1;
            }

            if (oldData.CurrentPaytableIndex != paytableIndex)
            {
                flag[5] = 1;
            }

            RequestBll.GameConfigEdit(gameId, flag, openStatus, rechargeRate, taxRate, maxBet, maxPaid, paytableIndex);
        }

        private void PaytableComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PaytableComboBox.SelectedValue != null)
            {
                oldData.CurrentPaytableIndex = (int)PaytableComboBox.SelectedValue;
            }
        }

        private void GameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int gameId = (int)GameComboBox.SelectedValue;
            RequestBll.SearchGameConfig(gameId);
        }

        private void CommonTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsInteger(e.Text))
            {
                e.Handled = true;
            }
        }

        private void CommonTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void TaxRateTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Back && e.Key != Key.OemPeriod)
            {
                if ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9))
                {
                    e.Handled = true;
                    return;
                }

                int dotIndex = TaxRateTextBox.Text.IndexOf('.');

                if (dotIndex > 0)
                {
                    if (TaxRateTextBox.Text.Length - dotIndex > 3)
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }
            else
            {
                if (e.Key == Key.OemPeriod)
                {
                    if (TaxRateTextBox.Text.IndexOf('.') >= 0)
                    {
                        e.Handled = true;
                        return;
                    }

                    if (TaxRateTextBox.SelectionStart == 0)
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }
        }

        private void TaxRateTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(TaxRateTextBox.Text != string.Empty)
            {
                double taxRate = Convert.ToDouble(TaxRateTextBox.Text);
                {
                    if (taxRate < 0)
                    {
                        TaxRateTextBox.Text = "0.000";
                    }

                    if (taxRate >= 1)
                    {
                        TaxRateTextBox.Text = "0.999";
                    }
                }
            }
        }
    }
}
