﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class RecordGameSearchResponseWeb:BaseReponse
    {
        public List<RecordGameWeb> Records = new List<RecordGameWeb>();

        public RecordGameSearchResponseWeb(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                RecordGameWeb report = new RecordGameWeb(Bytes, ref Offset);
                Records.Add(report);
            }
        }
    }
}
