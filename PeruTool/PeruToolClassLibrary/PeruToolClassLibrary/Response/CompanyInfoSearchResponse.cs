﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class CompanyInfoSearchResponse : BaseReponse
    {
        public string Name;
        public string HowToGet;
        public string Address;
        public string Telphone;

        public CompanyInfoSearchResponse(byte[] bytes)
        {
            Bytes = bytes;

            Name = ReadString();
            HowToGet = ReadString();
            Address = ReadString();
            Telphone = ReadString();
        }
    }
}
