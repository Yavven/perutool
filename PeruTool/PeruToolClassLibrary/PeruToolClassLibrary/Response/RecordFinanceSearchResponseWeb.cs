﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class RecordFinanceSearchResponseWeb:BaseReponse
    {
        public List<RecordFinanceWeb> Records = new List<RecordFinanceWeb>();

        public RecordFinanceSearchResponseWeb(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                RecordFinanceWeb report = new RecordFinanceWeb(Bytes, ref Offset);
                Records.Add(report);
            }
        }
    }
}
