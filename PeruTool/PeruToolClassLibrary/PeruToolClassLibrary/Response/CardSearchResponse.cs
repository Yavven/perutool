﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class CommonListResponse : BaseReponse
    {
        public List<CommonAccount> Cards = new List<CommonAccount>();

        public CommonListResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                CommonAccount card = new CommonAccount(Bytes, ref Offset);
                Cards.Add(card);
            }
        }
    }
}
