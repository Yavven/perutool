﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class TransferResponse : BaseReponse
    {
        public string CardId;
        public int Flag;
        public double Balance;

        public TransferResponse(byte[] bytes)
        {
            Bytes = bytes;

            CardId = ReadString();
            Flag = ReadInt();
            Balance = ReadDouble();
        }
    }
}
