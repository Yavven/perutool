﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class EnableResponse : BaseReponse
    {
        public string Name;
        public bool Enable;

        public EnableResponse(byte[] bytes)
        {
            Bytes = bytes;
            Name = ReadString();
            Enable = ReadInt() == StatusTypes.ENABLE ? true : false;
        }
    }
}
