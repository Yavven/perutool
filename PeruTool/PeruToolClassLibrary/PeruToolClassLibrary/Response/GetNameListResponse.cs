﻿using PeruToolClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class GetNameListResponse:BaseReponse
    {
        public List<NameModel> Names = new List<NameModel>();

        public GetNameListResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                NameModel name = new NameModel(Bytes, ref Offset);
                Names.Add(name);
            }
        }
    }
}
