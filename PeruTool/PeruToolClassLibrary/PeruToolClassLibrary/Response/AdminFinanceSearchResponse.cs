﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class AdminFinanceSearchResponse : BaseReponse
    {
        public List<AdminFinance> Admins = new List<AdminFinance>();

        public AdminFinanceSearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                AdminFinance employee = new AdminFinance(Bytes, ref Offset);
                Admins.Add(employee);
            }
        }
    }
}
