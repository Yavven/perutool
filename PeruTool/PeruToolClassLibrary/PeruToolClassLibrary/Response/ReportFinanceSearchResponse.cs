﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class FinanceReportSearchResponse:BaseReponse
    {
        public List<FinanceReport> Reports = new List<FinanceReport>();

        public FinanceReportSearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                FinanceReport reportFinance = new FinanceReport(Bytes, ref Offset);
                Reports.Add(reportFinance);
            }
        }
    }
}
