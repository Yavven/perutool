﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class AdminBackSearchResponse:BaseReponse
    {
        public List<AdminBack> Admins = new List<AdminBack>();

        public AdminBackSearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                AdminBack location = new AdminBack(Bytes, ref Offset);
                Admins.Add(location);
            }
        }
    }
}
