﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class TransactionConfirmResponse:BaseReponse
    {
        public string CardId;
        public int Flag;
        public double Balance;

        public TransactionConfirmResponse(byte[] bytes)
        {
            Bytes = bytes;

            CardId = ReadString();
            Flag = ReadInt();
            Balance = ReadDouble();
        }

        public bool IsSuccess()
        {
            if(Flag == FlagTypes.FAIL)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
