﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class GameConfigSearchResponse:BaseReponse
    {
        public int OpenStatus;
        public int RechargeRate;
        public double TaxRate;
        public int MaxBet;
        public int MaxPaid;
        public int CurrentPaytableIndex;
        public List<Paytable> Paytables;

        public GameConfigSearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;
            Paytables = new List<Paytable>();

            OpenStatus = ReadInt();
            RechargeRate = ReadInt();
            TaxRate = ReadDouble();
            MaxBet = ReadInt();
            MaxPaid = ReadInt();
            CurrentPaytableIndex = ReadInt();

            int paytableCount = ReadInt();
            for(int i = 0; i < paytableCount; i++)
            {
                Paytable paytable = new Paytable(bytes, ref Offset);
                Paytables.Add(paytable);
            }
        }

        public bool IsGameOpen()
        {
            if(OpenStatus == StatusTypes.DISABLE)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
