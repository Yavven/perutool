﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class CreateEditResponse : BaseReponse
    {
        public int Flag;

        public CreateEditResponse(byte[] bytes)
        {
            Bytes = bytes;
            Flag = ReadInt();
        }

        public bool IsSuccess()
        {
            if(Flag == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
