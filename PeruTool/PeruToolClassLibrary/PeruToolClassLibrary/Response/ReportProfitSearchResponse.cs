﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class ReportProfitResponse:BaseReponse
    {
        public List<ReportProfit> Reports = new List<ReportProfit>();

        public ReportProfitResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                ReportProfit reportProfit = new ReportProfit(Bytes, ref Offset);
                Reports.Add(reportProfit);
            }
        }
    }
}
