﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class GetOperatorLogResponse:BaseReponse
    {
        public List<OperatorLog> Logs = new List<OperatorLog>();

        public GetOperatorLogResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                OperatorLog log = new OperatorLog(Bytes, ref Offset);
                Logs.Add(log);
            }
        }
    }
}
