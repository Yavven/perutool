﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class ReportProfitSearchResponseWeb:BaseReponse
    {
        public List<ReportProfitWeb> Reports = new List<ReportProfitWeb>();

        public ReportProfitSearchResponseWeb(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;
                
            ReportProfitWeb reportProfit = new ReportProfitWeb(Bytes, ref Offset);      
            Reports.Add(reportProfit);
        }
    }
}
