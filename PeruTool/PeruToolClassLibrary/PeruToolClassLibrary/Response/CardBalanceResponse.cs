﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class BalanceResponse : BaseReponse
    {
        public string CardId;
        public double Balance;
        public double TotalMoney;

        public BalanceResponse(byte[] bytes)
        {
            Bytes = bytes;

            CardId = ReadString();
            Balance = ReadDouble();
            TotalMoney = ReadDouble();
        }
    }
}
