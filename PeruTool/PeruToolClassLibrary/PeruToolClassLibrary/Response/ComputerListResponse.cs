﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class ComputerListResponse : BaseReponse
    {
        public List<Computer> Computers = new List<Computer>();
        public Dictionary<string, Computer> ComputerDictionary;

        public ComputerListResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            ComputerDictionary = new Dictionary<string, Computer>();

            for(int i = 0; i < count ; i++)
            {
                Computer computer = new Computer(Bytes, ref Offset);
                Computers.Add(computer);
                ComputerDictionary.Add(computer.Name, computer);
            }
        }
    }
}
