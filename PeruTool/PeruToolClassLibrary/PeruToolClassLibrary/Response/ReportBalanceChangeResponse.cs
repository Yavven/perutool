﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class RecordBalanceResponse:BaseReponse
    {
        public List<BalanceRecord> Records = new List<BalanceRecord>();

        public RecordBalanceResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                BalanceRecord report = new BalanceRecord(Bytes, ref Offset);
                Records.Add(report);
            }
        }
    }
}
