﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class CodeUserSearchResponse:BaseReponse
    {
        public string CardId;

        public double TotalAmount;

        public string Name;

        public string Surname;

        public string Address;

        public string IdNumber;

        public CodeUserSearchResponse(byte[] bytes)
        {
            Bytes = bytes;

            CardId = ReadString();
            TotalAmount = ReadDouble();
            Name = ReadString();
            Surname = ReadString();
            Address = ReadString();
            IdNumber = ReadString();
        }
    }
}
