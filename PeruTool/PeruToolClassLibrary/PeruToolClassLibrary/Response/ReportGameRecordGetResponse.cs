﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class GameRecordResponse:BaseReponse
    {
        public List<ReportGameRecord> Records = new List<ReportGameRecord>();

        public GameRecordResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                ReportGameRecord report = new ReportGameRecord(Bytes, ref Offset);
                Records.Add(report);
            }
        }
    }
}
