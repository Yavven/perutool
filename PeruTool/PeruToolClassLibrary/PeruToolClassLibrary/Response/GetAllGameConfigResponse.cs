﻿using PeruToolClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class GetAllGameConfigResponse:BaseReponse
    {
        public List<SampleGameConfig> Configs = new List<SampleGameConfig>();

        public GetAllGameConfigResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                SampleGameConfig gameConfig = new SampleGameConfig(Bytes, ref Offset);
                Configs.Add(gameConfig);
            }
        }
    }
}
