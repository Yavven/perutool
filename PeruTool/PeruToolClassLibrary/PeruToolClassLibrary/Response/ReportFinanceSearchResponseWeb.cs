﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class ReportFinanceSearchResponseWeb:BaseReponse
    {
        public List<ReportFinanceWeb> Reports = new List<ReportFinanceWeb>();

        public ReportFinanceSearchResponseWeb(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;
         
            ReportFinanceWeb report = new ReportFinanceWeb(Bytes, ref Offset);   
            Reports.Add(report);
        }
    }
}
