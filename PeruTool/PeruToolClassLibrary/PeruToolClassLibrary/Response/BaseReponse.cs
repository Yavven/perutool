﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YWCommonUtil;

namespace PeruToolClassLibrary.Response
{
    public class BaseReponse
    {
        protected byte[] Bytes;
        protected int Offset;

        public string ReadString()
        {
            return ByteUtil.ConvertBigEndianBytesToString(ref Offset, Bytes);
        }

        public int ReadInt()
        {
            return ByteUtil.ConvertBigEndianBytesToInt(ref Offset, Bytes);
        }

        public double ReadDouble()
        {
            return ByteUtil.ConvertBigEndianBytesToDouble(ref Offset, Bytes);
        }

        public bool ReadBool()
        {
            return ByteUtil.ConvertBigEndianBytesToBool(ref Offset, Bytes);
        }

        public long ReadLong()
        {
            return ByteUtil.ConvertBigEndianBytesToLong(ref Offset, Bytes);
        }
    }
}
