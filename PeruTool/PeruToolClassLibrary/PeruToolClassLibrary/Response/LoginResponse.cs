﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class LoginResponse:BaseReponse
    {
        public int Flag;

        public LoginResponse(byte[] bytes)
        {
            Bytes = bytes;
            Flag = ReadInt();
        }

        public bool IsSuccess()
        {
            if(Flag == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsRoot()
        {
            if(Flag == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
