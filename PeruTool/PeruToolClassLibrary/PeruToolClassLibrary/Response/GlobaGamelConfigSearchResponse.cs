﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class GlobaGamelConfigSearchResponse:BaseReponse
    {
        public int TaxEdge;
        public int DoubleStatus;
        public int DoubleMaxEachBet;
        public int DoubleMaxEachPaid; 

        public GlobaGamelConfigSearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            TaxEdge = ReadInt();
            DoubleStatus = ReadInt();
            DoubleMaxEachBet = ReadInt();
            DoubleMaxEachPaid = ReadInt();
        }
    }
}
