﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class CommonEnableResponse : BaseReponse
    {
        public string Name;
        public int Flag;

        public CommonEnableResponse(byte[] bytes)
        {
            Bytes = bytes;

            Name = ReadString();
            Flag = ReadInt();
        }

        public bool IsSuccess()
        {
            if(Flag == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
