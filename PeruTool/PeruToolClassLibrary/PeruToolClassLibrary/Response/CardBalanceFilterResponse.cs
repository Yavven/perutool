﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class CardBalanceSearchResponse : BaseReponse
    {
        public List<CardBalance> CardBalances = new List<CardBalance>();

        public CardBalanceSearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                CardBalance cardBalance = new CardBalance(Bytes, ref Offset);
                CardBalances.Add(cardBalance);
            }
        }
    }
}
