﻿using PeruToolClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Response
{
    public class TransactionHistorySearchResponse:BaseReponse
    {
        public List<TransactionHistory> Historys = new List<TransactionHistory>();

        public TransactionHistorySearchResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                TransactionHistory history = new TransactionHistory(Bytes, ref Offset);
                Historys.Add(history);
            }
        }
    }
}
