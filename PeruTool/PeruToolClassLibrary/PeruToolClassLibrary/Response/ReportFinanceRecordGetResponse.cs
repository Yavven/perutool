﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Model;

namespace PeruToolClassLibrary.Response
{
    public class FinanceRecordResponse:BaseReponse
    {
        public List<FinanceRecord> Records = new List<FinanceRecord>();

        public FinanceRecordResponse(byte[] bytes)
        {
            Bytes = bytes;
            Offset = 0;

            int count = ReadInt();

            for(int i = 0; i < count ; i++)
            {
                FinanceRecord report = new FinanceRecord(Bytes, ref Offset);
                Records.Add(report);
            }
        }
    }
}
