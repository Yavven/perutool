﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class MessageHead
    {
        public const int MESSAGE_HEAD_LENGTH = 4;
        public const int RANDOM_NUMBER_OFFSET = 4;
    }
}
