﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class WebBackMessageId
    {
        public const int ADMIN_FINANCE_CREATE = 1031;
        public const int ADMIN_FINANCE_ENABLE = 1032;
        public const int ADMIN_FINANCE_SEARCH = 1033;

        public const int ADMIN_BACK_CREATE = 1021;
        public const int ADMIN_BACK_ENABLE = 1022;
        public const int ADMIN_BACK_SEARCH = 1023;

        public const int CARD_BALANCE_SEARCH = 1081;

        public const int COMPANY_INFO_SEARCH = 1071;
        public const int COMPANY_INFO_EDIT = 1072;

        public const int GAME_CONFIG_SEARCH = 1063;
        public const int GAME_CONFIG_EDIT = 1064;
        public const int GLOBAL_GAME_CONFIG_SEARCH = 1061;
        public const int GLOBAL_GAME_CONFIG_EDIT = 1062;

        public const int LOGIN = 1001;
        public const int LOGOUT = 0;

        public const int RECORD_BALANCE_SEARCH = 1121;
        public const int RECORD_FINANCE_SEARCH = 1131;
        public const int RECORD_GAME_SEARCH = 1151;

        public const int REPORT_FINANCE_SEARCH = 1111;
        public const int REPORT_PROFIT_SEARCH = 1101;
    }
}
