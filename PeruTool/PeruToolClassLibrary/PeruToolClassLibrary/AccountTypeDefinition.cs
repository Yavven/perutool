﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class AdminTypes
    {
        public const int BACK = 0;
        public const int ADMIN_CARD = 1;
        public const int FINANCE = 2;
        public const int PLAYER_COMPUTER = 3;
    }
}
