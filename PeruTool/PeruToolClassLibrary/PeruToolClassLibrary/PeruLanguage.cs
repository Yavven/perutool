﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class PeruLanguage
    {
        public const int LANGUAGE_ENGLISH = 0;
        public const int LANGUAGE_SPANISH = 1;

        int languageId;

        public PeruLanguage(int languageId)
        {
            this.languageId = languageId;
        }

        public string LoginPage()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Acceso";

                default:
                    return "LoginPage";
            }
        }

        public string Computer()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Computadora";

                default:
                    return "Computer";
            }
        }

        public string GetReport()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "CONSULTAR";

                default:
                    return "Get Report";
            }
        }

        public string User()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Usuario";

                default:
                    return "User";
            }
        }

        public string Password()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Clave";

                default:
                    return "Password";
            }
        }

        public string LogIn()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Acceder";

                default:
                    return "Log In";
            }
        }

        public string VirtualCashier()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Cajero Virtual";

                default:
                    return "Virtual Cashier";
            }
        }

        public string FinanceTool()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "CAJERO";

                default:
                    return "Finance Tool";
            }
        }

        public string TransactionHistory()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Historico de transacciones";

                default:
                    return "Transaction History";
            }
        }

        public string ManageAccount()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Administracion de Cuenta";

                default:
                    return "Manage Account";
            }
        }

        public string LogOut()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Salir";

                default:
                    return "Log Out";
            }
        }

        public string DepositWithDraw()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Depositos / Retiros";

                default:
                    return "Deposit / WithDraw";
            }
        }

        public string Sequence()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Secuencio";

                default:
                    return "Sequence";
            }
        }

        public string CardId()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Id Tarjeto";

                default:
                    return "Card Id";
            }
        }

        public string Time()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Fecha";

                default:
                    return "Time";
            }
        }

        public string Type()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Tipo";

                default:
                    return "Type";
            }
        }

        public string Amount()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Cantidad";

                default:
                    return "Amount";
            }
        }

        public string Deposit()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Deposito";

                default:
                    return "Deposit";
            }
        }

        public string WithDraw()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Retiro";

                default:
                    return "Withdraw";
            }
        }

        public string Cancel()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Cancelar";

                default:
                    return "Cancel";
            }
        }

        public string CodeUser()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Codigo Usuario";

                default:
                    return "Code User";
            }
        }

        public string Search()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Buscar";

                default:
                    return "Search";
            }
        }

        public string AccountInformation()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Informacion de la Cuenta";

                default:
                    return "Account Information";
            }
        }

        public string Name()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Nombre";

                default:
                    return "Name";
            }
        }

        public string Surname()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Apellidos";

                default:
                    return "Surname";
            }
        }

        public string Address()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Direccion";

                default:
                    return "Address";
            }
        }

        public string IdNumber()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Numero de DNI";

                default:
                    return "Id Number";
            }
        }

        public string TotalAmount()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Balance";

                default:
                    return "Total Amount";
            }
        }

        public string Fecha()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Fecha";

                default:
                    return "Fecha";
            }
        }

        public string MoneyReport()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Reporte de dinero";

                default:
                    return "Money Report";
            }
        }

        public string CashierInfo()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Informacion de Cajero";

                default:
                    return "Cashier Info";
            }
        }

        public string GameHistory()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Historico de Jugadas";

                default:
                    return "Game History";
            }
        }

        public string ByComputerName()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Por Nombre de Computadora";

                default:
                    return "By Computer Name";
            }
        }


        public string ByCardId()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Por Id de Tarjeta";

                default:
                    return "By Card Id";
            }
        }

        public string GetCardInfo()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Mostrar Datos";

                default:
                    return "Get Card Info";
            }
        }

        public string TodayMoney()
        {
            switch (languageId)
            {
                case LANGUAGE_SPANISH:
                    return "Depósitos del dia";

                default:
                    return "Today Money";
            }
        }
    }
}
