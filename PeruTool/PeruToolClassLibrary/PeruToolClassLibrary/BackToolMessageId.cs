﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class BackToolMessageId
    {
        public const int ADMIN_CARD = 1053;
        public const int ADMIN_CARD_ADD = 1051;
        public const int ADMIN_CARD_ENABLE = 1052;

        public const int ADMIN_FINANCE = 1012;
        public const int ADMIN_FINANCE_ADD = 1031;
        public const int ADMIN_FINANCE_ENABLE = 1032;
        public const int CHANGE_FINANCE_PASSWORD = 1033;

        public const int ADMIN_LOCATION = 1011;
        public const int ADMIN_LOCATION_ADD = 1021;
        public const int ADMIN_LOCATION_ENABLE = 1022;
        public const int CHANGE_LOCATION_PASSWORD = 1023;
 
        public const int CARD_BALANCE_FILTER = 1081;

        public const int COMPUTER = 1013;
        public const int COMPUTER_ADD = 1041;
        public const int COMPUTER_ENABLE = 1042;
        public const int COMPUTER_ENABLCE_10C = 1043;

        public const int GAME_CONFIG = 1063;
        public const int GAME_CONFIG_EDIT = 1064;
        public const int GAME_GLOBAL_CONFIG = 1061;
        public const int GAME_GLOBAL_CONFIG_EDIT = 1062;
        public const int SEARCH_ALL_GAME_CONFIG = 1065;
 

        public const int LOGOUT = 0;
        public const int LOGIN = 1001;

        public const int REPORT_BALANCE_CHANGE_GET = 1121;
        public const int RECORD_FINANCE_SEARCH = 1131;
        public const int RECORD_GAME_SEARCH_BY_CARD = 1151;
        public const int RECORD_GAME_SEARCH_BY_COMPUTER = 1141;
        public const int RECORD_CASHIER = 1132;

        public const int REPORT_FINANCE_SEARCH = 1111;
        public const int REPORT_PROFIT_SEARCH = 1101;

        public const int COMPANY_INFO = 1071;
        public const int COMPANY_INFO_EDIT = 1072;
    }
}
