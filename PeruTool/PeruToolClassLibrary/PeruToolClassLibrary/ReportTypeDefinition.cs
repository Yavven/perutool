﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class ReportTypes
    {
        public const int REPORT_PROFIT = 0;
        public const int REPORT_FINANCE = 1;
        public const int RECORD_FINANCE = 2;
        public const int RECORD_BALANCE = 3;
        public const int RECORD_GAME = 4;
        public const int COMPUTER_GAME_RECORD = 5;
        public const int CAISHER_LOG = 6;
        public const int OPERATOR_LOG = 7;
    }
}
