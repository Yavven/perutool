﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class OldLanguage
    {
        const int LANGUAGE_ENGLISH = 0;
        const int LANGUAGE_SPANISH = 1;

        public static int language = 1;

        public static string Login()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Cajero";
            }
            else
            {
                return "Login";
            }
        }

        public static string ReadCard()
        {
            if(language == LANGUAGE_SPANISH)
            {
                return "Leer Tarjeta";
            }
            else
            {
                return "Read Card";
            }
        }

        public static string LoginFormLogin()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Aceptar";
            }
            else
            {
                return "Login";
            }
        }

        public static string NAME()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Usuario";
            }
            else
            {
                return "NAME";
            }
        }

        public static string PWD()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Clave";
            }
            else
            {
                return "PWD";
            }
        }

        public static string MoneyTax()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Impuesto";
            }
            else
            {
                return "Money Tax";
            }
        }

        public static string Submit()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Clave";
            }
            else
            {
                return "Submit";
            }
        }

        public static string reset_pwd()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Cambiar clave";
            }
            else
            {
                return "reset_pwd";
            }
        }

        public static string Relate()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Personalizar";
            }
            else
            {
                return "Relate";
            }
        }

        public static string Deposit()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Depósitos";
            }
            else
            {
                return "Deposit";
            }
        }

        public static string Withdraw()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Retiro";
            }
            else
            {
                return "Withdraw";
            }
        }

        public static string draw()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Retiros";
            }
            else
            {
                return "draw";
            }
        }

        public static string exit()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Salir";
            }
            else
            {
                return "exit";
            }
        }

        public static string password()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Clave actual";
            }
            else
            {
                return "password";
            }
        }

        public static string newpassword()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Nueva clave";
            }
            else
            {
                return "new password";
            }
        }

        public static string confirmpassword()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Confirmar Nueva Clave";
            }
            else
            {
                return "confirm password";
            }
        }

        public static string CID()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "ID Tarjeta";
            }
            else
            {
                return "CID";
            }
        }

        public static string DEPOSIT()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Monto a depositar";
            }
            else
            {
                return "DEPOSIT";
            }
        }

        public static string BALANCE()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Saldo";
            }
            else
            {
                return "BALANCE";
            }
        }

        public static string Banlance()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Saldo";
            }
            else
            {
                return "Banlance";
            }
        }

        public static string Today()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Depositado Hoy";
            }
            else
            {
                return "Today";
            }
        }

        public static string Submit1()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Aceptar";
            }
            else
            {
                return "Submit";
            }
        }

        public static string submit()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Depositar";
            }
            else
            {
                return "submit";
            }
        }

        public static string DrawSubmit()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Retirar";
            }
            else
            {
                return "Submit";
            }
        }

        public static string DRAW()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Monto a retirar";
            }
            else
            {
                return "DRAW";
            }
        }

        public static string Sequence()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Secuencia";
            }
            else
            {
                return "Sequence";
            }
        }

        public static string Card()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Tarjeta_ID";
            }
            else
            {
                return "Card";
            }
        }

        public static string Type()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Tipo";
            }
            else
            {
                return "Type";
            }
        }

        public static string Amount()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Cantidad";
            }
            else
            {
                return "Amount";
            }
        }

        public static string Query()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Consultar";
            }
            else
            {
                return "Query";
            }
        }

        public static string Close()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Cerrar";
            }
            else
            {
                return "Close";
            }
        }

        public static string Location()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Concesionario";
            }
            else
            {
                return "Location";
            }
        }

        public static string Computer()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Terminal TI";
            }
            else
            {
                return "Computer";
            }
        }

        public static string MoneyPlayed()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "S/. Jugados";
            }
            else
            {
                return "Money Played";
            }
        }

        public static string Moneywon()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "S/. Ganados";
            }
            else
            {
                return "Money won";
            }
        }

        public static string Profit()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "S/.Sub Total";
            }
            else
            {
                return "Profit";
            }
        }

        public static string TesoroIncaMoneyPlayed()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Inca Play";
            }
            else
            {
                return "Tesoro Inca Money Played";
            }
        }

        public static string MoneyIn()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "S/. Depositos";
            }
            else
            {
                return "Money In";
            }
        }

        public static string Moneyout()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "S/. Retiros";
            }
            else
            {
                return "Money out";
            }
        }

        public static string CardId()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Tarjeta_ID";
            }
            else
            {
                return "Card";
            }
        }

        public static string Time()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Fecha";
            }
            else
            {
                return "Time";
            }
        }

        public static string Count()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Count";
            }
            else
            {
                return "Count";
            }
        }

        public static string ComputerReport()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Reporte de Produccion";
            }
            else
            {
                return "Computer Report";
            }
        }

        public static string CashierLog ()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Operaciones de Caja";
            }
            else
            {
                return "Cashier Log";
            }
        }

        public static string Export()
        {
            if (language == LANGUAGE_SPANISH)
            {
                return "Exportar";
            }
            else
            {
                return "Export";
            }
        }
    }
}
