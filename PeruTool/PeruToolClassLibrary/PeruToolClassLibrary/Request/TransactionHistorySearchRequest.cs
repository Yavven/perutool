﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class TransactionHistorySearchRequest:BaseRequest
    {
        public TransactionHistorySearchRequest(int startTimeStamp, int endTimeStamp)
        {
            WriteInt(startTimeStamp);
            WriteInt(endTimeStamp);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
