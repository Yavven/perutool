﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class AdminBackCreateRequest : BaseRequest
    {
        public AdminBackCreateRequest(string username, string password)
        {
            WriteString(username);
            WriteString(password);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
