﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CompanyInfoEditRequest : BaseRequest
    {
        public CompanyInfoEditRequest(string name, string howToGet, string address, string telphone)
        {
            WriteString(name);
            WriteString(howToGet);
            WriteString(address);
            WriteString(telphone);

        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
