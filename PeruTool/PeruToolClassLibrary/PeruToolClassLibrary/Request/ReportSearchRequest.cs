﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class ReportSearchRequest:BaseRequest
    {
        public ReportSearchRequest(string username, int startTimeStamp, int endTimeStamp)
        {
            WriteString(username);
            WriteInt(startTimeStamp);
            WriteInt(endTimeStamp);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
