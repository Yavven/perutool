﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YWCommonUtil;

namespace PeruToolClassLibrary.Request
{
    public class LoginRequest:BaseRequest
    {
        public LoginRequest(string username, string password)
        {
            WriteString(username);
            WriteString(password);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
