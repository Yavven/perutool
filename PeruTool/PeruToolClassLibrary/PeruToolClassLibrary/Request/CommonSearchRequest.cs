﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CommonSearchRequest:BaseRequest
    {
        public CommonSearchRequest(string locationName)
        {
            WriteString(locationName);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
