﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CardTransferRequest : BaseRequest
    {
        public CardTransferRequest(string cardId, int tag, double money)
        {
            WriteString(cardId);
            WriteInt(tag); 
            WriteDouble(money);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
