﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class ReportFinanceRecordGetRequest : BaseRequest
    {
        public ReportFinanceRecordGetRequest(string locationName, string cardId, int beginTime, int endTime)
        {
            WriteString(locationName);
            WriteString(cardId);
            WriteInt(beginTime);
            WriteInt(endTime);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
