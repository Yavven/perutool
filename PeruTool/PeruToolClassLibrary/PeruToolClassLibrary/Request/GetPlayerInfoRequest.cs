﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class GetPlayerInfoRequest : BaseRequest
    {
        public GetPlayerInfoRequest(string player)
        {
            WriteString(player);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
