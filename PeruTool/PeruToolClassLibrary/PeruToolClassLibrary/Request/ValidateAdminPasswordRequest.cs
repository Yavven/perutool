﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class ValidateAdminPasswordRequest : BaseRequest
    {
        public ValidateAdminPasswordRequest(string password)
        {
            WriteString(password);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
