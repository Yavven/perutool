﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class GameConfigEditRequest : BaseRequest
    {
        public GameConfigEditRequest(int gameId, int[] flags, int openStatus, int rechargeRate, double taxRate, int maxBet, int maxPaid, int iniIndex)
        {
            WriteInt(gameId);

            foreach(int flag in flags)
            {
                WriteInt(flag);
            }

            WriteInt(openStatus);
            WriteInt(rechargeRate);
            WriteDouble(taxRate);
            WriteInt(maxBet);
            WriteInt(maxPaid);
            WriteInt(iniIndex);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
