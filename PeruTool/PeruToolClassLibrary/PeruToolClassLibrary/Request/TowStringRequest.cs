﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class TowStringRequest : BaseRequest
    {
        public TowStringRequest(string string1, string string2)
        {
            WriteString(string1);
            WriteString(string2);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
