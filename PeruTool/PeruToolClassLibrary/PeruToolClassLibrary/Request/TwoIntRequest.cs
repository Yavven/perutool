﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class TwoIntRequest : BaseRequest
    {
        public TwoIntRequest(int firstInt, int secondInt)
        {
            WriteInt(firstInt);
            WriteInt(secondInt);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
