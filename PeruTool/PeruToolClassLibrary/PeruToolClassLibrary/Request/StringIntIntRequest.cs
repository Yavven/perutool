﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class StringIntIntRequest : BaseRequest
    {
        public StringIntIntRequest(string string1,int int1, int int2)
        {
            WriteString(string1);
            WriteInt(int1);
            WriteInt(int2);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
