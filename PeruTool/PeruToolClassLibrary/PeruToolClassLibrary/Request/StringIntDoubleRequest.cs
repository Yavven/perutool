﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class StringIntDoubleRequest : BaseRequest
    {
        public StringIntDoubleRequest(string stringValue, int intValue, double doubleValue)
        {
            WriteString(stringValue);
            WriteInt(intValue);
            WriteDouble(doubleValue);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
