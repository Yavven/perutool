﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CommonCreateRequest : BaseRequest
    {
        public CommonCreateRequest(string backAdminName, string password, string username)
        {
            WriteString(backAdminName);
            WriteString(username);
            WriteString(password);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
