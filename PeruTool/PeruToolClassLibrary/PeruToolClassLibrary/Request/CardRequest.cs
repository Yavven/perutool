﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CardSearchRequest:BaseRequest
    {
        public CardSearchRequest(double balance)
        {
            WriteDouble(balance);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
