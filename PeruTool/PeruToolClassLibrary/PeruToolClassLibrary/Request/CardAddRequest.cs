﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CardAddRequest : BaseRequest
    {
        public CardAddRequest(string cardId)
        {
            WriteString(cardId);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
