﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class GameConfigGlobalEditRequest : BaseRequest
    {
        public GameConfigGlobalEditRequest(int[] flags,int taxEdge,int doubleStatus, int doubleMaxEachBet, int doubleMaxEachPaid)
        {
            foreach(int flag in flags)
            {
                WriteInt(flag);
            }

            WriteInt(taxEdge);
            WriteInt(doubleStatus);
            WriteInt(doubleMaxEachBet);
            WriteInt(doubleMaxEachPaid);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
