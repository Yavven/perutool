﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YWCommonUtil;

namespace PeruToolClassLibrary.Request
{
    public class BaseRequest
    {
        public byte[] Bytes;

        public void WriteString(string value)
        {
            if(Bytes == null)
            {
                Bytes = ByteUtil.ConvertValueToBigEndianBytes(value);
            }
            else
            {
                ByteUtil.AddValueToBigEndianBytes(value, ref Bytes);
            }
        }

        public void WriteInt(int value)
        {
            if (Bytes == null)
            {
                Bytes = ByteUtil.ConvertValueToBigEndianBytes(value);
            }
            else
            {
                ByteUtil.AddValueToBigEndianBytes(value, ref Bytes);
            }
        }

        public void WriteBool(bool value)
        {
            if (Bytes == null)
            {
                Bytes = ByteUtil.ConvertValueToBigEndianBytes(value);
            }
            else
            {
                ByteUtil.AddValueToBigEndianBytes(value, ref Bytes);
            }
        }

        public void WriteLong(long value)
        {
            if (Bytes == null)
            {
                Bytes = ByteUtil.ConvertValueToBigEndianBytes(value);
            }
            else
            {
                ByteUtil.AddValueToBigEndianBytes(value, ref Bytes);
            }
        }

        public void WriteDouble(double value)
        {
            if (Bytes == null)
            {
                Bytes = ByteUtil.ConvertValueToBigEndianBytes(value);
            }
            else
            {
                ByteUtil.AddValueToBigEndianBytes(value, ref Bytes);
            }
        }
    }
}
