﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CommonEnableRequest : BaseRequest
    {
        public CommonEnableRequest(string username, bool enable)
        {
            WriteString(username);

            if(enable)
            {
                WriteInt(StatusTypes.ENABLE);
            }
            else
            {
                WriteInt(StatusTypes.DISABLE);
            }
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
