﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class TransactionConfirmRequest:BaseRequest
    {
        public TransactionConfirmRequest(string username, int type, double amount)
        {
            WriteString(username);
            WriteInt(type);
            WriteDouble(amount);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
