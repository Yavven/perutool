﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class CardAdminAddRequest : BaseRequest
    {
        public CardAdminAddRequest(string name, string password)
        {
            WriteString(name);
            WriteString(password);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
