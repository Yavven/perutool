﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Request
{
    public class GameConfigSearchRequest : BaseRequest
    {
        public GameConfigSearchRequest(int gameId)
        {
            WriteInt(gameId);
        }

        public byte[] GetBytes()
        {
            return Bytes;
        }
    }
}
