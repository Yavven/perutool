﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class FinanceToolWebVersionMessageId
    {
        public const int LOGIN = 2001;
        public const int SEARCH_PLAYER = 2011;
        public const int TRANSFER = 2012;
        public const int SEARCH_TRANSFER_HISTORY = 2013;
    }
}
