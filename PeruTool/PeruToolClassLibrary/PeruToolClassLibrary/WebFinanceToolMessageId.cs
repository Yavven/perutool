﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class WebFinanceToolMessageId
    {
        public const int LOGIN = 2001;
        public const int TRANSACTION_HISTORY_SEARCH = 2014444;
        public const int CODEUSER_SEARCH = 2011;
        public const int TRANSACTION_CONFIRM = 2012;
    }
}
