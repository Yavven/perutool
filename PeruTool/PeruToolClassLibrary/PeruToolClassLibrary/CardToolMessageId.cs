﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class CardToolMessageId
    {
        public const int LOGIN = 3001;
        public const int CARD_ADD = 3011;
        public const int CARD_ENABLE = 3012;
        public const int CARD_SEARCH = 3013;
    }
}
