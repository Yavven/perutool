﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class Computer:BaseModel
    {
        public string Name;

        public int Status;

        public int ShowStatus;

        public int TimeStamp;

        public Computer(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Name = ReadString();
            Status = ReadInt();
            ShowStatus = ReadInt();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
