﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class SampleGameConfig:BaseModel
    {
        public int GameId;

        public int OpenStatus;

        public int RechargeRate;

        public double TaxRate;

        public int MaxBet;

        public int MaxPaid;

        public int CurIniIndex;

        public string CurIniName;

        public SampleGameConfig(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            GameId = ReadInt();
            OpenStatus = ReadInt();
            RechargeRate = ReadInt();
            TaxRate = ReadDouble();
            MaxBet = ReadInt();
            MaxPaid = ReadInt();
            CurIniIndex = ReadInt();
            CurIniName = ReadString();

            offset = Offset;
        }
    }
}
