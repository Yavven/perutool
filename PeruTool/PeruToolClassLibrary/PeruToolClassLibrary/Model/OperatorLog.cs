﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class OperatorLog:BaseModel
    {
        public string Operator;

        public int Type;

        public string Data;

        public int TimeStamp;

        public OperatorLog(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Operator = ReadString();
            Type = ReadInt();
            Data = ReadString();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
