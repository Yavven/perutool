﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class NameModel:BaseModel
    {
        public string Name { get; set; }

        public string Value { get; set; }


        public NameModel(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Name = ReadString();
            Value = Name;

            offset = Offset;
        }

        public NameModel(string name,string value)
        {
            Name = name;
            Value = value;
        }
    }
}
