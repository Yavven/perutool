﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class BalanceRecord:BaseModel
    {
        public string Operator;

        public int Type;

        public double Amount;

        public double Balance;

        public int TimeStamp;

        public BalanceRecord(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Operator = ReadString();
            Type = ReadInt();
            Amount = ReadDouble();
            Balance = ReadDouble();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
