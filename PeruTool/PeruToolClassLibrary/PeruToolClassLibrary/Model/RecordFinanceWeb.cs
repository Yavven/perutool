﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class RecordFinanceWeb:BaseModel
    {
        public string Card;

        public string Operator;

        public int Type;

        public double Money;

        public double Balance;

        public int TimeStamp;

        public RecordFinanceWeb(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Card = ReadString();
            Operator = ReadString();
            Type = ReadInt();
            Money = ReadDouble();
            Balance = ReadDouble();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
