﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class TransactionHistory:BaseModel
    {
        public string WaterNumber;

        public string UserName;
        
        public string OperatorName;
               
        public int Type;
        
        public int Status;

        public double Money;

        public double Balance;

        public int TimeStamp;

        public TransactionHistory(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            WaterNumber = ReadString();
            UserName = ReadString();
            OperatorName = ReadString();            
            Type = ReadInt();
            Status = ReadInt();
            Money = ReadDouble();
            Balance = ReadDouble();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
