﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class ReportProfit:BaseModel
    {
        public string Location;

        public string Computer;

        public double AllBet;

        public double AllPaid;

        public double AllTax;

        public double TesoAllBet;

        public ReportProfit(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Location = ReadString();
            Computer = ReadString();
            AllBet = ReadDouble();
            AllPaid = ReadDouble();
            AllTax = ReadDouble();
            TesoAllBet = ReadDouble();

            offset = Offset;
        }
    }
}
