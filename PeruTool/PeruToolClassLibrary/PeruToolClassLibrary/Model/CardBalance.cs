﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class CardBalance:BaseModel
    {
        public string CardId;

        public double Balance;

        public CardBalance(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            CardId = ReadString();
            Balance = ReadDouble();

            offset = Offset;
        }
    }
}
