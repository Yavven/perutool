﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class ReportGameRecord:BaseModel
    {
        public string Name;

        public int GameId;

        public double Bet;

        public double Paid;

        public double Tax;

        public double Balance;

        public string BetData;

        public string PaidData;

        public string CardData;

        public string DuData;

        public int TimeStamp;

        public ReportGameRecord(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Name = ReadString();
            GameId = ReadInt();
            Bet = ReadDouble();
            Paid = ReadDouble();
            Tax = ReadDouble();
            Balance = ReadDouble();
            BetData = ReadString();
            PaidData = ReadString();
            CardData = ReadString();
            DuData = ReadString();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
