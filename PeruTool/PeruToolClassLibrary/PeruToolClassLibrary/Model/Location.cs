﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class AdminBack:BaseModel
    {
        public string Name;

        public int Status;

        public int TimeStamp;

        public AdminBack(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Name = ReadString();
            Status = ReadInt();
            TimeStamp = ReadInt();

            offset = Offset;
        }
    }
}
