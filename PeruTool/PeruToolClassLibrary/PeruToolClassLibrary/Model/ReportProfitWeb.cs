﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class ReportProfitWeb:BaseModel
    {
        public double AllBet;

        public double AllPaid;

        public double AllTax;

        public double TesoAllBet;

        public ReportProfitWeb(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            AllBet = ReadDouble();
            AllPaid = ReadDouble();
            AllTax = ReadDouble();
            TesoAllBet = ReadDouble();

            offset = Offset;
        }
    }
}
