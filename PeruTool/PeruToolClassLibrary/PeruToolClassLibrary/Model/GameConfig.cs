﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class GameConfig:BaseModel
    {
        public int OpenStatus;

        public int RechargeRate;

        public int TaxRate;

        public int MaxBet;

        public int MaxPaid;

        public int CurIniIndex;

        public List<Paytable> Paytabls;

        public GameConfig(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            OpenStatus = ReadInt();
            RechargeRate = ReadInt();
            TaxRate = ReadInt();
            MaxBet = ReadInt();
            MaxPaid = ReadInt();
            CurIniIndex = ReadInt();

            int paytableCount = ReadInt();

            for (int i = 0; i < paytableCount; i++)
            {
                Paytable paytable = new Paytable(bytes, ref Offset);
                Paytabls.Add(paytable);
            }
               
            offset = Offset;
        }
    }
}
