﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class ReportFinanceWeb:BaseModel
    {
        public double Deposit;

        public double Withdraw;

        public ReportFinanceWeb(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Deposit = ReadDouble();
            Withdraw = ReadDouble();
        }
    }
}
