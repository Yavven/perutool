﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YWCommonUtil;

namespace PeruToolClassLibrary.Model
{
    public class BaseModel
    {
        protected byte[] Bytes;
        protected int Offset;

        public bool ReadBool()
        {
            return ByteUtil.ConvertBigEndianBytesToBool(ref Offset, Bytes);
        }

        public double ReadDouble()
        {
            return ByteUtil.ConvertBigEndianBytesToDouble(ref Offset, Bytes);
        }

        public int ReadInt()
        {
            return ByteUtil.ConvertBigEndianBytesToInt(ref Offset, Bytes);
        }

        public long ReadLong()
        {
            return ByteUtil.ConvertBigEndianBytesToLong(ref Offset, Bytes);
        }

        public string ReadString()
        {
            return ByteUtil.ConvertBigEndianBytesToString(ref Offset, Bytes);
        }
    }
}
