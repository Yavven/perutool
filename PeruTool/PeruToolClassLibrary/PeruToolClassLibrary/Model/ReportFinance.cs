﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class FinanceReport:BaseModel
    {
        public string Location;

        public double Deposit;

        public double Withdraw;

        public FinanceReport(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Location = ReadString();
            Deposit = ReadDouble();
            Withdraw = ReadDouble();
        }
    }
}
