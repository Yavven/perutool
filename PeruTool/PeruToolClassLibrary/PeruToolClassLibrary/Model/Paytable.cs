﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary.Model
{
    public class Paytable:BaseModel
    {
        public int Index{get;set;}

        public string Name{get;set;}

        public Paytable(byte[] bytes, ref int offset)
        {
            Bytes = bytes;
            Offset = offset;

            Index = ReadInt();
            Name = ReadString();

            offset = Offset;
        }
    }
}
