﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruToolClassLibrary
{
    public class FinanceToolMessageId
    {
        public const int LOGIN = 2001;
        public const int CARD_BALANCE = 2011;
        public const int TRANSFER = 2012;
        public const int FINANCE_RECORD = 2021;
        public const int COMPUTER_GAME_RECORD = 2022;
        public const int CARD_GAME_RECORD = 2023;
        public const int CHANGE_PASSOWRD = 2051;
        public const int GET_OPERATOR_LOG = 2041;
        public const int CAISHER_LOG = 2024;
        public const int GET_ALL_CASHIER_NAME = 2061;
        public const int GET_ALL_COMPUTER_NAME = 2062;
        public const int GET_PROFIT_REPROT = 2071;
    }
}
