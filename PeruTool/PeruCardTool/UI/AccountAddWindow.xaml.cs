﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary;
using PeruCardTool.BLL;
using PeruToolClassLibrary.Model;
using YWCommonUtil;

namespace PeruCardTool.UI
{
    /// <summary>
    /// AddWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AccountAddWindow
    {
        public AccountAddWindow()
        {
            InitializeComponent();

            if(IsDebug)
            {
                CardIdTextBox.IsReadOnly = false;
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string cardId = CardIdTextBox.Text;

            if(string.IsNullOrEmpty(cardId))
            {
                MessageBox.Show("Card ID is empty!");
                return;
            }

            RequestBll.CardAdd(cardId);
            Hide();
        }

        private void ReadCardButton_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder cardId = new StringBuilder();

            if (!NativeMethods.GetCardId(ref cardId))
            {
                return;
            }
            else
            {
                CardIdTextBox.Text = cardId.ToString();
            }
        }

        private void CardIdTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsInteger(e.Text))
            {
                e.Handled = true;
            }
        }

        private void CardIdTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)                                                
                    return true;
                #else                               
                    return false;
                #endif
            }
        }
    }
}
