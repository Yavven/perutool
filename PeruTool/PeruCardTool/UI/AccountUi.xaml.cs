﻿using PeruCardTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using PeruToolClassLibrary.Response;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YWCommonUtil;

namespace PeruCardTool.UI
{
    /// <summary>
    /// LocationUi.xaml 的交互逻辑
    /// </summary>
    public partial class AccountUi : UserControl
    {
        DataTable dataTable = new DataTable();
        bool isEnable = false;

        public void OnMessageResponse(object response)
        {
            dataTable = new DataTable();
            CommonListResponse cardSearchResponse = response as CommonListResponse;
            dataTable.Columns.Add("CardId");
            dataTable.Columns.Add("Status");
            dataTable.Columns.Add("Time");

            foreach (CommonAccount card in cardSearchResponse.Cards)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["CardId"] = card.Name;
                dataRow["Status"] = card.Status == StatusTypes.ENABLE ? "Enable" : "Disable";
                dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(card.TimeStamp);
                dataTable.Rows.Add(dataRow);
            }

            AccountDataGrid.ItemsSource = dataTable.AsDataView();
        }

        public AccountUi()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AccountAddWindow accountAddWindow = new AccountAddWindow();
            accountAddWindow.Show();
        }

        private void EnableButton_Click(object sender, RoutedEventArgs e)
        {
            DataRowView selectedDataRowView = (DataRowView)AccountDataGrid.SelectedItem;

            if(selectedDataRowView == null)
            {
                return;
            }

            string cardId = selectedDataRowView.Row["CardId"].ToString();
            RequestBll.CardEnable(!isEnable, cardId);
        }

        private void AccountDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(AccountDataGrid.SelectedItem == null)
            {
                return;
            }

            DataRowView selectedDataRowView = (DataRowView)AccountDataGrid.SelectedItem;
            string status = selectedDataRowView.Row["Status"].ToString();

            if (status == "Enable")
            {
                EnableButton.Content = "Disable";
                isEnable = true;
            }
            else
            {
                EnableButton.Content = "Enable";
                isEnable = false;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.CardSearch();
        }
    }
}
