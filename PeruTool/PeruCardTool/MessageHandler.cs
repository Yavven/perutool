﻿using PeruCardTool.BLL;
using PeruToolClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YWCommonUtil;
using YWNetworkEngine;

namespace PeruCardTool
{
    public class PeruMessageHandler : YWMessageHandler
    {
        private YWResponseBll messageBll;

        public void SetMessageBLL(YWResponseBll messageBLL)
        {
            this.messageBll = messageBLL;
        }

        public override void ProcessReceivedMessag(StringBuilder message)
        {
            #region 恢复收到的消息
            string[] messageStrings = message.ToString().Split(',');
            byte[] messageBytes = new byte[messageStrings.Length];

            for (int i = 0; i < messageStrings.Length; i++)
            {
                messageBytes[i] = byte.Parse(messageStrings[i]);
            }
            #endregion

            int dataLength = messageBytes.Length;
            int offset = 0;

            while (dataLength > 0)
            {
                int messageLength = ByteUtil.ConvertBigEndianBytesToInt(ref offset, messageBytes); // 解包消息长度
                dataLength = dataLength - messageLength;
                int messageID = ByteUtil.ConvertBigEndianBytesToInt(ref offset, messageBytes); // 解包消息ID
                offset += MessageHead.RANDOM_NUMBER_OFFSET; // 随机数偏移量
                int messageBodyLength = messageLength - 12;
                byte[] messageBodyBytes = new byte[messageBodyLength];
                Array.Copy(messageBytes, offset, messageBodyBytes, 0, messageBodyLength);
                messageBll.OnMessageResponse(messageID, messageBodyBytes);
                offset = offset + messageBodyLength;
            }
        }

    }
}
