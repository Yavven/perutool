﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruCardTool.BLL;
using PeruToolClassLibrary.Response;
using YWCommonUtil;

namespace PeruCardTool
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow
    {
        public bool IsRoot;

        public LoginWindow()
        {
            InitializeComponent();

            App.GlobalLoginWindow = this;
        }

        public void OnMessageResponse(LoginResponse response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if (response.IsSuccess())
                {
                    IsRoot = response.IsRoot();
                    Hide();
                    App.GlobalMainWindow = new MainWindow();
                    App.GlobalMainWindow.Show();
                }
                else
                {
                    MessageBox.Show("fail");
                }
            }));
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Password;
            App.LoginLocation = username;
            RequestBll.Login(password, username);
        }

        private void UsernameTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void UsernameTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }
    }
}
