﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary.Request;
using PeruToolClassLibrary;

namespace PeruCardTool.BLL
{
    class RequestBll
    {
        public static void Login(string password, string username)
        {
            LoginRequest request = new LoginRequest(username, password);
            App.GlobalPeruTcpClient.Send(CardToolMessageId.LOGIN, request.GetBytes());
        }

        public static void CardSearch()
        {
            App.GlobalPeruTcpClient.Send(CardToolMessageId.CARD_SEARCH);
        }

        public static void CardAdd(string cardId)
        {
            CardAddRequest request = new CardAddRequest(cardId);
            App.GlobalPeruTcpClient.Send(CardToolMessageId.CARD_ADD, request.GetBytes());
        }

        public static void CardEnable(bool enable, string cardId)
        {
            CommonEnableRequest request = new CommonEnableRequest(cardId, enable);
            App.GlobalPeruTcpClient.Send(CardToolMessageId.CARD_ENABLE, request.GetBytes());
        }
    }
}
