﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using YWNetworkEngine;

namespace PeruCardTool.BLL
{
    public class MessageBll:YWResponseBll
    {
        public override void OnMessageResponse(int messageId, byte[] messageBodyBytes)
        {
            object response = null;

            switch (messageId)
            {
                case CardToolMessageId.LOGIN:
                    App.GlobalLoginWindow.OnMessageResponse(new LoginResponse(messageBodyBytes));
                    break;

                case CardToolMessageId.CARD_ADD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case CardToolMessageId.CARD_ENABLE:
                    response = new CommonEnableResponse(messageBodyBytes);
                    break;

                case CardToolMessageId.CARD_SEARCH:
                    response = new CommonListResponse(messageBodyBytes);
                    break;
            }

            if(response != null)
            {
                App.GlobalMainWindow.OnMessageResponse(messageId, response);
            }
        }
    }
}
