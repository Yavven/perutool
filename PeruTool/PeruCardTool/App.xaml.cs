﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using PeruCardTool.BLL;
using YWCommonUtil;
using PeruToolClassLibrary.Model;

namespace PeruCardTool
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public static string AppPath;
        public static string LoginLocation;
        public static MessageBll GlobalPeruMessageBll;
        public static PeruMessageHandler GlobalPeruMessageHandler;
        public static PeruTcpClient GlobalPeruTcpClient;
        public static Thread GlobalPeruTcpClientThread;
        public static LoginWindow GlobalLoginWindow;
        public static MainWindow GlobalMainWindow;
        public static List<AdminBack> Locations;
 
        public App()
        {
            string serverHostnameKey = "ServerHostname";
            string serverPortKey = "ServerPort";

            if (IsDebug)
            {
                serverHostnameKey = "Test" + serverHostnameKey;
                serverPortKey = "Test" + serverPortKey;
            }

            AppPath = AppDomain.CurrentDomain.BaseDirectory;
            string serverHostname = Convert.ToString(ConfigUtil.GetAppSettingsValue(serverHostnameKey));
            int serverHPort = Convert.ToInt32(ConfigUtil.GetAppSettingsValue(serverPortKey));
            GlobalPeruMessageHandler = new PeruMessageHandler();
            GlobalPeruMessageBll = new MessageBll();
            GlobalPeruMessageHandler.SetMessageBLL(GlobalPeruMessageBll);
            GlobalPeruTcpClient = new PeruTcpClient();
            GlobalPeruTcpClient.SetHostnameAndPort(serverHostname, serverHPort);
            GlobalPeruTcpClient.MessageHandler = GlobalPeruMessageHandler;
            GlobalPeruTcpClientThread = new Thread(GlobalPeruTcpClient.Connect);
            GlobalPeruTcpClientThread.IsBackground = true;
            GlobalPeruTcpClientThread.Start();
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)                 
                                  return true;
                #else
                                return false;
                #endif
            }
        }
    }
}
