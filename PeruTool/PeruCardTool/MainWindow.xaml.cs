﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeruCardTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;

namespace PeruCardTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void OnMessageResponse(int messageId, object response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                switch (messageId)
                {
                    case CardToolMessageId.CARD_ADD:
                        CreateEditResponse cardAddResponse = response as CreateEditResponse;

                        if (cardAddResponse.IsSuccess())
                        {
                            RequestBll.CardSearch();
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case CardToolMessageId.CARD_ENABLE:     
                        RequestBll.CardSearch();
                        break;

                    case CardToolMessageId.CARD_SEARCH:
                        CardUi.OnMessageResponse(response);
                        break;
                }
            }));           
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
