﻿using PeruFinanceTool.BLL;
using PeruFinanceTool.UI;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using YWCommonUtil;

namespace PeruFinanceTool
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public static bool IsRoot;
        public static string AppPath;
        public static string LoginLocation;
        public static MessageBll GlobalPeruMessageBll;
        public static PeruMessageHandler GlobalPeruMessageHandler;
        public static PeruTcpClient GlobalPeruTcpClient;
        public static Thread GlobalPeruTcpClientThread;
        public static LoginWindow GlobalLoginWindow;
        public static MainWindow GlobalMainWindow;
        public static List<AdminBack> Locations;
        public static Dictionary<int, string> GameNames;
        public static Dictionary<int, string> TransferTypeName;
        public static Dictionary<int, string> OperatorTypes;
        public static ReportWindow BalanceRecordWindow;
        public static ReportWindow FinanceRecordWindow;
        public static ReportWindow CashierLogWindow;
        public static ReportWindow OperatorLogWindow;
        public static ReportWindow CardGameRecordWindow;
        public static ReportWindow ComputerRecordWindow;
        public static ReportWindow ProfitReportWindow;
        public static List<NameModel> AllCashierName;
        public static List<NameModel> AllComputerName;
        public static PeruLanguage MyLanguage;

        public App()
        {
            string serverHostnameKey = "ServerHostname";
            string serverPortKey = "ServerPort";

            if (IsDebug)
            {
                serverHostnameKey = "Test" + serverHostnameKey;
                serverPortKey = "Test" + serverPortKey;
            }

            AppPath = AppDomain.CurrentDomain.BaseDirectory;
            string serverHostname = Convert.ToString(ConfigUtil.GetAppSettingsValue(serverHostnameKey));
            int serverHPort = Convert.ToInt32(ConfigUtil.GetAppSettingsValue(serverPortKey));
            GlobalPeruMessageHandler = new PeruMessageHandler();
            GlobalPeruMessageBll = new MessageBll();
            GlobalPeruMessageHandler.SetMessageBLL(GlobalPeruMessageBll);
            GlobalPeruTcpClient = new PeruTcpClient();
            GlobalPeruTcpClient.SetHostnameAndPort(serverHostname, serverHPort);
            GlobalPeruTcpClient.MessageHandler = GlobalPeruMessageHandler;
            GlobalPeruTcpClientThread = new Thread(GlobalPeruTcpClient.Connect);
            GlobalPeruTcpClientThread.IsBackground = true;
            GlobalPeruTcpClientThread.Start();
            
            TransferTypeName = new Dictionary<int, string>();
            TransferTypeName.Add(1, "Deposit");
            TransferTypeName.Add(2, "Withdraw");
            TransferTypeName.Add(3, "Game");

            GameNames = new Dictionary<int, string>();
            GameNames.Add(1, "Poker Comodin");
            GameNames.Add(2, "2 Comodin");
            GameNames.Add(3, "Classic Bonus");
            GameNames.Add(4, "Poker Comodin");
            GameNames.Add(5, "Four Deck Draw");
            GameNames.Add(11, "Siete Rojo");
            GameNames.Add(12, "Gran Tiburon");
            GameNames.Add(13, "Tesoro Inca");
            GameNames.Add(30, "Keno");
            GameNames.Add(31, "Power Keno");
            GameNames.Add(32, "Highroller Keno");
            GameNames.Add(33, "Alley Cat Keno");
            GameNames.Add(34, "Campanitas");
            GameNames.Add(35, "Polly's Gold");
            GameNames.Add(36, "Keno Wild");
            GameNames.Add(37, "Abejitas");

            OperatorTypes = new Dictionary<int, string>();
            OperatorTypes.Add(1, "Change Password");
            OperatorTypes.Add(2, "Deposit");
            OperatorTypes.Add(3, "Withdraw");

            AllCashierName = new List<NameModel>();
            MyLanguage = new PeruLanguage(1);
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)                                             
                    return true;
                #else                         
                    return false;
                #endif
            }
        }
    }
}
