﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PeruFinanceTool
{
    class NativeMethods
    {
        [DllImport("Card.dll", EntryPoint = "?GetCard@@YA_NPAD@Z", CallingConvention = CallingConvention.Cdecl)]
        static extern bool getCardId(StringBuilder cardId);

        public static bool GetCardId(ref StringBuilder cardId)
        {
            return getCardId(cardId);
        }
    }
}
