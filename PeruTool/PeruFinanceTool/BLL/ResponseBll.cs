﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using YWNetworkEngine;

namespace PeruFinanceTool.BLL
{
    public class MessageBll:YWResponseBll
    {
        public override void OnMessageResponse(int messageId, byte[] messageBodyBytes)
        {
            object response = null;

            switch (messageId)
            {
                case FinanceToolMessageId.LOGIN:
                    App.GlobalLoginWindow.OnMessageResponse(new LoginResponse(messageBodyBytes));
                    break;

                case FinanceToolMessageId.CARD_BALANCE:
                    response = new BalanceResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.TRANSFER:
                    response = new TransferResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.FINANCE_RECORD:
                    response = new FinanceRecordResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.CARD_GAME_RECORD:
                    response = new GameRecordResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.COMPUTER_GAME_RECORD:
                    response = new GameRecordResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.CHANGE_PASSOWRD:
                    response = new CreateEditResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.GET_OPERATOR_LOG:
                    response = new GetOperatorLogResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.CAISHER_LOG:
                    response = new FinanceRecordResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.GET_ALL_CASHIER_NAME:
                    response = new GetNameListResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.GET_ALL_COMPUTER_NAME:
                    response = new GetNameListResponse(messageBodyBytes);
                    break;

                case FinanceToolMessageId.GET_PROFIT_REPROT:
                    response = new ReportProfitResponse(messageBodyBytes);
                    break;
            }

            if(response != null)
            {
                App.GlobalMainWindow.OnMessageResponse(messageId, response);
            }
        }
    }
}
