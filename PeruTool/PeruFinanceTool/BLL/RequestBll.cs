﻿using PeruToolClassLibrary;
using PeruToolClassLibrary.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeruFinanceTool.BLL
{
    class RequestBll
    {
        public static void Login(string password, string username)
        {
            LoginRequest request = new LoginRequest(username, password);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.LOGIN, request.GetBytes());
        }

        public static void CardBalance(string cardId)
        {
            CardBalanceRequest request = new CardBalanceRequest(cardId);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.CARD_BALANCE, request.GetBytes());
        }

        public static void Deposit(string cardId, double amount)
        {
            CardTransferRequest request = new CardTransferRequest(cardId, 1, amount);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.TRANSFER, request.GetBytes());
        }

        public static void Withdraw(string cardId, double amount)
        {
            CardTransferRequest request = new CardTransferRequest(cardId, 2, amount);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.TRANSFER, request.GetBytes());
        }

        public static void ReportGet(int messageId, string username, int startTime, int endTime)
        {
            ReportSearchRequest request = new ReportSearchRequest(username, startTime, endTime);
            App.GlobalPeruTcpClient.Send(messageId, request.GetBytes());
        }

        public static void ReportCardGameRecordGet(string cardId, int startTime, int endTime)
        {
            ReportGet(FinanceToolMessageId.CARD_GAME_RECORD, cardId, startTime, endTime);
        }

        public static void ReportComputerGameRecordGet(string computerName, int startTime, int endTime)
        {
            ReportGet(FinanceToolMessageId.COMPUTER_GAME_RECORD, computerName, startTime, endTime);
        }

        public static void ReportFinanceRecordGet(string cardId, int startTime, int endTime)
        {
            ReportSearchRequest request = new ReportSearchRequest(cardId, startTime, endTime);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.FINANCE_RECORD, request.GetBytes());
        }

        public static void GetProfitReport(string computerName, int startTime, int endTime)
        {
            ReportSearchRequest request = new ReportSearchRequest(computerName, startTime, endTime);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.GET_PROFIT_REPROT, request.GetBytes());
        }

        public static void GetOperatorLog(string username, int startTimeStamp, int endTimeStamp)
        {
            StringIntIntRequest request = new StringIntIntRequest(username, startTimeStamp, endTimeStamp);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.GET_OPERATOR_LOG, request.GetBytes());
        }

        public static void GetCasherLog(int startTimeStamp, int endTimeStamp)
        {
            TwoIntRequest request = new TwoIntRequest(startTimeStamp, endTimeStamp);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.CAISHER_LOG, request.GetBytes());
        }

        public static void ChangePassword(string oldPassword, string newPassword)
        {
            TowStringRequest request = new TowStringRequest(oldPassword, newPassword);
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.CHANGE_PASSOWRD, request.GetBytes());
        }

        public static void GetAllCashierName()
        {
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.GET_ALL_CASHIER_NAME);
        }

        public static void GetAllComputerName()
        {
            App.GlobalPeruTcpClient.Send(FinanceToolMessageId.GET_ALL_COMPUTER_NAME);
        }
    }
}
