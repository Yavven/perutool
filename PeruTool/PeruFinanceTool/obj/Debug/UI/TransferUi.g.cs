﻿#pragma checksum "..\..\..\UI\TransferUi.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "AB767D7C3B5B18D1C510A1E323F23A13"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PeruFinanceTool.UI {
    
    
    /// <summary>
    /// TransferUi
    /// </summary>
    public partial class TransferUi : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label BalanceLabel;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Balance;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TodayMoneyLabel;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TodayMoney;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label CardIdLabel;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Amount;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DepositButton;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox CardIdTextBox;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ReadCardButton;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button GetCardInfoButton;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TransferTypeLabel;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\UI\TransferUi.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TitleLabel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PeruFinanceTool;component/ui/transferui.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UI\TransferUi.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.BalanceLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.Balance = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.TodayMoneyLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.TodayMoney = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.CardIdLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.Amount = ((System.Windows.Controls.TextBox)(target));
            
            #line 14 "..\..\..\UI\TransferUi.xaml"
            this.Amount.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.Amount_PreviewTextInput);
            
            #line default
            #line hidden
            
            #line 14 "..\..\..\UI\TransferUi.xaml"
            this.Amount.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.Amount_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.DepositButton = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\..\UI\TransferUi.xaml"
            this.DepositButton.Click += new System.Windows.RoutedEventHandler(this.DepositButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.CardIdTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 16 "..\..\..\UI\TransferUi.xaml"
            this.CardIdTextBox.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.CardIdTextBox_PreviewKeyDown);
            
            #line default
            #line hidden
            
            #line 16 "..\..\..\UI\TransferUi.xaml"
            this.CardIdTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.CardIdTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 9:
            this.ReadCardButton = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\UI\TransferUi.xaml"
            this.ReadCardButton.Click += new System.Windows.RoutedEventHandler(this.ReadCardButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.GetCardInfoButton = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\UI\TransferUi.xaml"
            this.GetCardInfoButton.Click += new System.Windows.RoutedEventHandler(this.GetCardInfoButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.TransferTypeLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.TitleLabel = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

