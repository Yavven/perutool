﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PeruToolClassLibrary;
using PeruFinanceTool.BLL;
using PeruToolClassLibrary.Model;
using YWCommonUtil;

namespace PeruFinanceTool.UI
{
    /// <summary>
    /// AddWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ChangetPasswordWindow
    {
        public ChangetPasswordWindow()
        {
            InitializeComponent();

            OldPasswordLabel.Content = OldLanguage.password();
            NewPasswordLabel.Content = OldLanguage.newpassword();
            ConfirmButton.Content = OldLanguage.reset_pwd();
            Title = OldLanguage.reset_pwd();
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            string oldPassword = OldPasswordPasswordBox.Password;
            string newPassword = NewPasswordPasswordBox.Password;

            if (string.IsNullOrEmpty(oldPassword))
            {
                MessageBox.Show("La nueva clave debe contener entre 6 y 32 caracteres!");
                return;
            }

            
            if (string.IsNullOrEmpty(newPassword))
            {
                MessageBox.Show("La nueva clave debe contener entre 6 y 32 caracteres!");
                return;
            }
                             
            RequestBll.ChangePassword(oldPassword, newPassword);
            Hide();
        }
    }
}
