﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YWCommonUtil;
using PeruFinanceTool.BLL;
using PeruToolClassLibrary;
using System.Data;
using PeruToolClassLibrary.Response;
using PeruToolClassLibrary.Model;

namespace PeruFinanceTool.UI
{
    /// <summary>
    /// ReportWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ReportWindow
    {
        DataTable dataTable;
        int type;

        public void OnMessageResponse(object response)
        {
            int id = 1;

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    ReportProfitResponse profitReportResponse = response as ReportProfitResponse;
                    double totalMoneyPlayed = 0.00;
                    double totalMoneyWon = 0.00;
                    double totalMoneyTaxed = 0.00;
                    double totalProfit = 0.00;
                    double totalTeso = 0.00;

                    foreach (ReportProfit report in profitReportResponse.Reports)
                    {
                        totalMoneyPlayed += report.AllBet;
                        totalMoneyWon += report.AllPaid;
                        totalMoneyTaxed += report.AllTax;
                        totalProfit += (report.AllBet - report.AllPaid + report.AllTax);
                        totalTeso += report.TesoAllBet;

                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = report.Computer;
                        dataRow[1] = report.AllBet.ToString("0.00");
                        dataRow[2] = report.AllTax.ToString("0.00"); 
                        dataRow[3] = report.AllPaid.ToString("0.00");
                        dataRow[4] =(report.AllBet - report.AllPaid + report.AllTax).ToString("0.00");
                        dataRow[5] = report.TesoAllBet.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    DataRow totalDataRow = dataTable.NewRow();
                    totalDataRow[0] = "Total";
                    totalDataRow[1] = totalMoneyPlayed.ToString("0.00");
                    totalDataRow[2] = totalMoneyTaxed.ToString("0.00");
                    totalDataRow[3] = totalMoneyWon.ToString("0.00");
                    totalDataRow[4] = totalProfit.ToString("0.00");
                    totalDataRow[5] = totalTeso.ToString("0.00");
                    dataTable.Rows.Add(totalDataRow);
                    break;

                case ReportTypes.RECORD_BALANCE:
                    RecordBalanceResponse balanceRecordResponse = response as RecordBalanceResponse;

                    foreach (BalanceRecord record in balanceRecordResponse.Records)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Operator"] = record.Operator;
                        dataRow["Type"] = App.TransferTypeName[record.Type];
                        dataRow["Amount"] = record.Amount;
                        dataRow["Balance"] = record.Balance;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.RECORD_FINANCE:
                    FinanceRecordResponse financeRecordResponse = response as FinanceRecordResponse;

                    foreach (FinanceRecord record in financeRecordResponse.Records)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[OldLanguage.Sequence()] = record.Card;
                        dataRow[OldLanguage.CardId()] = record.Card;
                        dataRow[OldLanguage.Type()] = App.TransferTypeName[record.Type];
                        dataRow["Money"] = record.Money;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataTable.Rows.Add(dataRow);
                    }

                    break;


                case ReportTypes.CAISHER_LOG:
                    double deposit = 0.00;
                    double withdraw = 0.00;
                    int count = 0;

                    FinanceRecordResponse caisherLogResponse = response as FinanceRecordResponse;

                    foreach (FinanceRecord record in caisherLogResponse.Records)
                    {
                        if(record.Type == TransferTypes.DEPOSIT)
                        {
                            deposit += record.Money;
                        }
                        else
                        {
                            withdraw += record.Money;
                        }

                        count++;

                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = id++;
                        dataRow[OldLanguage.CardId()] = record.Card;
                        dataRow[OldLanguage.Time()] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow[OldLanguage.Type()] = App.TransferTypeName[record.Type];
                        dataRow[OldLanguage.Amount()] = record.Money.ToString("0.00");
                        dataTable.Rows.Add(dataRow);
                    }

                    Title = OldLanguage.CashierLog();
                    DepositTile.Content = OldLanguage.Deposit();
                    WithdrawTitle.Content = OldLanguage.Withdraw();
                    BalanceTitle.Content = OldLanguage.Banlance();
                    ReportGetButton.Content = OldLanguage.Query();
                    Deposit.Content = deposit.ToString("0.00");
                    Withdraw.Content = withdraw.ToString("0.00");
                    Balance.Content = (deposit + withdraw).ToString("0.00");
                    Count.Content = count.ToString();

                    break;

                case ReportTypes.RECORD_GAME:
                    GameRecordResponse cardGameReocrds = response as GameRecordResponse;

                    foreach (ReportGameRecord record in cardGameReocrds.Records)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = id++;
                        dataRow["Computer"] = record.Name;
                        dataRow["Game"] = App.GameNames[record.GameId];
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow["Status"] = "Normal";
                        dataRow["Win"] = record.Paid;
                        dataRow["Lose"] = record.Bet;
                        dataRow["Tax"] = record.Tax;
                        dataRow["Balance"] = record.Balance;
                        dataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.COMPUTER_GAME_RECORD:
                    GameRecordResponse computerGameRecordResponse = response as GameRecordResponse;

                    foreach (ReportGameRecord record in computerGameRecordResponse.Records)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Sequence"] = id++;
                        dataRow["CardId"] = record.Name;
                        dataRow["Game"] = App.GameNames[record.GameId];
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(record.TimeStamp);
                        dataRow["Status"] = "Normal";
                        dataRow["Win"] = record.Paid;
                        dataRow["Lose"] = record.Bet;
                        dataRow["Tax"] = record.Tax;
                        dataRow["Balance"] = record.Balance;
                        dataTable.Rows.Add(dataRow);
                    }

                    break;

                case ReportTypes.OPERATOR_LOG:
                    GetOperatorLogResponse getOperatorLogResponse = response as GetOperatorLogResponse;

                    foreach (OperatorLog log in getOperatorLogResponse.Logs)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow["Operator"] = log.Operator;
                        dataRow["Operator Type"] = App.OperatorTypes[log.Type];
                        dataRow["Note"] = log.Data;
                        dataRow["Time"] = DateTimeUtil.ConvertTimeStampToTimeString(log.TimeStamp);
                        dataTable.Rows.Add(dataRow);
                    }

                    break;
            }

            dataTable.AcceptChanges();
            ReportDataGrid.ItemsSource = dataTable.AsDataView();
        }

        public ReportWindow(int reportType)
        {
            InitializeComponent();
            StartDateTimePicker.Value = DateTimeUtil.GetTodayStartDateTime();
            EndDateTimePicker.Value = DateTime.Now;
            type = reportType;
            dataTable = new DataTable();
            CountTitle.Content = OldLanguage.Count();

            switch (reportType)
            {
                case ReportTypes.REPORT_PROFIT:
                    PlayerLabel.Visibility = Visibility.Collapsed;
                    PlayerTextBox.Visibility = Visibility.Collapsed;
                    ComputerLabel.Visibility = Visibility.Visible;
                    ComputerComboBox.Visibility = Visibility.Visible;
                    ComputerComboBox.ItemsSource = App.AllComputerName;
                    ComputerComboBox.SelectedIndex = 0;
                    Title = OldLanguage.ComputerReport();
                    ComputerLabel.Content = App.MyLanguage.Computer();
                    dataTable.Columns.Add("Computer");
                    dataTable.Columns.Add("Money played");
                    dataTable.Columns.Add("Money tax");
                    dataTable.Columns.Add("Money won");
                    dataTable.Columns.Add("Profit");
                    dataTable.Columns.Add("Teso");
                    MyStatusBar.Visibility = Visibility.Collapsed;
                    ReportDataGrid.Height += MyStatusBar.Height;
                    ReadCardButton.Visibility = Visibility.Collapsed;
                    PlayerLabel.Content = OldLanguage.Computer();
                    ReportGetButton.Content = OldLanguage.Query();
                    break;

                case ReportTypes.RECORD_BALANCE:
                    dataTable.Columns.Add(OldLanguage.Sequence());
                    dataTable.Columns.Add(OldLanguage.CardId());
                    dataTable.Columns.Add(OldLanguage.Type());
                    dataTable.Columns.Add(OldLanguage.Amount());
                    dataTable.Columns.Add(OldLanguage.Banlance());
                    dataTable.Columns.Add(OldLanguage.Time());
                    break;

                case ReportTypes.RECORD_GAME:
                    dataTable.Columns.Add("Computer");
                    dataTable.Columns.Add("Game");
                    dataTable.Columns.Add("Bet");
                    dataTable.Columns.Add("Paid");
                    dataTable.Columns.Add("Tax");
                    dataTable.Columns.Add("Balance");
                    dataTable.Columns.Add("Time");
                    Title = "Card Game Record";
                    PlayerLabel.Content = OldLanguage.CardId();
                    break;

                case ReportTypes.COMPUTER_GAME_RECORD:
                    Title = "Computer Game Record";
                    PlayerLabel.Content = App.MyLanguage.Computer();
                    dataTable.Columns.Add(App.MyLanguage.CardId());
                    dataTable.Columns.Add("Game");
                    dataTable.Columns.Add("Bet");
                    dataTable.Columns.Add("Paid");
                    dataTable.Columns.Add("Tax");
                    dataTable.Columns.Add("Balance");
                    dataTable.Columns.Add("Time");
                    ComputerLabel.Visibility = Visibility.Visible;
                    ComputerComboBox.Visibility = Visibility.Visible;
                    ComputerComboBox.ItemsSource = App.AllComputerName;
                    ComputerComboBox.SelectedIndex = 0;
                    break;

                case ReportTypes.RECORD_FINANCE:                        
                    Title = App.MyLanguage.CashierInfo();
                    PlayerLabel.Content = App.MyLanguage.CardId();
                    ReadCardButton.Content = OldLanguage.ReadCard();
                    ReportGetButton.Content = App.MyLanguage.GetReport();
                    dataTable.Columns.Add("Sequence");
                    dataTable.Columns.Add("Card Id");
                    dataTable.Columns.Add("Type");
                    dataTable.Columns.Add("Money");
                    dataTable.Columns.Add("Time");                     
                    MyStatusBar.Visibility = Visibility.Collapsed;
                    StartDateTimePicker.Value = Convert.ToDateTime("2000-01-01 00:00:00");
                    EndDateTimePicker.Value = Convert.ToDateTime("2010-01-01 00:00:00");
                    StartDateTimePicker.Visibility = Visibility.Collapsed;
                    EndDateTimePicker.Visibility = Visibility.Collapsed;
                    PlayerLabel.Visibility = Visibility.Visible;
                    PlayerTextBox.Visibility = Visibility.Visible;
                    ReportDataGrid.Height += MyStatusBar.Height;
                    TimeLabel.Visibility = Visibility.Collapsed;                            
                    PrintButton.Visibility = Visibility.Collapsed;
                    ReadCardButton.Visibility = Visibility.Visible;
                    SperotrLabel.Visibility = Visibility.Collapsed;
                    break;

                case ReportTypes.CAISHER_LOG:
                    ReadCardButton.Visibility = Visibility.Collapsed;
                    PlayerLabel.Visibility = Visibility.Collapsed;
                    PlayerTextBox.Visibility = Visibility.Collapsed;
                    dataTable.Columns.Add("Sequence");
                    dataTable.Columns.Add(OldLanguage.CardId());
                    dataTable.Columns.Add(OldLanguage.Time());
                    dataTable.Columns.Add(OldLanguage.Type());
                    dataTable.Columns.Add(OldLanguage.Amount());
                    Title = OldLanguage.CashierLog();
                    DepositTile.Content = OldLanguage.Deposit();
                    WithdrawTitle.Content = OldLanguage.Withdraw();
                    BalanceTitle.Content = OldLanguage.Banlance();
                    CountTitle.Content = OldLanguage.Count();
                    ReportGetButton.Content = OldLanguage.Query();
                    PrintButton.Visibility = Visibility.Collapsed;
                    TimeLabel.Content = App.MyLanguage.Time();
                    break;

                case ReportTypes.OPERATOR_LOG:
                    Title = "Operator Log";                        
                    dataTable.Columns.Add("Operator");
                    dataTable.Columns.Add("Operator Type");
                    dataTable.Columns.Add("Note");
                    dataTable.Columns.Add("Time");
                    CashierLabel.Visibility = Visibility.Visible;
                    CashierComboBox.Visibility = Visibility.Visible;
                    PlayerLabel.Visibility = Visibility.Collapsed;
                    PlayerTextBox.Visibility = Visibility.Collapsed;
                    App.AllCashierName.Insert(0, new NameModel("All", ""));        
                    CashierComboBox.ItemsSource = App.AllCashierName;
                    CashierComboBox.SelectedIndex = 0;
                    break;
            }

            ReportDataGrid.ItemsSource = dataTable.AsDataView();
        }

        private void ReportGetButton_Click(object sender, RoutedEventArgs e)
        {
            dataTable.Clear();
            ReportDataGrid.ItemsSource = dataTable.AsDataView();

            string name = string.Empty;
            int startTime = DateTimeUtil.ConvertDateTimeToInt(StartDateTimePicker.Value.Value);
            int endTime = DateTimeUtil.ConvertDateTimeToInt(EndDateTimePicker.Value.Value);

            switch (type)
            {
                case ReportTypes.REPORT_PROFIT:
                    name = ComputerComboBox.SelectedValue.ToString();
                    RequestBll.GetProfitReport(name, startTime, endTime);
                    break;

                case ReportTypes.RECORD_FINANCE:
                    name = PlayerTextBox.Text;
                    RequestBll.ReportFinanceRecordGet(name, startTime, endTime);
                    break;

                case ReportTypes.RECORD_GAME:
                    name = PlayerTextBox.Text;
                    RequestBll.ReportCardGameRecordGet(name, startTime, endTime);
                    break;

                case ReportTypes.COMPUTER_GAME_RECORD:
                    name = ComputerComboBox.SelectedValue.ToString();
                    RequestBll.ReportComputerGameRecordGet(name, startTime, endTime);
                    break;

                case ReportTypes.CAISHER_LOG:
                    RequestBll.GetCasherLog(startTime, endTime);
                    break;

                case ReportTypes.OPERATOR_LOG:
                    string cashierName = CashierComboBox.SelectedValue.ToString();
                    RequestBll.GetOperatorLog(cashierName, startTime, endTime);
                    break;
            }
        }

        private void PlayerTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void PlayerTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!StringUtil.IsLetterOrDigit(e.Text))
            {
                e.Handled = true;
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            PrintUtil.PrintPreview(dataTable);
        }

        private void ReadCardButton_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder cardId = new StringBuilder();

            if (!NativeMethods.GetCardId(ref cardId))
            {
                return;
            }
            else
            {
                PlayerTextBox.Text = cardId.ToString();
            }
        }
    }
}
