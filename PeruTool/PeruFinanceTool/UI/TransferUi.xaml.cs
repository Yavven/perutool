﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeruToolClassLibrary.Response;
using PeruFinanceTool.BLL;
using YWCommonUtil;
using PeruToolClassLibrary;

namespace PeruFinanceTool.UI
{
    /// <summary>
    /// TransferUi.xaml 的交互逻辑
    /// </summary>
    public partial class TransferUi : UserControl
    {    
        int type;

        public TransferUi()
        {
            InitializeComponent();
        }

        public void SetType(int transferType)
        {
            if(IsDebug)
            {
                CardIdTextBox.IsReadOnly = false;
            }

            type = transferType;

            if(type == TransferTypes.DEPOSIT)
            {
                TitleLabel.Content = OldLanguage.DEPOSIT();
                TransferTypeLabel.Content = OldLanguage.DEPOSIT();
                DepositButton.Content = OldLanguage.submit();
            }
            else
            {
                TitleLabel.Content = OldLanguage.DRAW();
                TransferTypeLabel.Content = OldLanguage.DRAW();
                DepositButton.Content = OldLanguage.DrawSubmit();
            }

            CardIdLabel.Content = OldLanguage.CID();
            BalanceLabel.Content = OldLanguage.BALANCE();
            ReadCardButton.Content = OldLanguage.ReadCard();
            GetCardInfoButton.Content = App.MyLanguage.GetCardInfo();
            TodayMoneyLabel.Content = App.MyLanguage.TodayMoney();
        }

        public void OnBalanceMessageResponse(BalanceResponse response)
        {
            Balance.Content = response.Balance;
            TodayMoney.Content = response.TotalMoney;
        }

        public void OnTransferMessageResponse(TransferResponse response)
        {
            if(response.Flag == 0)
            {
                MessageBox.Show("Fail");
            }
            else
            {
                MessageBox.Show("Success");
            }

            Balance.Content = response.Balance;
        }

        private void DepositButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(CardIdTextBox.Text))
            {
                MessageBox.Show("Card ID is empty!");
                return;
            }

            if (string.IsNullOrEmpty(Amount.Text))
            {
                MessageBox.Show("Amount is empty!");
                return;
            }

            double amount = Convert.ToDouble(Amount.Text);


            if(type == TransferTypes.DEPOSIT)
            {
                RequestBll.Deposit(CardIdTextBox.Text, amount);
            }
            else
            {
                RequestBll.Withdraw(CardIdTextBox.Text, amount);
            }
        }

        private void WithdrawButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(CardIdTextBox.Text))
            {
                MessageBox.Show("Card ID is empty");
                return;
            }

            if (string.IsNullOrEmpty(Amount.Text))
            {
                MessageBox.Show("Amount is empty");
                return;
            }

            double amount = Convert.ToDouble(Amount.Text);

            if(amount <= 0 )
            {
                MessageBox.Show("Amount cannot be small than 0");
            }

            RequestBll.Withdraw(CardIdTextBox.Text, amount);
        }

        private void GetCardInfoButton_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(CardIdTextBox.Text))
            {
                MessageBox.Show("Card ID is empty");
                return;
            }

            RequestBll.CardBalance(CardIdTextBox.Text);
        }

        private void CardIdTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void CardIdTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsInteger(e.Text))
            {
                e.Handled = true;
            }
        }

        private void Amount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if(!StringUtil.IsNumber(e.Text))
            {
                e.Handled = true;
            }
        }

        private void Amount_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        public virtual bool IsDebug
        {
            get
            {
                #if (DEBUG)                                                        
                  return true;
                #else          
                   return false;
                #endif
            }
        }

        private void ReadCardButton_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder cardId = new StringBuilder();

            if (!NativeMethods.GetCardId(ref cardId))
            {
                return;
            }
            else
            {
                CardIdTextBox.Text = cardId.ToString();
            }
        }
    }
}
