﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PeruFinanceTool.BLL;
using PeruToolClassLibrary;
using PeruToolClassLibrary.Response;
using PeruFinanceTool.UI;

namespace PeruFinanceTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow
    {
        UserControl currentUi;

        public MainWindow()
        {
            InitializeComponent();
            ChangePasswordMenuItem.Header = OldLanguage.reset_pwd();
            DepositMenuItem.Header = OldLanguage.Deposit();
            WithdrawMenuItem.Header = OldLanguage.draw();
            ExitMenuItem.Header = OldLanguage.exit();
            CashierLogMenuItem.Header = OldLanguage.CashierLog();
            CashierInfoMenuItem.Header = App.MyLanguage.CashierInfo();
            GameLogMenuItem.Header = App.MyLanguage.GameHistory();
            ComputerGameRecordMenuItem.Header = App.MyLanguage.ByComputerName();
            CardGameRecordMenuItem.Header = App.MyLanguage.ByCardId();
            Title = App.MyLanguage.FinanceTool();
            ComputerReportMenuItem.Header = OldLanguage.ComputerReport();
            RequestBll.GetAllComputerName();
        }

        public void OnMessageResponse(int messageId, object response)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                switch(messageId)
                {
                    case FinanceToolMessageId.CARD_BALANCE:
                        MyTransferUi.OnBalanceMessageResponse(response as BalanceResponse);
                        break;

                    case FinanceToolMessageId.TRANSFER:
                        MyTransferUi.OnTransferMessageResponse(response as TransferResponse);
                        break;

                    case FinanceToolMessageId.FINANCE_RECORD:
                        App.FinanceRecordWindow.OnMessageResponse(response);
                        break;

                    case FinanceToolMessageId.CARD_GAME_RECORD:
                        App.CardGameRecordWindow.OnMessageResponse(response);
                        break;

                    case FinanceToolMessageId.COMPUTER_GAME_RECORD:
                        App.ComputerRecordWindow.OnMessageResponse(response);
                        break;

                    case FinanceToolMessageId.CHANGE_PASSOWRD:
                        CreateEditResponse changePasswordResponse = response as CreateEditResponse;

                        if(changePasswordResponse.IsSuccess())
                        {
                            MessageBox.Show("Success");
                        }
                        else
                        {
                            MessageBox.Show("Fail");
                        }

                        break;

                    case FinanceToolMessageId.CAISHER_LOG:
                        App.CashierLogWindow.OnMessageResponse(response);
                        break;


                    case FinanceToolMessageId.GET_OPERATOR_LOG:
                        App.OperatorLogWindow.OnMessageResponse(response);
                        break;

                    case FinanceToolMessageId.GET_ALL_CASHIER_NAME:
                        App.AllCashierName = (response as GetNameListResponse).Names;            
                        App.OperatorLogWindow = new ReportWindow(ReportTypes.OPERATOR_LOG);     
                        App.OperatorLogWindow.Show();
                        break;


                    case FinanceToolMessageId.GET_ALL_COMPUTER_NAME:
                        App.AllComputerName = (response as GetNameListResponse).Names;
                        break;

                    case FinanceToolMessageId.GET_PROFIT_REPROT:
                        App.ProfitReportWindow.OnMessageResponse(response as ReportProfitResponse);
                        break;
                }
            }));           
        }

        public void ShowUi(UserControl ui)
        {
            if (currentUi != null)
            {
                currentUi.Visibility = Visibility.Collapsed;
            }

            ui.Visibility = Visibility.Visible;
            currentUi = ui;
        }

        private void DepositMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MyTransferUi.SetType(TransferTypes.DEPOSIT);
            ShowUi(MyTransferUi);
        }

        private void BalanceRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.BalanceRecordWindow = new ReportWindow(ReportTypes.RECORD_BALANCE);
            App.BalanceRecordWindow.Show();
        }

        private void FinanceRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.FinanceRecordWindow = new ReportWindow(ReportTypes.RECORD_FINANCE);
            App.FinanceRecordWindow.Show();
        }

        private void CardGameRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.CardGameRecordWindow = new ReportWindow(ReportTypes.RECORD_GAME);
            App.CardGameRecordWindow.Show();
        }

        private void ComputerGameRecordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.ComputerRecordWindow = new ReportWindow(ReportTypes.COMPUTER_GAME_RECORD);
            App.ComputerRecordWindow.Show();
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void CashierLogMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.CashierLogWindow = new ReportWindow(ReportTypes.CAISHER_LOG);
            App.CashierLogWindow.Show();
        }

        private void ChangePasswordMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ChangetPasswordWindow changetPasswordWindow = new ChangetPasswordWindow();
            changetPasswordWindow.Show();
        }

        private void OperatorLogMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RequestBll.GetAllCashierName();
        }

        private void WithdrawMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MyTransferUi.SetType(TransferTypes.WITHDRAW);
            ShowUi(MyTransferUi);
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void CashierInfoMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.FinanceRecordWindow = new ReportWindow(ReportTypes.RECORD_FINANCE);
            App.FinanceRecordWindow.Show();
        }

        private void ComputerReportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.ProfitReportWindow = new ReportWindow(ReportTypes.REPORT_PROFIT);
            App.ProfitReportWindow.Show();
        }
    }
}
