﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// OverviewItem.xaml 的交互逻辑
    /// </summary>
    public partial class OverviewItem : UserControl
    {
        public OverviewItem()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return FirstLabel.Content.ToString(); }
            set { FirstLabel.Content = value; }
        }

        public string Time
        {
            get { return SecondLabel.Content.ToString(); }
            set { SecondLabel.Content = value; }
        }

        public string ButtonText
        {
            get { return MyButton.Content.ToString(); }
            set { MyButton.Content = value; }
        }

        public Button GetButton
        {
            get { return MyButton; }
        }

        public bool IsShowButton
        {
            get { return MyButton.Visibility == Visibility.Visible; }
            set
            {
                if (value)
                {
                    MyButton.Visibility = Visibility.Visible;
                }
                else
                {
                    MyButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        public void SetDeposit(string value)
        {
            Deposit.Content = value == "0" ? "--" : value;
        }

        public void SetWithdraws(string value)
        {
            Withdraws.Content = value == "0" ? "--" : value;
        }

        public void SetProfit(string value)
        {
            Profit.Content = value == "0" ? "--" : value;
        }

        public void SetJackpot(string value)
        {
            Jackpot.Content = value == "0" ? "--" : value;
        }
    }
}
