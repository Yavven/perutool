﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YwTextBox.xaml 的交互逻辑
    /// </summary>
    public partial class YWTextBoxNoInputMethod : UserControl
    {
        public YWTextBoxNoInputMethod()
        {
            InitializeComponent();

            this.getTextBox = MyTextBox;
        }

        #region 名称
        private string textBoxName;

        public string TextBoxName
        {
            get { return textBoxName; }
            set { textBoxName = value; }
        }
        #endregion

        #region 最小长度
        private int miniLength;

        public int MiniLength
        {
            get { return miniLength; }
            set { miniLength = value; }
        }
        #endregion

        #region 最大长度
        private int maxLength;

        public int MaxLength
        {
            get { return maxLength; }

            set
            {
                maxLength = value;
                MyTextBox.MaxLength = value;
            }
        }
        #endregion

        private TextBox getTextBox;

        public TextBox GetTextBox
        {
            get { return getTextBox; }
            set { getTextBox = value; }
        }

        #region 内容
        private string textBoxValue;

        public string TextBoxValue
        {
            get
            {
                textBoxValue = MyTextBox.Text;
                return textBoxValue;
            }

            set
            {
                textBoxValue = value;
                MyTextBox.Text = textBoxValue;
            }
        }
        #endregion

        #region 允许输入空格
        private bool isCanTypeSpace;

        public bool IsCanTypeSpace
        {
            get { return isCanTypeSpace; }
            set { isCanTypeSpace = value; }
        }
        #endregion

        #region 只允许数字
        private bool isNumberOnly;

        public bool IsNumberOnly
        {
            get { return isNumberOnly; }

            set
            {
                isNumberOnly = value;
            }
        }
        #endregion

        #region 只允许整数
        private bool isIntegerOnly;

        public bool IsIntegerOnly
        {
            get { return isIntegerOnly; }

            set
            {
                isIntegerOnly = value;
            }
        }
        #endregion

        #region 只允许数字和字幕
        private bool isNumberAndLetterOnly;

        public bool IsNumberAndLetterOnly
        {
            get { return isNumberAndLetterOnly; }
            set { isNumberAndLetterOnly = value; }
        }
        #endregion

        #region 只允许输入 IP
        private bool isOnlyIP;

        public bool IsOnlyIP
        {
            get { return isOnlyIP; }
            set { isOnlyIP = value; }
        }
        #endregion

        #region 只读
        private bool isReadOnly;

        public bool IsReadOnly
        {
            get { return isReadOnly; }

            set
            {
                isReadOnly = value;
                MyTextBox.IsReadOnly = value;
            }
        }
        #endregion

        #region 输入限制
        private void BaseTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            #region 屏蔽空格
            if (e.Key == Key.Space)
            {
                if(!IsCanTypeSpace)
                {
                    e.Handled = true;
                    return;
                }
            }
            #endregion

            #region 只允许数字和字母
            if (IsNumberAndLetterOnly)
            {
                if ((e.Key >= Key.A && e.Key <= Key.Z) || (e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
                {

                }
                else
                {
                    if (e.Key != Key.Back && e.Key != Key.BrowserBack && e.Key != Key.BrowserForward && e.Key != Key.Tab)
                    {
                        if(IsCanTypeSpace)
                        {
                            if(e.Key != Key.Space)
                            {
                                e.Handled = true;
                                MessageBox.Show("Please use only letters and numbers for your " + textBoxName + ".");
                            }
                        }
                        else
                        {
                            e.Handled = true;
                            MessageBox.Show("Please use only letters and numbers for your " + textBoxName + ".");
                        }
                    }
                }

                return;
            }
            #endregion

            #region 只允许数字
            if (IsNumberOnly)
            {
                if (e.Key != Key.Back && e.Key != Key.OemPeriod)
                {
                    if ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9))
                    {
                        e.Handled = true;
                        return;
                    }
                }
                else
                {
                    if (e.Key == Key.OemPeriod)
                    {
                        if (MyTextBox.Text.IndexOf('.') >= 0)
                        {
                            e.Handled = true;
                            return;
                        }

                        if (MyTextBox.SelectionStart == 0)
                        {
                            e.Handled = true;
                            return;
                        }
                    }
                }
            }
            #endregion

            #region 只允许整数
            if (IsIntegerOnly)
            {
                if (e.Key != Key.Back)
                {
                    if ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9))
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }
            #endregion
        }
        #endregion

        #region 调整控件大小
        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.WidthChanged)
            {
                MyTextBox.Width = BaseUserControl.Width;
            }

            if (e.HeightChanged)
            {
                MyTextBox.Height = BaseUserControl.Height;
            }
        }
        #endregion

        #region IP 输入限制
        private void MyTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsOnlyIP)
            {
                if (!string.IsNullOrEmpty((MyTextBox.Text)))
                {
                    int number = Convert.ToInt32(MyTextBox.Text);

                    if (number > 255)
                    {
                        MyTextBox.Text = "255";
                    }
                }
            }
        }
        #endregion
    }
}
