﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YwLabelWithTextBox.xaml 的交互逻辑
    /// </summary>
    public partial class YWLabelWithTextBox : UserControl
    {
        public YWLabelWithTextBox()
        {
            InitializeComponent();
        }

        #region 允许标点符号
        private bool isCanTypeSpace;

        public bool IsCanTypeSpace
        {
            get 
            {
                isCanTypeSpace = MyTextBox.IsCanTypeSpace;
                return isCanTypeSpace; 
            }

            set 
            {
                isCanTypeSpace = value;
                MyTextBox.IsCanTypeSpace = value;
            }
        }
        #endregion

        private string labelValue;

        public string LabelValue
        {
            get { return labelValue; }

            set 
            { 
                labelValue = value;
                MyLabel.Content = value;
                MyTextBox.TextBoxName = value;
            }
        }

        private string textBoxValue;

        public string TextBoxValue
        {
            get 
            {
                textBoxValue = MyTextBox.TextBoxValue;
                return textBoxValue;
            }

            set 
            { 
                textBoxValue = value;
                MyTextBox.TextBoxValue = value;
            }
        }

        #region 只允许数字
        private bool isNumberOnly;

        public bool IsNumberOnly
        {
            get { return isNumberOnly; }

            set
            {
                isNumberOnly = value;
                MyTextBox.IsNumberOnly = value;
            }
        }
        #endregion

        #region 只允许整数
        private bool isIntegerOnly;

        public bool IsIntegerOnly
        {
            get { return isIntegerOnly; }

            set
            {
                isIntegerOnly = value;
                MyTextBox.IsIntegerOnly = value;
            }
        }
        #endregion

        #region 只允许数字和字幕
        private bool isNumberAndLetterOnly;

        public bool IsNumberAndLetterOnly
        {
            get { return isNumberAndLetterOnly; }

            set 
            { 
                isNumberAndLetterOnly = value;
                MyTextBox.IsNumberAndLetterOnly = value;
            }
        }
        #endregion

        #region 只读
        private bool isReadOnly;

        public bool IsReadOnly
        {
            get { return isReadOnly; }

            set
            {
                isReadOnly = value;
                MyTextBox.IsReadOnly = value;
            }
        }
        #endregion

        #region 最大长度
        private int maxLength;

        public int MaxLength
        {
            get { return maxLength; }

            set
            {
                maxLength = value;
                MyTextBox.MaxLength = value;
            }
        }
        #endregion
    }
}
