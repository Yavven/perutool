﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YwPageLabel.xaml 的交互逻辑
    /// </summary>
    public partial class YWPageLabel : UserControl
    {
        public YWPageLabel()
        {
            InitializeComponent();
        }

        private int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
           
            set 
            { 
                currentPage = value;
                LabelCurrentPage.Content = value.ToString();
            }
        }

        private int maxPage;

        public int MaxPage
        {
            get { return maxPage; }
          
            set 
            { 
                maxPage = value;
                LabelMaxPage.Content = value.ToString();
            }
        }
    }
}
