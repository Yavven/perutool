﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YwTextBox.xaml 的交互逻辑
    /// </summary>
    public partial class YWTextBox : UserControl
    {
        public YWTextBox()
        {
            InitializeComponent();
        }

        #region 内容
        private string text;

        public string Text
        {
            get
            {
                text = BaseTextBox.Text;
                return text;
            }

            set
            {
                text = value;
                BaseTextBox.Text = text;
            }
        }
        #endregion

        #region 只允许数字
        private bool isNumberOnly;

        public bool IsNumberOnly
        {
            get { return isNumberOnly; }

            set
            {
                isNumberOnly = value;
            }
        }
        #endregion

        #region 只允许整数
        private bool isIntegerOnly;

        public bool IsIntegerOnly
        {
            get { return isIntegerOnly; }

            set
            {
                isIntegerOnly = value;
            }
        }
        #endregion

        #region 只读
        private bool isReadOnly;

        public bool IsReadOnly
        {
            get { return isReadOnly; }

            set
            {
                isReadOnly = value;
                BaseTextBox.IsReadOnly = value;
            }
        }
        #endregion

        #region 输入限制
        private void BaseTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
                return;
            }

            if (IsIntegerOnly)
            {
                if (e.Key != Key.Back)
                {
                    if ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9))
                    {
                        e.Handled = true;
                        return;
                    }
                }
            }
            else if (IsNumberOnly)
            {
                if (e.Key != Key.Back && e.Key != Key.OemPeriod)
                {
                    if ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9))
                    {
                        e.Handled = true;
                        return;
                    }
                }
                else
                {
                    if (e.Key == Key.OemPeriod)
                    {
                        if (BaseTextBox.Text.IndexOf('.') >= 0)
                        {
                            e.Handled = true;
                            return;
                        }

                        if (BaseTextBox.SelectionStart == 0)
                        {
                            e.Handled = true;
                            return;
                        }
                    }
                }
            }
        }
        #endregion

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(e.WidthChanged)
            {
                BaseTextBox.Width = BaseUserControl.Width;
            }

            if(e.HeightChanged)
            {
                BaseTextBox.Height = BaseUserControl.Height;
            }
        }
    }
}
