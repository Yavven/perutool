﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YWIPTextBox.xaml 的交互逻辑
    /// </summary>
    public partial class YWIPTextBox : UserControl
    {
        public YWIPTextBox()
        {
            InitializeComponent();
        }

        #region 获取 IP
        public string GetIp()
        {
            return IP1.TextBoxValue + "." + IP2.TextBoxValue + "." + IP3.TextBoxValue + "." + IP4.TextBoxValue;
        }
        #endregion

        #region 设置 IP
        public void SetIp(string ip)
        {
            string[] ips = ip.Split('.');

            if (ips.Length != 4)
            {
                return;
            }

            foreach(string s in ips)
            {

                if(!char.IsNumber(s[0]))
                {
                    return;
                }
            }

            IP1.TextBoxValue = ips[0];
            IP2.TextBoxValue = ips[1];
            IP3.TextBoxValue = ips[2];
            IP4.TextBoxValue = ips[3];
        }
        #endregion
    }
}
