﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using YWCommonUtil;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YwLabelWithDatePicker.xaml 的交互逻辑
    /// </summary>
    public partial class YWLabelWithDatePicker : UserControl
    {
        public YWLabelWithDatePicker()
        {
            InitializeComponent();

            MyDatePicker.SelectedDate = DateTime.Now;
        }

        #region Label 值
        private string labelValue;

        public string LabelValue
        {
            get { return labelValue; }

            set
            {
                labelValue = value;
                MyLabel.Content = value;
            }
        }
        #endregion

        #region 获取该日期开始时间戳
        public long GetStartTimeStampMillisecond()
        {
            if (MyDatePicker.SelectedDate == null)
            {
                return DateTimeUtil.GetTodayStartTimeStamp();
            }
            else
            {
                return DateTimeUtil.GetStartTimeStamp(MyDatePicker.SelectedDate.Value);
            }
        }

        public long GetStartTimeStampSecond()
        {
            if (MyDatePicker.SelectedDate == null)
            {
                return DateTimeUtil.GetTodayStartTimeStamp() / 1000;
            }
            else
            {
                return DateTimeUtil.GetStartTimeStamp(MyDatePicker.SelectedDate.Value) / 1000;
            }
        }
        #endregion

        #region 获取该日期
        public long GetDateLong()
        {
            string dateString = "";

            if(MyDatePicker.SelectedDate != null)
            {
                string year = MyDatePicker.SelectedDate.Value.Year.ToString();
                string month = MyDatePicker.SelectedDate.Value.Month.ToString();

                if (month.Length == 1)
                {
                    month = "0" + month;
                }

                string day = MyDatePicker.SelectedDate.Value.Day.ToString();

                if (day.Length == 1)
                {
                    day = "0" + day;
                }

                dateString = year + month + day;
            }
      

            if(dateString == string.Empty)
            {
                dateString = DateTime.Now.ToString("yyyyMMdd");
            }

            long dateLong = Convert.ToInt64(dateString);
            return dateLong;
        }
        #endregion

        #region 获取该日期结束时间戳
        public long GetEndTimeStampMillisecond()
        {
            if (MyDatePicker.SelectedDate == null)
            {
                return DateTimeUtil.GetTodayEndTimeStamp();
            }
            else
            {
                return DateTimeUtil.GetEndTimeStamp(MyDatePicker.SelectedDate.Value);
            }
        }

        public long GetEndTimeStampSecond()
        {
            if (MyDatePicker.SelectedDate == null)
            {
                return DateTimeUtil.GetTodayEndTimeStamp() / 1000;
            }
            else
            {
                return DateTimeUtil.GetEndTimeStamp(MyDatePicker.SelectedDate.Value) / 1000;
            }
        }
        #endregion
    }
}
