﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YwComboBoxWithLabel.xaml 的交互逻辑
    /// </summary>
    public partial class YWComboBoxWithLabel : UserControl
    {
        public YWComboBoxWithLabel()
        {
            InitializeComponent();
        }

        private string labelValue;

        public string LabelValue
        {
            get { return labelValue; }

            set
            {
                labelValue = value;
                MyLabel.Content = value;
            }
        }

        private int comboBoxIndex;

        public int ComboBoxIndex
        {
            get {
                comboBoxIndex = MyComboBox.SelectedIndex;
                return comboBoxIndex; 
            }

            set 
            { 
                comboBoxIndex = value;
                MyComboBox.SelectedIndex = value;
            }
        }

        private string comboBoxText;

        public string ComboBoxText
        {
            get 
            {
                comboBoxText = MyComboBox.Text;
                return comboBoxText;
            }

            set 
            { 
                comboBoxText = value;
                MyComboBox.Text = value;
            }
        }

        private object comboBoxItem;

        public object ComboBoxItem
        {
            get 
            {
                comboBoxItem = MyComboBox.SelectedItem;
                return comboBoxItem; 
            }

            set 
            { 
                comboBoxItem = value;
                MyComboBox.SelectedItem = value;
            }
        }

        private ComboBox comboBox;

        public ComboBox ComboBox
        {
            get
            {
                comboBox = MyComboBox;
                return comboBox; 
            }

            set 
            { 
                comboBox = value;
                MyComboBox = value;
            }
        }
    }
}
