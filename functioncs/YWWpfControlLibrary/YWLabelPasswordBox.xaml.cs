﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YWWpfControlLibrary
{
    /// <summary>
    /// YWLabelTextBox.xaml 的交互逻辑
    /// </summary>
    public partial class YWLabelPasswordBox : UserControl
    {
        public YWLabelPasswordBox()
        {
            InitializeComponent();
        }

        public string LabelValue
        {
            get { return MyLabel.Content.ToString(); }
            set { MyLabel.Content = value; }
        }

        public string PasswordBoxValue
        {
            get { return MyPasswordBox.Password; }
            set { MyPasswordBox.Password = value; }
        }

        private void MyUserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MyStackPanel.Width = MyUserControl.ActualWidth;
        }

        private void MyStackPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MyPasswordBox.Width = MyStackPanel.ActualWidth - MyLabel.ActualWidth;
        }
    }
}
