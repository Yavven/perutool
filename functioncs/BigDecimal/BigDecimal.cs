﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Numerics
{
    /// <summary>
    /// Supports arbitrarily long decimal numbers, with a ushort scale.
    /// This means it can supports any arbitrarily big number (as far as memory can hold it), with up to 65,535 digits after the decimal period.
    /// 
    /// The smallest representable number is 1E-65535
    /// 
    /// No underflow exceptions will occur. Data beyond the 65535th digit will be lost.
    /// </summary>
    public struct BigDecimal
    {
        private readonly BigInteger value;
        private readonly ushort scale;

        public BigDecimal(float value)
        {
            string str = value.ToString("R"); // "R" format string: Round-trip - A string that can round-trip to an identical number.
            this = Parse(str); // can assing to this because it's a struct
        }

        public BigDecimal(double value)
        {
            string str = value.ToString("R"); // "R" format string: Round-trip - A string that can round-trip to an identical number.
            this = Parse(str); // can assing to this because it's a struct
        }

        public BigDecimal(decimal value)
        {
            string str = value.ToString();
            this = Parse(str); // can assing to this because it's a struct
        }

        /// <summary>
        /// Initializes a BigDecimal with an integer value.
        /// </summary>
        public BigDecimal(long value)
            : this((BigInteger)value)
        {
            // this constructor is provided to prevent implicit casts from choosing casting to decimal types, which are slower
        }

        /// <summary>
        /// Initializes a BigDecimal with an integer value.
        /// </summary>
        public BigDecimal(ulong value)
            : this((BigInteger)value)
        {
            // this constructor is provided to prevent implicit casts from choosing casting to decimal types, which are slower
        }

        /// <summary>
        /// Initializes a BigDecimal with an integer value.
        /// </summary>
        public BigDecimal(BigInteger value)
            : this(value, 0)
        {
        }

        /// <summary>
        /// Initializes a BigDecimal with an integer value, and a scale.
        /// 
        /// The actual decimal value will be (value * 10^(-scale)).
        /// </summary>
        public BigDecimal(BigInteger value, ushort scale)
        {
            this.value = value;
            this.scale = scale;
        }

        /// <summary>
        /// Parses a (big) decimal from a string.
        /// </summary>
        /// <remarks>
        /// Number format: (given in pseudo-regex, no whitespace allowed)
        /// {+|-}? [0-9]* (\. [0-9]*)? (E {+|-}? [0-9]+)?
        /// All of the parts are optional, but there must be at least one digit, and if the 'E' (case insensitive) exists there must be at least one digit before and at least one digit after the E.
        /// 
        /// Two passes are made over the string; one to validate and split it into its parts, and one to actually parts the different parts.
        /// 
        /// The exponent part is limited to the int range; an overflow will be thrown for numbers above or below that range.
        /// The digits after the decimal point are limited to the ushort range, i.e. 65535 digits. An overflow will be thrown for more digits.
        /// 
        /// If (the nubmer of digits after the decimal point - the exponent) is less than -ushort.MAX_VALUE, then precision will be lost. (e.g. 1E-70000 == 0, and 123.456E-65536 == 120E65536)
        /// </remarks>
        /// <exception cref="ArgumentNullException">The given string is null</exception>
        /// <exception cref="FormatException">The number could not be parsed because it had a bad format or invalid characters</exception>
        /// <exception cref="OverflowException">One of the parts was over its limit: the exponent or the number of digits after the decimal point</exception>
        public static BigDecimal Parse(string str)
        {
            if (str == null || str == "")
                str = "0";
            //throw new ArgumentNullException("str", "BigDecimal.Parse: Cannot parse null");

            // first, we go over the string, separate it into parts.
            // At this stage, we create:
            //		valueBuilder: Contains a string that represents the value (should be parsed using BigInteger), e.g. for 1234.5678E-17, will contain "12345678".
            //		exponentBuilder: Is null if no exponent exists in the original string, or contains a string that represents that exponent otherwise, e.g. for 1234.5678E-17, will contain "-17".
            //		scale: Contains the scale from the original number (before the exponent was applied), e.g. for 1234.5678E-17, will contain 4.
            // The second stage should parse the results. In our example of 1234.5678E-17, the final result should be value=12345678 and scale=4-(-17)=21.
            ushort scale = 0;
            StringBuilder valueBuilder = new StringBuilder();
            StringBuilder exponentBuilder = null;


            ParseState state = ParseState.Start;

            // non-trivial things that are using in multiple cases
            Action<char> formatException = c => { throw new FormatException("BigDecimal.Parse: invalid character '" + c + "' in: " + str); };
            Action startExponent = () => { exponentBuilder = new StringBuilder(); state = ParseState.E; };
            foreach (char c in str)
            {
                switch (state)
                {
                    case ParseState.Start:
                        if (char.IsDigit(c) || c == '-' || c == '+')
                        {
                            state = ParseState.Integer;
                            valueBuilder.Append(c);
                        }
                        else if (c == '.')
                        {
                            state = ParseState.Decimal;
                        }
                        else
                        {
                            formatException(c);
                        }
                        break;

                    case ParseState.Integer:
                        if (char.IsDigit(c))
                        {
                            valueBuilder.Append(c);
                        }
                        else if (c == '.')
                        {
                            state = ParseState.Decimal;
                        }
                        else if (c == 'e' || c == 'E')
                        {
                            startExponent();
                        }
                        else
                        {
                            formatException(c);
                        }
                        break;

                    case ParseState.Decimal:
                        if (char.IsDigit(c))
                        {
                            // checked so that an overflow is thrown for too much precision
                            checked { scale++; }
                            valueBuilder.Append(c);
                        }
                        else if (c == 'e' || c == 'E')
                        {
                            startExponent();
                        }
                        else
                        {
                            formatException(c);
                        }
                        break;

                    case ParseState.E:
                        if (char.IsDigit(c) || c == '-' || c == '+')
                        {
                            state = ParseState.Exponent;
                            exponentBuilder.Append(c);
                        }
                        else
                        {
                            formatException(c);
                        }
                        break;

                    case ParseState.Exponent:
                        if (char.IsDigit(c))
                        {
                            exponentBuilder.Append(c);
                        }
                        else
                        {
                            formatException(c);
                        }
                        break;
                }
            }

            if (valueBuilder.Length == 0 ||
                (valueBuilder.Length == 1 && !char.IsDigit(valueBuilder[0])))
            {
                // the value doesn't have any digit (one character could be the sign)
                throw new FormatException("BigDecimal.Parse: string didn't contain a value: \"" + str + "\"");
            }

            if (exponentBuilder != null &&
                (exponentBuilder.Length == 0 ||
                (valueBuilder.Length == 1 && !char.IsDigit(valueBuilder[0]))))
            {
                // the scale builder exists but is empty, meaning there was an 'e' in the number, but no digits afterwards (one character could be the sign)
                throw new FormatException("BigDecimal.Parse: string contained an 'E' but no exponent value: \"" + str + "\"");
            }

            BigInteger value = BigInteger.Parse(valueBuilder.ToString());

            if (exponentBuilder == null)
            {
                // simple case with no exponent
            }
            else
            {

                // we need to correct the scale to match the given exponent
                // Note: The scale goes downwards (i.e. a large scale means more precision) while the exponent goes up (i.e. a large exponent means the number is larger)
                int exponent = int.Parse(exponentBuilder.ToString());
                if (exponent > 0)
                {
                    if (exponent <= scale)
                    {
                        // relatively simply case; decrease the scale by the exponent (e.g. 1.2e1 would have a scale of 1 and exponent of 1 resulting in 12 with scale of 0)
                        scale -= (ushort)exponent;
                    }
                    else
                    {
                        // scale would be negative; increase the actual value to represent that (remember, scale is only used for places after the decimal point)
                        exponent -= scale;
                        scale = 0;
                        value *= BigInteger.Pow(10, exponent);
                    }
                }
                else if (exponent < 0)
                {
                    exponent = (-exponent) + scale;
                    if (exponent <= ushort.MaxValue)
                    {
                        // agian, relatively simple case; increate the scale by the (negated) exponent (e.g. 1.2e-1 would have a scale of 1 and an exponent of -1 resulting in 12 with scale of 2, i.e. 0.12)
                        scale = (ushort)exponent;
                    }
                    else
                    {
                        // scale would overflow; lose some precision instead by dividing the value (integer truncating division)
                        scale = ushort.MaxValue;
                        value /= BigInteger.Pow(10, exponent - ushort.MaxValue);
                    }
                }
            }

            return new BigDecimal(value, scale);
        }

        private BigDecimal Upscale(ushort newScale)
        {
            if (newScale < scale)
                throw new InvalidOperationException("Cannot upscale a BigDecimal to a smaller scale!");

            return new BigDecimal(value * BigInteger.Pow(10, newScale - scale), newScale);
        }

        private static ushort SameScale(ref BigDecimal left, ref BigDecimal right)
        {
            var newScale = Math.Max(left.scale, right.scale);
            left = left.Upscale(newScale);
            right = right.Upscale(newScale);
            return newScale;
        }

        public static BigDecimal operator +(BigDecimal left, BigDecimal right)
        {
            var scale = SameScale(ref left, ref right);
            return new BigDecimal(left.value + right.value, scale);
        }

        public static BigDecimal operator -(BigDecimal left, BigDecimal right)
        {
            var scale = SameScale(ref left, ref right);
            return new BigDecimal(left.value - right.value, scale);
        }

        public static BigDecimal operator *(BigDecimal left, BigDecimal right)
        {
            var value = left.value * right.value;
            var scale = (int)left.scale + (int)right.scale;
            if (scale > ushort.MaxValue)
            {
                value /= BigInteger.Pow(10, scale - ushort.MaxValue);
                scale = ushort.MaxValue;
            }
            return new BigDecimal(value, (ushort)scale);
        }

        public static BigDecimal operator /(BigDecimal left, BigDecimal right)
        {
            if (right.value == 0)
            {
                return new BigDecimal(0.00);
            }

            //if (left.ToString().IndexOf(".") < 0)
            //{
            //    left = BigDecimal.Parse(left.ToString() + ".00");
            //}

            //if (right.ToString().IndexOf(".") < 0)
            //{
            //    right = BigDecimal.Parse(right.ToString() + ".00");
            //}

            var scale = Math.Max(left.scale, right.scale);
            string decimal1 = left.ToString();
            string decimal2 = right.ToString();

            BigDecimal value = BigDecimal.Parse(Division(decimal1, decimal2, scale));
            return value;
        }

        public static bool operator >(BigDecimal left, BigDecimal right)
        {
            BigDecimal result = left - right;

            if (result.ToString().IndexOf("-") != -1)
            {
                return false;
            }
            else
            {
                if (result.value == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool operator <(BigDecimal left, BigDecimal right)
        {
            BigDecimal result = left - right;

            if (result.ToString().IndexOf("-") != -1)
            {
                return true;
            }
            else
            {
                if (result.value == 0)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 计算除法，精度由CustomScale决定，超过部分直接截去。如果CustomScale小于0，表示精度由前两个参数的最长小数位决定。
        /// </summary>   
        private static string Division(string decimal1, string decimal2, int CustomScale)
        {
            //if (decimal1.IndexOf(".") < 0)
            //{
            //   decimal1 = decimal1 + ".00";
            //}

            //if (decimal2.IndexOf(".") < 0)
            //{
            //    decimal2 = decimal2 + ".00";
            //}

            //算法：把两个参数的小数部分补0对齐，整数部分确保被除数长度大于除数，
            //      放大被除数的要求精度的倍数，确保整数计算能保留希望的小数部分
            //      还原小数点，输出要求的精度，多余部分截断
            if (!isNumeric(decimal1)) throw new ArgumentException("Invalid argument");
            if (!isNumeric(decimal2)) throw new ArgumentException("Invalid argument");

            //判断负号
            int s1 = decimal1.IndexOf('-');
            if (s1 >= 0) decimal1 = decimal1.Replace("-", "");

            //判断负号
            int s2 = decimal2.IndexOf('-');
            if (s2 >= 0) decimal2 = decimal2.Replace("-", "");

            int sign = s1 + s2;     //=-2都是负数；=-1一正一负；=0都是正数；>0非法数字

            int decimalpartlength1 = 0;
            int decimalpartlength2 = 0;
            int integerpartlength1 = 0;
            int integerpartlength2 = 0;

            int maxscale = 0;
            BigInteger bi1;
            BigInteger bi2;

            //检查小数部分长度
            int pointIdx1 = decimal1.IndexOf('.');
            if (pointIdx1 >= 0)
            {
                decimalpartlength1 = decimal1.Length - pointIdx1 - 1;      //得到小数部分长度
                integerpartlength1 = pointIdx1 == 0 ? 1 : pointIdx1;       //得到整数部分长度,考虑小数点在第一位的情况
            }
            else
            {
                integerpartlength1 = decimal1.Length;                      //得到整数部分长度
            }

            //检查小数部分长度
            int pointIdx2 = decimal2.IndexOf('.');
            if (pointIdx2 >= 0)
            {
                decimalpartlength2 = decimal2.Length - pointIdx2 - 1;      //得到小数部分长度
                integerpartlength2 = pointIdx2 == 0 ? 1 : pointIdx2;       //得到整数部分长度,考虑小数点在第一位的情况
            }
            else
            {
                integerpartlength2 = decimal2.Length;                      //得到整数部分长度
            }

            decimal1 = decimal1.Replace(".", "");
            decimal2 = decimal2.Replace(".", "");

            //对齐小数部分
            if (decimalpartlength1 < decimalpartlength2)
            {
                decimal1 = decimal1 + new string('0', decimalpartlength2 - decimalpartlength1);
            }
            if (decimalpartlength2 < decimalpartlength1)
            {
                decimal2 = decimal2 + new string('0', decimalpartlength1 - decimalpartlength2);
            }

            bi1 = BigInteger.Parse(decimal1);
            bi2 = BigInteger.Parse(decimal2);

            if (bi2.ToString() == "0") throw new DivideByZeroException("DivideByZeroError");  //throw new DivideByZeroException("DivideByZeroError")


            int rightpos = 0;                                               //计算从右边数小数点的位置，用于还原小数点
            int pows = integerpartlength2 - integerpartlength1;
            if (pows >= 0)
            {
                bi1 = bi1 * BigInteger.Pow(10, pows + 1);                   //放大被除数，确保大于除数
                rightpos += pows + 1;
            }

            //确定小数位的精度
            maxscale = Math.Max(decimalpartlength1, decimalpartlength2);
            if (CustomScale < 0)
            {
                CustomScale = maxscale;                                     //CustomScale<0，表示精度由参数决定
            }
            else
            {
                maxscale = Math.Max(maxscale, CustomScale);                 //得到最大的小数位数
            }

            bi1 = bi1 * BigInteger.Pow(10, maxscale);             //放大被除数，确保整数除法之后，能保留小数部分
            rightpos += maxscale;

            BigInteger d = bi1 / bi2;                                       //注意整数除法的特点：会丢掉小数部分
            string result = d.ToString();

            if (rightpos > result.Length)
            {
                result = "0." + new string('0', rightpos - result.Length) + result;    //小数点后面的0补上，再还原小数点
            }
            else
            {
                result = result.Insert(result.Length - rightpos, ".");                 //还原小数点
                if (result.StartsWith(".")) result = "0" + result;                     //补上个位的0
            }

            //还原正负号 
            if (sign == -1) result = "-" + result;
            return result;

            ////超出精度截断
            //string tempResult = "0";
            //if (rightpos > CustomScale) tempResult = result.Substring(0, result.Length - (rightpos - CustomScale));

            ////还原正负号 
            //if (sign == -1) result = "-" + result;

            //if(tempResult == "0")
            //{
            //    return result;  
            //}
            //else
            //{
            //    return tempResult;
            //}
        }

        /// <summary>
        /// 判断字符串是不是数字：不能有两个小数点、负号只能在最前面、除了小数点和负号，只能是数字。
        /// </summary>        
        private static bool isNumeric(string strInput)
        {
            char[] ca = strInput.ToCharArray();
            int pointcount = 0;
            for (int i = 0; i < ca.Length; i++)
            {
                if ((ca[i] < '0' || ca[i] > '9') && ca[i] != '.' && ca[i] != '-') return false;
                if ((ca[i] == '-') && (i != 0)) return false;

                if (ca[i] == '.') pointcount++;
            }
            if (pointcount > 1) return false;
            return true;
        }

        private enum ParseState
        {
            /// <summary>
            /// First character
            /// </summary>
            Start,
            /// <summary>
            /// During the first part of the number, or right after the sign
            /// </summary>
            Integer,
            /// <summary>
            /// After the decimal point, or during a number in it
            /// </summary>
            Decimal,
            /// <summary>
            /// Right after the E
            /// </summary>
            E,
            /// <summary>
            /// After the E's sign, or during its number (i.e. the exponent).
            /// </summary>
            Exponent
        }

        public static implicit operator BigDecimal(sbyte value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(byte value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(short value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(ushort value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(int value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(uint value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(long value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(ulong value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(decimal value)
        {
            return new BigDecimal(value);
        }

        public static implicit operator BigDecimal(BigInteger value)
        {
            return new BigDecimal(value);
        }

        public override string ToString()
        {
            if (scale == 0)
            {
                //return value.ToString() + "."; // we add a decimal point at the end so we always know it's a decimal and not an integer.
                return value.ToString() + ".00";
            }
            else
            {
                // we need to add a decimal point at the right place
                string result = value.ToString();

                if (result.Length > scale)
                {
                    // the number is big enough to add the point inside it
                    if (value < 0 && result.Length == scale + 1)
                    {
                        string showValue = result.Insert(result.Length - scale, "0.");
                        int decimalIndex = showValue.IndexOf(".");

                        if (showValue.Length < decimalIndex + 3)
                        {
                            showValue += "0";
                        }

                        return showValue.Substring(0, decimalIndex + 3);
                    }
                    else
                    {
                        string showValue = result.Insert(result.Length - scale, ".");
                        int decimalIndex = showValue.IndexOf(".");

                        if (showValue.Length < decimalIndex + 3)
                        {
                            showValue += "0";
                        }

                        return showValue.Substring(0, decimalIndex + 3);
                    }
                }
                else
                {
                    // add a leading zero and a (potentially lot of, potentially none) zeros
                    string showValue = "0." + new String('0', scale - result.Length) + result;
                    int decimalIndex = showValue.IndexOf(".");

                    if (showValue.Length < decimalIndex + 3)
                    {
                        showValue += "0";
                    }

                    return showValue.Substring(0, decimalIndex + 3);
                }
            }
        }

        public string GetStringValue()
        {
            if (scale == 0)
            {
                //return value.ToString() + "."; // we add a decimal point at the end so we always know it's a decimal and not an integer.
                return value.ToString() + ".00";
            }
            else
            {
                // we need to add a decimal point at the right place
                string result = value.ToString();

                if (result.Length > scale)
                {
                    // the number is big enough to add the point inside it
                    if (value < 0 && result.Length == scale + 1)
                    {
                        return result.Insert(result.Length - scale, "0.");
                    }
                    else
                    {
                        return result.Insert(result.Length - scale, ".");
                    }
                }
                else
                {
                    // add a leading zero and a (potentially lot of, potentially none) zeros
                    return "0." + new String('0', scale - result.Length) + result;
                }
            }
        }

    }
}
