﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Microsoft.Office.Interop.Excel;

namespace YWCommonUtil
{
    public class UnsafeNativeMethods
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern int GetWindowThreadProcessId(IntPtr intPtr, out int ID);
        
        // 结束Excel进程
        public static void KillExcel(Application excelApp)
        {
            IntPtr intPtr = new IntPtr(excelApp.Hwnd);
            int ID = 0;
            GetWindowThreadProcessId(intPtr, out ID);
            Process process = Process.GetProcessById(ID);
            process.Kill();
        }
    }
}
