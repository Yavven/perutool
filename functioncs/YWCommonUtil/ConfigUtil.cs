﻿using System;
using System.Configuration;
using System.Xml;

namespace YWCommonUtil
{
    public class ConfigUtil
    {
        public static object GetAppSettingsValue(string key)
        {
            object value = ConfigurationManager.AppSettings[key];

            return value;
        }

        public static bool GetBoolValue(string key)
        {
            object value = ConfigurationManager.AppSettings[key];
            return Convert.ToBoolean(value);
        }

        public static string GetStringValue(string key)
        {
            object value = ConfigurationManager.AppSettings[key];
            return value.ToString();
        }

        public static int GetIntValue(string key)
        {
            object value = ConfigurationManager.AppSettings[key];
            return Convert.ToInt32(value);
        }

        static public void EditAppSettingsValue(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();
            configuration.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");
        }

        //第一个参数是xml文件中的add节点的value，第二个参数是add节点的key
        public static void SaveConfig(string ConnenctionString, string strKey)
        {
            XmlDocument doc = new XmlDocument();
            //获得配置文件的全路径
            string strFileName = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            // string  strFileName= AppDomain.CurrentDomain.BaseDirectory + "\\exe.config";
            doc.Load(strFileName);
            //找出名称为“add”的所有元素
            XmlNodeList nodes = doc.GetElementsByTagName("add");
            for (int i = 0; i < nodes.Count; i++)
            {
                //获得将当前元素的key属性
                XmlAttribute att = nodes[i].Attributes["key"];
                //根据元素的第一个属性来判断当前的元素是不是目标元素
                if (att.Value == strKey)
                {
                    //对目标元素中的第二个属性赋值
                    att = nodes[i].Attributes["value"];
                    att.Value = ConnenctionString;
                    break;
                }
            }
            //保存上面的修改
            doc.Save(strFileName);
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
