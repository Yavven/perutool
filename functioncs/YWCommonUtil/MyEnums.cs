﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil
{
    public class MessageStatus
    {
        public const string Faild = "0";
        public const string Success = "1";
    }

    public class TypeItem
    {
        string key = "";
        object value = "";

        public TypeItem()
        {

        }

        public TypeItem(string key, object value)
        {
            this.key = key;
            this.value = value;
        }

        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

    }

    public enum UserTypes
    {
        CasinoSuperAdmin = 1,
        CasinoAdmin,
        CasinoSupport,
        CasinoLv1Support,
        Agents,
        AgentsSupport,
    }

    public enum ActionTypes
    {
        Create,
        Edit,
    }

    public enum CodeStatus
    {
        Unused,
        Used,
    }

    public enum LoginStatus
    {
        Faild,
        Success,
        NoExist,
        AlreadyLogin,
        Locked,
        NoLogin,
    }

    public enum UserCreateStatus
    {
        Fail,
        Success,
        UserExist,
        UserInfoError,
    }

    public enum EditResult
    {
        Fail,
        Success,
        UserNoExist,
    }

    public enum ActionResults
    {
        Fail,
        Success,
    }

    public enum TransactionTypes
    {
        Purchase,
        Cashout,
        SsytemCredit,
        RollingCommision,
        SystemDebit,
    }

    public enum TransactionStatus
    {
        Pending,
        Complete,
    }
}
