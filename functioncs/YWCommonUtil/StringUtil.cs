﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using System.Text.RegularExpressions;

namespace YWCommonUtil
{
    public class StringUtil
    {
        static int rep = 0;

        public static string[] SplitString(string yourString, string separator)
        {
            return yourString.Split(new string[] { separator }, StringSplitOptions.None);
        }
        public static bool IsLetterOrDigit(char value)
        {
           return char.IsLetterOrDigit(value);
        }

        public static bool IsLetterOrDigit(string value)
        {
            char charValue = Convert.ToChar(value);
            return char.IsLetterOrDigit(charValue);
        }

        public static bool IsInteger(string value)
        {
            int i = 0;
            bool result = int.TryParse(value, out i); //i now = 108
            return result;
        }

        public static bool IsNumber(string value)
        {
            long number1 = 0;
            bool canConvert = long.TryParse(value, out number1);
            if (canConvert == true)
                return true;

            byte number2 = 0;
            canConvert = byte.TryParse(value, out number2);
            if (canConvert == true)
                return true;

            decimal number3 = 0;
            canConvert = decimal.TryParse(value, out number3);
            if (canConvert == true)
                return true;

            int number4 = 0;
            canConvert = int.TryParse(value, out number4);
            if (canConvert == true)
                return true;

            double number5 = 0;
            canConvert = double.TryParse(value, out number5);
            if (canConvert == true)
                return true;

            BigDecimal number6 = 0;
            try
            {
                number6 = BigDecimal.Parse(value);
                canConvert = true;
            }
            catch
            {
                canConvert = false;
            }
            if (canConvert == true)
                return true;

            return false;
        }

        public static string GenerateCheckCode(int codeCount)
        {
            string str = string.Empty;
            long num2 = DateTime.Now.Ticks + rep;
            rep++;
            Random random = new Random(((int)(((ulong)num2) & 0xffffffffL)) | ((int)(num2 >> rep)));
            for (int i = 0; i < codeCount; i++)
            {
                char ch;
                int num = random.Next();
                if ((num % 2) == 0)
                {
                    ch = (char)(0x30 + ((ushort)(num % 10)));
                }
                else
                {
                    ch = (char)(0x41 + ((ushort)(num % 0x1a)));
                }
                str = str + ch.ToString();
            }
            return str;
        }

        public static string StringToHexString(string value, Encoding myEncoding)
        {
            byte[] bytes = myEncoding.GetBytes(value); // 按照指定编码将 value 编程字节数组
            string result = string.Empty;

            for (int i = 0; i < bytes.Length; i++) //逐字节变为16进制字符，以%隔开
            {
                result += Convert.ToString(bytes[i], 16);
            }

            return result;
        }

        public static string GetHexString(byte[] bytes, Encoding myEncoding)
        {
            string result = string.Empty;

            for (int i = 0; i < bytes.Length; i++) //逐字节变为16进制字符，以%隔开
            {
                string temp = Convert.ToString(bytes[i], 16);

                if(temp.Length == 1)
                {
                    temp = "0" + temp;
                }

                result += temp;
            }

            return result;
        }

        /// <summary>
        /// 获得字符串中开始和结束字符串中间的字符串
        /// </summary>
        /// <param name="fullString">字符串</param>
        /// <param name="startString">开始</param>
        /// <param name="endString">结束</param>
        /// <returns></returns> 
        public static string GetString(string fullString, string startString, string endString)
        {
            Regex regex = new Regex("(?<=(" + startString + "))[.\\s\\S]*?(?=(" + endString + "))", RegexOptions.Multiline | RegexOptions.Singleline);
            return regex.Match(fullString).Value;
        }
    }
}
