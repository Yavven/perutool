﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Management;
using System.Management.Instrumentation;

namespace YWCommonUtil
{
    public class IPUtil
    {
        public static string GetRealIp()
        {
            ManagementClass mc_nac = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc_nac = mc_nac.GetInstances();
            string result = "";

            try
            {
                foreach (ManagementObject mo in moc_nac)
                {
                    string mServiceName = mo["ServiceName"] as string;
                    //过滤非真实网卡
                    if (!(bool)mo["IPEnabled"])
                    { continue; }
                    if (mServiceName.ToLower().Contains("vmnetadapter")
                    || mServiceName.ToLower().Contains("ppoe")
                    || mServiceName.ToLower().Contains("bthpan")
                    || mServiceName.ToLower().Contains("tapvpn")
                    || mServiceName.ToLower().Contains("ndisip")
                     || mServiceName.ToLower().Contains("sinforvnic"))
                    { continue; }

                    bool mDHCPEnabled = (bool)mo["IPEnabled"];
                    string mCaption = mo["Caption"] as string;
                    string mMACAddress = mo["MACAddress"] as string;
                    string[] mIPAddress = mo["IPAddress"] as string[];
                    string[] mIPSubnet = mo["IPSubnet"] as string[];
                    string[] mDefaultIPGateway = mo["DefaultIPGateway"] as string[];
                    string[] mDNSServerSearchOrder = mo["DNSServerSearchOrder"] as string[];
                    if (mIPAddress != null)
                    {
                        foreach (string ip in mIPAddress)
                        {
                            if (ip != "0.0.0.0")
                            {
                                result = ip;
                                return result;
                            }
                        }
                    }
                    mo.Dispose();
                }

                return result;
            }
            catch
            {
                return result;
            }
        }

        public static string GetLocalIp()
        {
            IPAddress[] ipAddressArray = Dns.GetHostAddresses(Dns.GetHostName());
            IPAddress ipAddress = Array.Find(ipAddressArray, a => a.AddressFamily == AddressFamily.InterNetwork);
            return ipAddress.ToString();
        }

        public static string GetIp()
        {
            String url = "http://yawen.me/getip.php";
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            byte[] responseData = webClient.UploadData(url, "POST", null);
            string scrString = Encoding.UTF8.GetString(responseData);
            return scrString;
        }
    }
}
