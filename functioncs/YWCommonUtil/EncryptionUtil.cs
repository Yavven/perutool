﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace YWCommonUtil
{
    public class EncryptionUtil
    {
        public static string SHA1Encrypt(string value, Encoding myEncoding)
        {
            string encryptionValue = "";
            SHA1 mySHA1 = SHA1.Create();
            byte[] bytes = myEncoding.GetBytes(value);
            mySHA1.ComputeHash(bytes);
            encryptionValue = Convert.ToBase64String(mySHA1.Hash);
            return encryptionValue;
        }

        public static byte[] GetSHA1Bytes(string value, Encoding myEncoding)
        {
            SHA1 mySHA1 = SHA1.Create();
            byte[] bytes = myEncoding.GetBytes(value);
            return mySHA1.ComputeHash(bytes);
        }
    }
}
