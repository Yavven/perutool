﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using DLLFullPrint;

namespace YWCommonUtil
{
    public class PrintUtil
    {
        public static void PrintPreview(DataTable dataTable)
        {
            DataSet ds = new DataSet();
            DataTable tempDataTable = dataTable.Copy();
            ds.Tables.Add(tempDataTable);
            MyDLL.TakeOver(ds);
        }
    }
}
