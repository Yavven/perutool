﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil
{
    public class ResponseFlag
    {
        public const int FAIL = 0;
        public const int SUCCESS = 1;

        public const int USER_ALREADY_LOGIN = 1001;
        public const int USER_ACCOUNT_ERROR = 1002;

        public const int ROOM_NAME_EXIST = 2001;

        public const int TOURNAMENT_NAME_EXIST = 3001;
    }
}
