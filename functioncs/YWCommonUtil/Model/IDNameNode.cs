﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil.Model
{
    public class IDName
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public IDName()
        {

        }

        public IDName(int id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}
