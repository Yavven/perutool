﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil.Model
{
    public class NameValue
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
