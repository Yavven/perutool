﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil.Model
{
    public class ExtraXmlNode2
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Info { get; set; }

        public ExtraXmlNode2(int id, string name, string info)
        {
            this.ID = id;
            this.Name = name;
            this.Info = info;
        }

        public ExtraXmlNode2()
        {

        }
    }
}
