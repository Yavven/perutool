﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil.Model
{
    public class SimpleXmlNode
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }
}
