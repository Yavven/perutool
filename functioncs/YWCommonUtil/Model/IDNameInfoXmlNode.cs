﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil.Model
{
    public class IDNameInfo
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Info { get; set; }

        public IDNameInfo()
        {

        }

        public IDNameInfo(int id, string name, string info)
        {
            this.ID = id;
            this.Name = name;
            this.Info = info;
        }
    }
}
