﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YWCommonUtil.Model
{
    public class ExtraXmlNode
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int Info { get; set; }
    }
}
