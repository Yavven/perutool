﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace YWCommonUtil
{
    public class ByteUtil
    {
        #region 添加数据
        static public byte[] AddValueToBigEndianBytes(bool value, byte[] bytes)
        {
            int offset = 0;
            byte valueByte = Convert.ToByte(value);
            byte[] newBytes = new byte[bytes.Length + 1];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            newBytes[offset] = valueByte;
            return newBytes;
        }

        public static void AddValueToBigEndianBytes(bool value, ref byte[] bytes)
        {
            int offset = 0;
            byte valueByte = Convert.ToByte(value);
            byte[] newBytes = new byte[bytes.Length + 1];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            newBytes[offset] = valueByte;
            bytes = newBytes;
        }

        static public byte[] AddValueToBigEndianBytes(byte value, byte[] bytes)
        {
            int offset = 0;
            byte[] newBytes = new byte[bytes.Length + 1];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            newBytes[offset] = value;
            return newBytes;
        }

        public static void AddValueToBigEndianBytes(byte value, ref byte[] bytes)
        {
            int offset = 0;
            byte[] newBytes = new byte[bytes.Length + 1];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            newBytes[offset] = value;
            bytes = newBytes;
        }

        public static byte[] AddValueToBigEndianBytes(int value, byte[] bytes)
        {
            int offset = 0;
            byte[] intBytes = BitConverter.GetBytes(value);
            Array.Reverse(intBytes);
            byte[] newBytes = new byte[bytes.Length + intBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            intBytes.CopyTo(newBytes, offset);
            return newBytes;
        }

        static public void AddValueToBigEndianBytes(int value, ref byte[] bytes)
        {
            int offset = 0;
            byte[] intBytes = BitConverter.GetBytes(value);
            Array.Reverse(intBytes);
            byte[] newBytes = new byte[bytes.Length + intBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            intBytes.CopyTo(newBytes, offset);
            bytes = newBytes;
        }

        static public byte[] AddValueToBigEndianBytes(long value, byte[] bytes)
        {
            int offset = 0;
            byte[] longBytes = BitConverter.GetBytes(value);
            Array.Reverse(longBytes);
            byte[] newBytes = new byte[bytes.Length + longBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            longBytes.CopyTo(newBytes, offset);
            return newBytes;
        }

        public static void AddValueToBigEndianBytes(long value, ref byte[] bytes)
        {
            int offset = 0;
            byte[] longBytes = BitConverter.GetBytes(value);
            Array.Reverse(longBytes);
            byte[] newBytes = new byte[bytes.Length + longBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            longBytes.CopyTo(newBytes, offset);
            bytes = newBytes;
        }

        public static void AddValueToBigEndianBytes(double value, ref byte[] bytes)
        {
            int offset = 0;
            byte[] doubleBytes = BitConverter.GetBytes(value);
            Array.Reverse(doubleBytes);
            byte[] newBytes = new byte[bytes.Length + doubleBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            doubleBytes.CopyTo(newBytes, offset);
            bytes = newBytes;
        }

        static public byte[] AddValueToBigEndianBytes(string value, byte[] bytes)
        {
            int offset = 0;
            byte[] stringBytes = Encoding.ASCII.GetBytes(value);
            byte[] stringLengthBytes = BitConverter.GetBytes(stringBytes.Length);
            Array.Reverse(stringLengthBytes);
            byte[] newBytes = new byte[bytes.Length + stringLengthBytes.Length + stringBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            stringLengthBytes.CopyTo(newBytes, offset);
            offset += stringLengthBytes.Length;
            stringBytes.CopyTo(newBytes, offset);
            return newBytes;
        }

        static public byte[] AddValueToBigEndianBytes2(string value, byte[] bytes)
        {
            int offset = 0;
            byte[] stringBytes = Encoding.Default.GetBytes(value);
            byte[] stringLengthBytes = BitConverter.GetBytes(stringBytes.Length);
            Array.Reverse(stringLengthBytes);
            byte[] newBytes = new byte[bytes.Length + stringLengthBytes.Length + stringBytes.Length];
            bytes.CopyTo(newBytes, offset);
            offset += bytes.Length;
            stringLengthBytes.CopyTo(newBytes, offset);
            offset += stringLengthBytes.Length;
            stringBytes.CopyTo(newBytes, offset);
            return newBytes;
        }

        public static void AddValueToBigEndianBytes(string value, ref byte[] bytes)
        {
            bytes = AddValueToBigEndianBytes(value, bytes);
        }

        public static void AddValueToBigEndianBytes2(string value, ref byte[] bytes)
        {
            bytes = AddValueToBigEndianBytes2(value, bytes);
        }
        #endregion

        #region 数据转换
        public static byte[] ConvertValueToBigEndianBytes(bool value)
        {
            byte[] boolBytes = new byte[1];
            boolBytes[0] = Convert.ToByte(value);

            return boolBytes;
        }

        static public byte[] ConvertValueToBigEndianBytes(int value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);

            return bytes;
        }

        static public byte[] ConvertValueToBigEndianBytes(long value)
        {
            int offset = 0;
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            byte[] newBytes = new byte[bytes.Length];
            bytes.CopyTo(newBytes, offset);

            return newBytes;
        }

        static public byte[] ConvertValueToBigEndianBytes(double value)
        {
            int offset = 0;
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            byte[] newBytes = new byte[bytes.Length];
            bytes.CopyTo(newBytes, offset);

            return newBytes;
        }

        public static byte[] ConvertValueToBigEndianBytes(string value)
        {
            int offset = 0;
            byte[] stringBytes = Encoding.Default.GetBytes(value);
            byte[] stringLengthBytes = BitConverter.GetBytes(stringBytes.Length);
            Array.Reverse(stringLengthBytes);
            byte[] newBytes = new byte[stringLengthBytes.Length + stringBytes.Length];
            stringLengthBytes.CopyTo(newBytes, offset);
            offset += stringLengthBytes.Length;
            stringBytes.CopyTo(newBytes, offset);

            return newBytes;
        }

        public static byte[] ConvertValueToBigEndianBytes2(string value)
        {
            int offset = 0;
            byte[] stringBytes = Encoding.Default.GetBytes(value);
            byte[] stringLengthBytes = BitConverter.GetBytes(stringBytes.Length);
            Array.Reverse(stringLengthBytes);
            byte[] newBytes = new byte[stringLengthBytes.Length + stringBytes.Length];
            stringLengthBytes.CopyTo(newBytes, offset);
            offset += stringLengthBytes.Length;
            stringBytes.CopyTo(newBytes, offset);
            return newBytes;
        }

        public static BigDecimal ConvertBigEndianBytesToBigDecimal(ref int offset, byte[] bytes)
        {
            int stringLength = ConvertBigEndianBytesToInt(ref offset, bytes);
            byte[] stringBytes = new byte[stringLength];
            Array.Copy(bytes, offset, stringBytes, 0, stringLength);
            offset += stringLength;
            string bigDecimalString = Encoding.ASCII.GetString(stringBytes);
            string[] bigDecimalStrings = bigDecimalString.Split('.');

            if (bigDecimalStrings.Length == 2)
            {
                ushort decimalLength = Convert.ToUInt16(bigDecimalStrings[1].Length);
                string tempbigDecimalString = bigDecimalString.Replace(".", "");
                BigDecimal bigDecimal = new BigDecimal(BigInteger.Parse(tempbigDecimalString), decimalLength);
                return bigDecimal;
            }
            else
            {
                BigDecimal bigDecimal = new BigDecimal(BigInteger.Parse(bigDecimalString), 0);
                return bigDecimal;
            }
        }

        public static bool ConvertBigEndianBytesToBool(ref int offset, byte[] bytes)
        {
            bool value = Convert.ToBoolean(bytes[offset]);

            offset++;

            return value;
        }

        public static double ConvertBigEndianBytesToDouble(ref int offset, byte[] bytes)
        {
            byte[] doubleBytes = new byte[8];
            Array.Copy(bytes, offset, doubleBytes, 0, 8);
            Array.Reverse(doubleBytes);
            offset += 8;
            return BitConverter.ToDouble(doubleBytes, 0);
        }

        public static int ConvertBigEndianBytesToInt(ref int offset, byte[] bytes)
        {
            byte[] intBytes = new byte[4];
            Array.Copy(bytes, offset, intBytes, 0, 4);
            Array.Reverse(intBytes);
            offset += 4;
            return BitConverter.ToInt32(intBytes, 0);
        }

        public static int ConvertBytesToInt(ref int offset, byte[] bytes)
        {
            byte[] intBytes = new byte[4];
            Array.Copy(bytes, offset, intBytes, 0, 4);
            offset += 4;
            return BitConverter.ToInt32(intBytes, 0);
        }

        public static long ConvertBigEndianBytesToLong(ref int offset, byte[] bytes)
        {
            byte[] longBytes = new byte[8];
            Array.Copy(bytes, offset, longBytes, 0, 8);
            Array.Reverse(longBytes);
            offset += 8;
            return BitConverter.ToInt64(longBytes, 0);
        }

        public static string ConvertBigEndianBytesToStringUTF8(ref int offset, byte[] bytes)
        {
            int stringLength = ConvertBigEndianBytesToInt(ref offset, bytes);
            byte[] stringBytes = new byte[stringLength];
            Array.Copy(bytes, offset, stringBytes, 0, stringLength);
            offset += stringLength;
            return Encoding.UTF8.GetString(stringBytes);
        }

        public static string ConvertBigEndianBytesToString(ref int offset, byte[] bytes)
        {
            int stringLength = ConvertBigEndianBytesToInt(ref offset, bytes);
            byte[] stringBytes = new byte[stringLength];
            Array.Copy(bytes, offset, stringBytes, 0, stringLength);
            offset += stringLength;
            return Encoding.Default.GetString(stringBytes).Replace("ÿÿÿ","");
        }
        #endregion
    }
}
