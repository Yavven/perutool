﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace YWCommonUtil
{
    public class FileUtil
    {
        public static string GetFileContent(string path)
        {
            StringBuilder content = new StringBuilder();
            StreamReader streamReader = new StreamReader(path, Encoding.Default);

            while (!streamReader.EndOfStream)
            {
                string readLine = streamReader.ReadLine();
                content.Append(readLine);
            }

            streamReader.Close();
            return content.ToString();
        }

        public static List<string> GetContentListFromTxt(string path)
        {
            return GetContentList(path + ".txt");
        }

        public static bool IsExists(string fileName)
        {
            return File.Exists(fileName);
        }

        public static List<string> GetContentList(string path)
        {
            List<string> contentList = new List<string>();

            if(path == "")
            {
                return contentList;
            }

            StreamReader streamReader = new StreamReader(path, Encoding.Default);

            while (!streamReader.EndOfStream)
            {
                string readLine = streamReader.ReadLine();
                contentList.Add(readLine);
            }

            streamReader.Close();
            return contentList;
        }

        public static void Write(string content, string fileName)
        {
            FileStream fileStream = new FileStream(fileName + ".txt", FileMode.Create);
            byte[] bytes = Encoding.Default.GetBytes(content);
            fileStream.Write(bytes, 0, bytes.Length);
            fileStream.Flush();
            fileStream.Close();
        }
    }
}
