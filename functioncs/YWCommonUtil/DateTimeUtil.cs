﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YWCommonUtil
{
    public class DateTimeUtil
    {
        public static string TIME_FORMAT_DATE_TIME = "yyyy-MM-dd hh:mm:ss";
        public static string TimeFormatForSend = "yyyy-MM-dd 00:00:00";
        public static string TimeFormatForShow = "yyyy-MM-dd hh:mm:ss";
        public static string TIME_FORMAT_DATE_SHORT = "yyyyMMdd";
        public const string TIME_FORMAT_INT = "yyyyMMddHHmmss";

        /// <summary>
        /// DateTime 转换时间戳
        /// </summary>
        /// <param name="time">DateTime</param>
        /// <returns>时间戳</returns>
        public static int ConvertDateTimeToInt(DateTime dateTime)
        {
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            return (int)(dateTime - startDateTime).TotalSeconds;
        }

        /// <summary>
        /// DateTime 转换时间戳 2
        /// </summary>
        /// <param name="time">DateTime</param>
        /// <returns>时间戳</returns>
        public static long ConvertDateTimeToLong(DateTime dateTime)
        {
            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);

            return (long)ts.TotalMilliseconds;
        }

        public static long ConvertDateTimeToEndTimeStamp(DateTime dateTime)
        {
            dateTime = Convert.ToDateTime(dateTime.ToString("yyyy-MM-dd 23:59:59"));
            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);

            return (long)ts.TotalMilliseconds;
        }


        public static int ConvertDateTimeToIntTimeStamp(DateTime dateTime)
        {
            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);

            return (int)(ts.TotalMilliseconds / 1000);
        }

        /// <summary>
        /// Java TimeStamp To Int
        /// </summary>
        /// <param name="time">DateTime</param>
        /// <returns>时间戳</returns>
        public static string ConvertJavaTimeStampToString(int timeStamp)
        {
            DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dateTime = baseTime.Add(new TimeSpan(timeStamp * TimeSpan.TicksPerMillisecond * 1000)).ToLocalTime();

            return dateTime.ToString(TIME_FORMAT_DATE_TIME);
        }

        public static string ConvertJavaTimeStampToString(long timeStamp)
        {
            DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dateTime = baseTime.Add(new TimeSpan(timeStamp * TimeSpan.TicksPerMillisecond)).ToLocalTime();

            return dateTime.ToString(TIME_FORMAT_DATE_TIME);
        }

        /// <summary>
        /// Java TimeStamp To String
        /// </summary>
        /// <param name="time">DateTime</param>
        /// <returns>时间戳</returns>
        public static string ConvertJavaTimeStampToString(string timeFormat, int timeStamp)
        {
            DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dateTime = baseTime.Add(new TimeSpan(timeStamp * TimeSpan.TicksPerMillisecond * 1000)).ToLocalTime();

            return dateTime.ToString(timeFormat);
        }

        public static string ConvertJavaTimeStampToString(string timeFormat, long timeStamp)
        {
            DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dateTime = baseTime.Add(new TimeSpan(timeStamp * TimeSpan.TicksPerMillisecond)).ToLocalTime();

            return dateTime.ToString(timeFormat);
        }

        /// <summary>
        /// string 转换时间戳
        /// </summary>
        /// <param name="stringTime">string</param>
        /// <returns>时间戳</returns>
        public static int ConvertStringTimeToInt(string stringTime)
        {
            DateTime dateTime = DateTime.Parse(stringTime);
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            return (int)(dateTime - startDateTime).TotalSeconds;
        }

        /// <summary>
        /// 时间戳格式转 DateTime
        /// </summary>
        /// <param name="timeStamp">时间戳</param>
        /// <returns>DateTime</returns>
        public static DateTime ConvertIntToDateTime(int timeStamp)
        {
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long ticks = long.Parse(timeStamp + "0000000");
            TimeSpan timeSpan = new TimeSpan(ticks);
            return startDateTime.Add(timeSpan);
        }

        public static DateTime ConvertTimeStampToDateTime(int timeStamp)
        {
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long ticks = long.Parse(timeStamp + "0000000");
            TimeSpan timeSpan = new TimeSpan(ticks);
            return startDateTime.Add(timeSpan);
        }

        public static DateTime ConvertLongToDateTime(long timeStamp)
        {
            if (timeStamp > 1000000000000)
            {
                timeStamp = timeStamp / 1000;
            }

            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan timeSpan = new TimeSpan(timeStamp * 10000000);

            return startDateTime.Add(timeSpan);
        }

        public static DateTime ConvertSecondToDateTime(long timeStamp)
        {
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan timeSpan = new TimeSpan(timeStamp * 10000000);

            return startDateTime.Add(timeSpan);
        }


        /// <summary>
        /// 时间戳格式转 DateTime
        /// </summary>
        /// <param name="timeStamp">时间戳</param>
        /// <returns>DateTime</returns>
        public static String ConvertTimeStampToTimeString(int timeStamp)
        {
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long ticks = long.Parse(timeStamp + "0000000");
            TimeSpan timeSpan = new TimeSpan(ticks);
            return startDateTime.Add(timeSpan).ToString();
        }

        public static String ConvertIntToTimeString(long timeStamp)
        {
            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            //long ticks = long.Parse(timeStamp);
            TimeSpan timeSpan = new TimeSpan(timeStamp);
            return startDateTime.Add(timeSpan).ToString();
        }

        public static string GetShortTimeString()
        {
            return DateTime.Now.ToString(TIME_FORMAT_DATE_SHORT);
        }

        public static String ConvertTimeStampToTimeString(long timeStamp)
        {
            if (timeStamp > 1000000000000)
            {
                timeStamp = timeStamp / 1000;
            }

            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan timeSpan = new TimeSpan(timeStamp * 10000000);
            return startDateTime.Add(timeSpan).ToString();
        }

        public static String ConvertTimeStampToTimeString(long timeStamp, string timeFormat)
        {
            if (timeStamp > 1000000000000)
            {
                timeStamp = timeStamp / 1000;
            }

            DateTime startDateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan timeSpan = new TimeSpan(timeStamp * 10000000);
            return startDateTime.Add(timeSpan).ToString(timeFormat);
        }

        public static long GetTodayStartTimeStamp()
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));

            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);

            return (long)ts.TotalMilliseconds;
        }

        public static DateTime GetTodayStartDateTime()
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));
            return dateTime;
        }

        public static long GetPastStartTimeStamp(int day)
        {
            DateTime pastDateTime = DateTime.Now.AddDays(day);
            DateTime dateTime = DateTime.Parse(pastDateTime.ToString("yyyy-MM-dd 00:00:00"));

            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);

            return (long)ts.TotalMilliseconds;
        }

        public static long GetStartTimeStamp(DateTime dateTime)
        {
            DateTime startDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd 00:00:00"));

            TimeSpan ts = new TimeSpan(startDateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);

            return (long)ts.TotalMilliseconds;
        }

        #region 获取开始时间
        public static DateTime GetStartDateTime(DateTime dateTime)
        {
            DateTime startDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd 00:00:00"));
            return startDateTime;
        }

        public static DateTime GetStartDateTime(int dayOffset)
        {
            DateTime dateTime = DateTime.Now.AddDays(dayOffset);
            DateTime startDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd 00:00:00"));
            return startDateTime;
        }

        public static long GetStartTimeStamp(int dayOffset)
        {
            DateTime dateTime = DateTime.Now.AddDays(dayOffset);
            string dateTimeString = dateTime.ToString("yyyy-MM-dd 00:00:00");
            DateTime startDateTime = DateTime.Parse(dateTimeString);
            return ConvertDateTimeToLong(startDateTime);
        }

        public static int GetIntStartDate(int dayOffset)
        {
            DateTime dateTime = DateTime.Now.AddDays(dayOffset);
            string dateTimeString = dateTime.ToString("yyyy-MM-dd 00:00:00");
            DateTime startDateTime = DateTime.Parse(dateTimeString);
            return ConvertDateTimeToIntDate(startDateTime);
        }
        #endregion

        #region 获取结束时间戳
        public static long GetEndTimeStamp(int dayOffset)
        {
            DateTime dateTime = DateTime.Now.AddDays(dayOffset);
            string dateTimeString = dateTime.ToString("yyyy-MM-dd 23:59:59");
            DateTime endDateTime = DateTime.Parse(dateTimeString);
            return ConvertDateTimeToLong(endDateTime);
        }

        public static int GetIntEndDate(int dayOffset)
        {
            DateTime dateTime = DateTime.Now.AddDays(dayOffset);
            string dateTimeString = dateTime.ToString("yyyy-MM-dd 23:59:59");
            DateTime endDateTime = DateTime.Parse(dateTimeString);
            return ConvertDateTimeToIntDate(endDateTime);
        }

        public static long GetTodayEndTimeStamp()
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd 23:59:59"));

            TimeSpan ts = new TimeSpan(dateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);
            return (long)ts.TotalMilliseconds;
        }

        public static long GetEndTimeStamp(DateTime dateTime)
        {
            DateTime endDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd 23:59:59"));

            TimeSpan ts = new TimeSpan(endDateTime.ToUniversalTime().Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);
            return (long)ts.TotalMilliseconds;
        }
        #endregion

        public static DateTime GetEndDateTime(DateTime dateTime)
        {
            DateTime endDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd 23:59:59"));
            return endDateTime;
        }

        public static DateTime GetEndDateTime(int dayOffset)
        {
            DateTime dateTime = DateTime.Now.AddDays(dayOffset);
            DateTime endDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-dd 23:59:59"));
            return endDateTime;
        }

        public static long GetDays(long timeStamp)
        {
            TimeSpan timeSpan = new TimeSpan(timeStamp * 10000);
            return (long)timeSpan.TotalDays;
        }

        #region 得到一周的周一和周日的日期
        public static DateTime GetMondayDate()
        {
            return GetMondayDateTime(DateTime.Now);
        }

        public static DateTime GetSundayStartDateTime(int offset)
        {
            DateTime sundayDateTime = GetSundayDateTime(DateTime.Now.AddDays(7 * offset));
            return GetStartDateTime(sundayDateTime);
        }

        public static DateTime GetSaturdayEndDateTime(int offset)
        {
            DateTime saturdayDateTime = GetSundayDateTime(DateTime.Now).AddDays(-1 + (7 * offset));
            return GetEndDateTime(saturdayDateTime);
        }

        public static long GetSundayDateTimeStamp()
        {
            return GetEndTimeStamp(GetSundayStartDateTime(0));
        }

        public static DateTime GetLastMondayDateTime()
        {
            DateTime mondayDateTime = GetMondayDateTime(DateTime.Now);
            return mondayDateTime.AddDays(-7);
        }

        public static long GetMondayTimeStamp(int weekOffset)
        {
            DateTime mondayDateTime = GetMondayDateTime(weekOffset);
            return ConvertDateTimeToLong(mondayDateTime);
        }

        public static long GetMondayStartTimeStamp(int weekOffset)
        {
            DateTime mondayDateTime = GetMondayDateTime(weekOffset);
            mondayDateTime = DateTime.Parse(mondayDateTime.ToString("yyyy-MM-dd 00:00:00"));
            return ConvertDateTimeToLong(mondayDateTime);
        }

        public static int GetMondayStartDate(int weekOffset)
        {
            DateTime mondayDateTime = GetMondayDateTime(weekOffset);
            mondayDateTime = DateTime.Parse(mondayDateTime.ToString("yyyy-MM-dd 00:00:00"));
            return ConvertDateTimeToIntDate(mondayDateTime);
        }

        public static DateTime GetFirstDayDateTime(string year)
        {
            DateTime firstDayDateTime = DateTime.Parse(DateTime.Now.ToString(year + "-01-01 00:00:00"));
            return firstDayDateTime;
        }

        public static DateTime GetLastDayDateTime(string year)
        {
            DateTime firstDayDateTime = DateTime.Parse(DateTime.Now.ToString(year + "-01-01 00:00:00"));
            return firstDayDateTime.AddYears(1).AddDays(-1);
        }

        public static DateTime GetFirstSundayDateTime(string year)
        {
            DateTime firstDayDateTime = DateTime.Parse(DateTime.Now.ToString(year + "-01-01 00:00:00"));
            DateTime firstSundayDateTime = GetSundayDateTime(firstDayDateTime);
            return firstSundayDateTime;
        }

        public static long GetLastMondayDate()
        {
            return Convert.ToInt64(GetLastMondayDateTime().ToString(TIME_FORMAT_DATE_SHORT));
        }

        public static long GetLastSundayDate()
        {
            return Convert.ToInt64(GetSundayDateTime().ToString(TIME_FORMAT_DATE_SHORT));
        }

        public static DateTime GetSundayDateTime()
        {
            return GetMondayDate().AddDays(-1);
        }

        public static long GetSundayTimeStamp(int weekOffset)
        {
            DateTime sundayDateTime = GetSundayDateTime(weekOffset);
            return ConvertDateTimeToLong(sundayDateTime);
        }

        public static long GetSundayEndTimeStamp(int weekOffset)
        {
            DateTime sundayDateTime = GetSundayDateTime(weekOffset);
            sundayDateTime = DateTime.Parse(sundayDateTime.ToString("yyyy-MM-dd 23:59:59"));
            return ConvertDateTimeToLong(sundayDateTime);
        }

        public static int GetSundayEndDate(int weekOffset)
        {
            DateTime sundayDateTime = GetSundayDateTime(weekOffset);
            sundayDateTime = DateTime.Parse(sundayDateTime.ToString("yyyy-MM-dd 23:59:59"));
            return ConvertDateTimeToIntDate(sundayDateTime);
        }

        public static DateTime GetMondayDateTime(DateTime someDate)
        {
            int i = someDate.DayOfWeek - DayOfWeek.Monday;

            if (i == -1) i = 6;// i值 > = 0 ，因为枚举原因，Sunday排在最前，此时Sunday-Monday=-1，必须+7=6。   
            TimeSpan ts = new TimeSpan(i, 0, 0, 0);
            return someDate.Subtract(ts);
        }

        public static DateTime GetMondayDateTime(int weekOffset)
        {
            return GetMondayDateTime(DateTime.Now.AddDays(weekOffset * 7));
        }

        public static DateTime GetSundayDateTime(DateTime someDate)
        {
            int i = someDate.DayOfWeek - DayOfWeek.Sunday;

            if (i != 0) i = 7 - i;// 因为枚举原因，Sunday排在最前，相减间隔要被7减。   
            TimeSpan ts = new TimeSpan(i, 0, 0, 0);
            return someDate.Add(ts);
        }

        public static DateTime GetSundayDateTime(int weekOffset)
        {
            return GetSundayDateTime(DateTime.Now.AddDays(weekOffset * 7));
        }
        #endregion

        public static long SubtractOneWeek(long timeStamp)
        {
            return timeStamp - 86400 * 7 * 1000;
        }

        public static long SubtractOneDay(long timeStamp)
        {
            return timeStamp - 86400 * 1000;
        }

        public static long ConverTimeStampToDate(long timeStamp)
        {
            return Convert.ToInt64(ConvertTimeStampToTimeString(timeStamp, TIME_FORMAT_DATE_SHORT));
        }

        public static int GetTodayDateIntFormat()
        {
            return Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
        }

        public static string GetTodayDateString()
        {
            return DateTime.Now.ToString("yyyy-MM-dd");
        }

        public static long GetLongDate(int dayOffset)
        {
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddDays(dayOffset);
            return Convert.ToInt64(dateTime.ToString("yyyyMMdd"));
        }

        public static int GetIntDate(int dayOffset)
        {
            DateTime dateTime = DateTime.Now;
            dateTime = dateTime.AddDays(dayOffset);
            return Convert.ToInt32(dateTime.ToString("yyyyMMdd"));
        }

        public static string GetDateTimeString(int dayOffset, string formatString)
        {
            return DateTime.Now.AddDays(dayOffset).ToString(formatString);
        }

        #region 获取上一个月第一天日期
        public static long GetMonthFirstDay()
        {
            return Convert.ToInt64(DateTime.Now.AddMonths(-1).ToString("yyyyMM01"));
        }

        public static DateTime GetMonthFirstDayDateTime(int monthOffset)
        {
            return DateTime.Parse(DateTime.Now.AddMonths(monthOffset).ToString("yyyyMM01"));
        }

        public static long GetMonthStartTimeStamp(int monthOffset)
        {
            DateTime dateTime = DateTime.Now.AddMonths(monthOffset);
            DateTime monthStartDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-01 00:00:00"));
            return ConvertDateTimeToLong(monthStartDateTime);
        }

        public static int GetMonthStartDate(int monthOffset)
        {
            DateTime dateTime = DateTime.Now.AddMonths(monthOffset);
            DateTime monthStartDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-01 00:00:00"));
            return ConvertDateTimeToIntDate(monthStartDateTime);
        }

        public static DateTime GetMonthStartDateTime(int monthOffset)
        {
            DateTime dateTime = DateTime.Now.AddMonths(monthOffset);
            DateTime monthStartDateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-01 00:00:00"));
            return monthStartDateTime;
        }
        #endregion

        #region 获取上一个月最后一天日期
        public static long GetMonthLastDay()
        {
            DateTime tempDateTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-01 00:00:00"));
            return Convert.ToInt64(tempDateTime.AddDays(-1).ToString("yyyyMMdd"));
        }

        public static DateTime GetMonthLastDayDateTime(int monthOffset)
        {
            DateTime dateTime = Convert.ToDateTime(DateTime.Now.AddMonths(monthOffset + 1).ToString("yyyy-MM-01"));
            return dateTime.AddDays(-1);
        }

        public static long GetMonthEndTimeStamp(int monthOffset)
        {
            DateTime dateTime = DateTime.Now.AddMonths(monthOffset);
            dateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-01 23:59:59"));
            return ConvertDateTimeToLong(dateTime.AddDays(-1));
        }

        public static int GetMonthEndDate(int monthOffset)
        {
            DateTime dateTime = DateTime.Now.AddMonths(monthOffset + 1);
            dateTime = DateTime.Parse(dateTime.ToString("yyyy-MM-01 23:59:59"));
            return ConvertDateTimeToIntDate(dateTime.AddDays(-1));
        }
        #endregion

        #region 获取上一年第一天日期
        public static long GetYearFirstDay()
        {
            return Convert.ToInt64(DateTime.Now.AddYears(-1).ToString("yyyy0101"));
        }

        public static long GetYearStartTimeStamp(int yearOffset)
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.AddYears(-1).ToString("yyyy-01-01 00:00:00"));
            return ConvertDateTimeToLong(dateTime);
        }

        public static int GetYearStartDate(int yearOffset)
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.AddYears(-1).ToString("yyyy-01-01 00:00:00"));
            return ConvertDateTimeToIntDate(dateTime);
        }
        #endregion

        public static string GetYearString(int yearOffset)
        {
            return DateTime.Now.AddYears(yearOffset).ToString("yyyy");
        }

        #region 获取上一年最后一天日期
        public static long GetYearLastDay()
        {
            DateTime tempDateTime = Convert.ToDateTime(DateTime.Now.ToString("yyyy-01-01 00:00:00"));
            return Convert.ToInt64(tempDateTime.AddDays(-1).ToString("yyyyMMdd"));
        }

        public static long GetYearEndTimeStamp(int yearOffset)
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 23:59:59"));
            return ConvertDateTimeToLong(dateTime.AddDays(-1));
        }

        public static int GetYearEndDate(int yearOffset)
        {
            DateTime dateTime = DateTime.Parse(DateTime.Now.ToString("yyyy-01-01 23:59:59"));
            return ConvertDateTimeToIntDate(dateTime.AddDays(-1));
        }
        #endregion

        #region 获取第一天日期
        public static long GetFirstDay()
        {
            return Convert.ToInt64(DateTime.MinValue.ToString("yyyyMMdd"));
        }

        public static long GetMinTimeStamp()
        {
            return ConvertDateTimeToLong(DateTime.MinValue);
        }
        #endregion

        #region 获取最后一天日期
        public static long GetLastDay()
        {
            return Convert.ToInt64(DateTime.Now.ToString("yyyyMMdd"));
        }
        #endregion

        public static long GetLastDay(int offset)
        {
            return Convert.ToInt64(DateTime.Now.AddDays(offset).ToString("yyyyMMdd"));
        }

        public static long ConvertDateTimeToDate(DateTime dateTime)
        {
            return Convert.ToInt64(dateTime.ToString("yyyyMMdd"));
        }


        public static int ConvertDateTimeToIntDate(DateTime dateTime)
        {
            return Convert.ToInt32(dateTime.ToString("yyyyMMdd"));
        }

        public static int ConvertDateTimeToMonthEndIntDate(DateTime dateTime)
        {
            dateTime = Convert.ToDateTime(dateTime.AddMonths(1).ToString("yyyy-MM-01 23:59:59"));
            return Convert.ToInt32(dateTime.AddDays(-1).ToString("yyyyMMdd"));
        }

        public static long GetNowTimeStamp()
        {
            return ConvertDateTimeToLong(DateTime.Now);
        }

        public static int GetNowDate()
        {
            return ConvertDateTimeToIntDate(DateTime.Now);
        }
    }
}
