﻿using System;
using System.Data;
using System.Reflection;

using Microsoft.Office.Interop.Excel;

namespace YWCommonUtil
{
    public class Excel
    {
        const int VERSION_2003 = 11;
        const int VERSION_2007 = 12;

        public static double GetExcelVersion()
        {
            Type appType = Type.GetTypeFromProgID("Excel.Application");

            if (appType == null)
            {
                return 0;
            }

            object excelApp = Activator.CreateInstance(appType);

            if (excelApp == null)
            {
                return 0;
            }

            object excelVersion = excelApp.GetType().InvokeMember("Version", BindingFlags.GetProperty, null, excelApp, null);
            double version = Convert.ToDouble(excelVersion.ToString());
            excelVersion = null;
            excelApp = null;
            appType = null;
            GC.Collect();

            return version;
        }

        public static bool DataTableToExcel(System.Data.DataTable dataTable, string fileName)
        {
            try
            {
                bool success = false;

                if (dataTable != null)
                {
                    double version = 0;
                    int rowCount = dataTable.Rows.Count;
                    int columnCount = dataTable.Columns.Count;
                    int rowIndex = 1;
                    int columnIndex = 0;
                    Application excelApp = null;
                    version = GetExcelVersion();
                    excelApp = new Application();
                    excelApp.DefaultFilePath = fileName;
                    excelApp.DisplayAlerts = true;
                    excelApp.SheetsInNewWorkbook = 1;
                    Workbook workbook = excelApp.Workbooks.Add(true);

                    // 将DataTable的列名导入Excel表第一行
                    foreach (DataColumn dataColumn in dataTable.Columns)
                    {
                        columnIndex++;
                        excelApp.Cells[rowIndex, columnIndex] = dataColumn.ColumnName;
                    }

                    // 将DataTable中的数据导入Excel中
                    for (int i = 0; i < rowCount; i++)
                    {
                        rowIndex++;
                        columnIndex = 0;

                        for (int j = 0; j < columnCount; j++)
                        {
                            columnIndex++;
                            excelApp.Cells[rowIndex, columnIndex] = dataTable.Rows[i][j].ToString();
                        }
                    }

                    try
                    {
                        if (version == VERSION_2003)
                        {
                            workbook.SaveAs(fileName, XlFileFormat.xlExcel7, null, null, false, false, XlSaveAsAccessMode.xlNoChange, null, null, null, null, null);
                        }
                        else if (version == VERSION_2007)
                        {
                            workbook.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, null, null, false, false, XlSaveAsAccessMode.xlNoChange, null, null, null, null, null);
                        }
                        else
                        {
                            workbook.SaveAs(fileName, XlFileFormat.xlWorkbookNormal, null, null, false, false, XlSaveAsAccessMode.xlNoChange, null, null, null, null, null);
                        }

                        UnsafeNativeMethods.KillExcel(excelApp);
                        success = true;
                    }
                    catch
                    {
                        UnsafeNativeMethods.KillExcel(excelApp);
                    }
                }

                return success;
            }
            catch
            {
                return false;
            }
        }
    }
}
