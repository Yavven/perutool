﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using YWCommonUtil.Model;

namespace YWCommonUtil
{
    public class XmlUtil
    {
        public static List<IDName> GetIDNameNodeList(string path,string rootNode)
        {
            List<IDName> xmlNodes = new List<IDName>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList xmlNodeList = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                IDName xmlNodeModel = new IDName();
                XmlElement xmlElement = (XmlElement)xmlNode;
                xmlNodeModel.ID = Convert.ToInt32(xmlElement.GetAttribute("ID"));
                xmlNodeModel.Name = Convert.ToString(xmlElement.GetAttribute("Name"));
                xmlNodes.Add(xmlNodeModel);
            }

            return xmlNodes;
        }

        public static List<IDNameInfo> GetIDNameInfoXmlNodeList(string path, string rootNode)
        {
            List<IDNameInfo> xmlNodes = new List<IDNameInfo>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList xmlNodeList = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                IDNameInfo xmlNodeModel = new IDNameInfo();
                XmlElement xmlElement = (XmlElement)xmlNode;
                xmlNodeModel.ID = Convert.ToInt32(xmlElement.GetAttribute("ID"));
                xmlNodeModel.Name = Convert.ToString(xmlElement.GetAttribute("Name"));
                xmlNodeModel.Info = Convert.ToString(xmlElement.GetAttribute("Info"));
                xmlNodes.Add(xmlNodeModel);
            }

            return xmlNodes;
        }

        public static Dictionary<int, IDName> GetIDNameDictionary(string path, string rootNode)
        {
            Dictionary<int, IDName> xmlNodes = new Dictionary<int, IDName>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList xmlNodeList = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                IDName xmlNodeModel = new IDName();
                XmlElement xmlElement = (XmlElement)xmlNode;
                xmlNodeModel.ID = Convert.ToInt32(xmlElement.GetAttribute("ID"));
                xmlNodeModel.Name = Convert.ToString(xmlElement.GetAttribute("Name"));
                xmlNodes.Add(xmlNodeModel.ID, xmlNodeModel);
            }

            return xmlNodes;
        }

        public static Dictionary<string, string> GetKeyValueDictionary(string path, string root, string key, string value)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(root);
            XmlNodeList xmlNodeList = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                XmlElement xmlElement = (XmlElement)xmlNode;
                dictionary.Add(xmlElement.GetAttribute(key), xmlElement.GetAttribute(value));
            }

            return dictionary;
        }


        public static Dictionary<int, IDNameInfo> GetIDNameInfoXmlNodeDictionary(string path, string rootNode)
        {
            Dictionary<int, IDNameInfo> xmlNodes = new Dictionary<int, IDNameInfo>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList xmlNodeList = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                IDNameInfo xmlNodeModel = new IDNameInfo();
                XmlElement xmlElement = (XmlElement)xmlNode;
                xmlNodeModel.ID = Convert.ToInt32(xmlElement.GetAttribute("ID"));
                xmlNodeModel.Name = Convert.ToString(xmlElement.GetAttribute("Name"));
                xmlNodeModel.Info = Convert.ToString(xmlElement.GetAttribute("Info"));
                xmlNodes.Add(xmlNodeModel.ID, xmlNodeModel);
            }

            return xmlNodes;
        }

        public static List<IDNameInfoXmlNode2> GetIDNameInfoXmlNode2List(string path, string rootNode)
        {
            List<IDNameInfoXmlNode2> xmlNodes = new List<IDNameInfoXmlNode2>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList xmlNodeList = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                IDNameInfoXmlNode2 xmlNodeModel = new IDNameInfoXmlNode2();
                XmlElement xmlElement = (XmlElement)xmlNode;
                xmlNodeModel.ID = Convert.ToInt32(xmlElement.GetAttribute("ID"));
                xmlNodeModel.Name = Convert.ToString(xmlElement.GetAttribute("Name"));
                xmlNodeModel.Info = Convert.ToInt32(xmlElement.GetAttribute("Info"));
                xmlNodes.Add(xmlNodeModel);
            }

            return xmlNodes;
        }

        public static List<NameValue> GetNameValueList(string path, string rootNode)
        {
            List<NameValue> xmlNodes = new List<NameValue>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(path);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList childNodes = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in childNodes)
            {
                NameValue nameValueXmlNode = new NameValue();
                XmlElement xmlElement = (XmlElement)xmlNode;
                nameValueXmlNode.Name = Convert.ToString(xmlElement.GetAttribute("name"));
                nameValueXmlNode.Value = Convert.ToString(xmlElement.GetAttribute("value"));
                xmlNodes.Add(nameValueXmlNode);
            }

            return xmlNodes;
        }

        public static List<NameValue> GetNameValueXmlNodeList(XmlReader xmlReader, string rootNode)
        {
            List<NameValue> xmlNodes = new List<NameValue>();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlReader);
            XmlNode rootXmlNode = xmlDocument.SelectSingleNode(rootNode);
            XmlNodeList childNodes = rootXmlNode.ChildNodes;

            foreach (XmlNode xmlNode in childNodes)
            {
                NameValue nameValueXmlNode = new NameValue();
                XmlElement xmlElement = (XmlElement)xmlNode;
                nameValueXmlNode.Name = Convert.ToString(xmlElement.GetAttribute("name"));
                nameValueXmlNode.Value = Convert.ToString(xmlElement.GetAttribute("value"));
                xmlNodes.Add(nameValueXmlNode);
            }

            return xmlNodes;
        }
    }
}
