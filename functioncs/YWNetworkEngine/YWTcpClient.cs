﻿using System;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using YWCommonUtil;

namespace YWNetworkEngine
{
    public class YWTcpClient:IDisposable
    {
        public bool IsConected = false;

        bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                eventWaitHandle.Dispose();
                MyTcpClient.Close();
            }

            disposed = true;
        }

        #region 配置信息

        #region 服务器端口
        private int serverPort;

        public int ServerPort
        {
            get { return serverPort; }
            set { serverPort = value; }
        }
        #endregion

        #region 服务器IP
        private string serverIP;

        public string ServerIp
        {
            get { return serverIP; }
            set { serverIP = value; }
        }
        #endregion

        #region 消息处理器
        private YWMessageHandler messageHandler;

        public YWMessageHandler MessageHandler
        {
            get { return messageHandler; }
            set { messageHandler = value; }
        }
        #endregion

        #endregion

        private int messageLenght;
        private const int BUFFER_SIZE = 8192000;
        private byte[] readBuffer;
        private EventWaitHandle eventWaitHandle;
        private NetworkStream networkStream;
        private StringBuilder stringBuilder;
        public TcpClient MyTcpClient;


        public YWTcpClient()
        {
            readBuffer = new byte[BUFFER_SIZE];
            eventWaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
            stringBuilder = new StringBuilder();
        }

        public void SetHostnameAndPort(string hostname, int port)
        {
            this.serverIP = hostname;
            this.serverPort = port;
        }

        #region 检查服务器状态
        public virtual void CheckServerStatus()
        {


        }
        #endregion

        #region 连接服务器
        public void Connect()
        {
            try
            {
                MyTcpClient = new TcpClient(serverIP, serverPort);
                networkStream = MyTcpClient.GetStream();
                startReceive();
                IsConected = true;
            }
            catch
            {
                MessageHandler.ProcessErrorMessage("can not connect server");
            }
        }
        #endregion

        #region 开始接收数据
        private void startReceive()
        {
            if (networkStream.CanRead)
            {
                networkStream.BeginRead(readBuffer, 0, readBuffer.Length, readCallBack, networkStream);
                eventWaitHandle.WaitOne();
            }
        }
        #endregion

        #region 发送消息
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="bytes">数据（字节数组）</param>
        public bool Send(byte[] bytes)
        {
            try
            {
                networkStream.BeginWrite(bytes, 0, bytes.Length, writeCallback, null);
                return true;
            }
            catch
            {
                messageHandler.OnServerDisconnect();
                return false;
            }
        }
        #endregion

        #region 消息发送完毕
        /// <summary>
        /// 消息发送完毕
        /// </summary>
        /// <param name="iAsyncResult"></param>
        private void writeCallback(IAsyncResult iAsyncResult)
        {
            networkStream.EndWrite(iAsyncResult);
        }
        #endregion

        #region 消息接收完毕
        /// <summary>
        /// 消息接收完毕
        /// </summary>
        /// <param name="iAsyncResult"></param>
        private void readCallBack(IAsyncResult iAsyncResult)
        {
            NetworkStream myNetworkStream = null;

            try
            {
                myNetworkStream = (NetworkStream)iAsyncResult.AsyncState;

                if (myNetworkStream != null && myNetworkStream.CanRead)
                {
                    int count = 0;

                    if (MyTcpClient.Connected == false)
                    {
                        return;
                    }

                    int tempOffset = 0;
                    int tempMessageLenght = ByteUtil.ConvertBigEndianBytesToInt(ref tempOffset, readBuffer);

                    if (messageLenght == 0)
                    {
                        messageLenght = tempMessageLenght;
                    }

                    count = myNetworkStream.EndRead(iAsyncResult);

                    if(count == 0)
                    {
                        messageHandler.OnServerDisconnect();
                        return;
                    }

                    for (int i = 0; i < count; i++)
                    {
                        stringBuilder.Append(readBuffer[i]);

                        if (i != count - 1)
                        {
                            stringBuilder.Append(',');
                        }
                    }

                    readBuffer = new byte[BUFFER_SIZE];
                    messageLenght -= count;

                    if (messageLenght > 0)
                    {
                        stringBuilder.Append(',');
                        myNetworkStream.BeginRead(readBuffer, 0, readBuffer.Length, new AsyncCallback(readCallBack), myNetworkStream);
                        return;
                    }

                    messageLenght = 0;
                    eventWaitHandle.Set();

                    if (stringBuilder.Length != 0)
                    {
                        MessageHandler.ProcessReceivedMessag(stringBuilder);
                        stringBuilder.Clear();
                        stringBuilder.Length = 0;
                    }

                    readBuffer = new byte[BUFFER_SIZE];
                    myNetworkStream.BeginRead(readBuffer, 0, readBuffer.Length, new AsyncCallback(readCallBack), myNetworkStream);
                    eventWaitHandle.WaitOne();
                }
            }
            catch
            {
                stringBuilder.Clear();
                stringBuilder.Length = 0;
                readBuffer = new byte[BUFFER_SIZE];
                myNetworkStream.BeginRead(readBuffer, 0, readBuffer.Length, new AsyncCallback(readCallBack), myNetworkStream);
                eventWaitHandle.WaitOne();
                MessageHandler.OnServerDisconnect();
            }       
        }
        #endregion
    }
}
