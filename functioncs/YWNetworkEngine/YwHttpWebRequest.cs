﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace YWNetworkEngine
{
    public class YWHttpWebRequest
    {
        public string Post(string url, Dictionary<string, string> parameters, ref CookieContainer yourCookieContainer)
        {
            string parameterString = string.Empty;

            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                parameterString += parameter.Key + "=" + parameter.Value + "&";
            }

            parameterString = parameterString.Substring(0, parameterString.Length - 1);
            byte[] parametersBytes = Encoding.ASCII.GetBytes(parameterString);
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = parametersBytes.Length;
            string reciveString;

            if (yourCookieContainer != null)
            {
                httpWebRequest.CookieContainer = yourCookieContainer;
            }

            using (Stream requestStream = httpWebRequest.GetRequestStream())
            {
                requestStream.Write(parametersBytes, 0, parametersBytes.Length);
            }

            using (WebResponse webResponse = httpWebRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    StreamReader streamReader = new StreamReader(stream);
                    reciveString = streamReader.ReadToEnd();
                }
            }

            return reciveString;
        }
    }
}
