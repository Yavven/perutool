﻿using System.Text;

namespace YWNetworkEngine
{
    public class YWMessageHandler
    {        
        #region 处理接收到的消息
        public virtual void ProcessReceivedMessag(StringBuilder message)
        {

        }
        #endregion

        #region 处理错误消息
        public virtual void ProcessErrorMessage(string message)
        {

        }
        #endregion

        #region 处理连接断开错误
        public virtual void OnServerDisconnect()
        {

        }
        #endregion

        public virtual void OnServerConnect()
        {

        }
    }
}
