﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using YWNetworkEngine.Request;
using YWNetworkEngine.Response;
using System.Threading;

namespace YWNetworkEngine
{
    public class YWJsonHttpWebRequest
    {
        List<JsonRequest> requests;
        YWJsonMessageHandler handler;

        public YWJsonHttpWebRequest(YWJsonMessageHandler handler)
        {
            this.requests = new List<JsonRequest>();
            this.handler = handler;
        }

        public bool AddJsonRequest(JsonRequest request)
        {
            if (!requests.Contains(request))
            {
                requests.Add(request);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Start()
        {
            while (true)
            {
                if (requests.Count > 0)
                {
                    JsonRequest request = requests[0];
                    string receivedJson = "";

                    if (request != null)
                    {
                        receivedJson = Post(request);
                        JsonReponse response = new JsonReponse();
                        response.Api = request.Api;
                        response.Json = receivedJson;

                        if (receivedJson != "")
                        {
                            handler.OnMessageResponse(response);
                        }
                    }

                    requests.Remove(request);
                }

                Thread.Sleep(100);
            }
        }

        #region 发送 Post 请求
        public string Post(JsonRequest request)
        {
            byte[] parameters = Encoding.ASCII.GetBytes(request.Json);
            string host = "http://" + request.Host;
            string requestUriString = host + request.Api;
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(requestUriString);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = parameters.Length;
            string reciveString;

            try
            {
                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(parameters, 0, parameters.Length);
                }

                using (WebResponse webResponse = httpWebRequest.GetResponse())
                {
                    Stream stream = webResponse.GetResponseStream();
                    StreamReader streamReader = new StreamReader(stream);
                    reciveString = streamReader.ReadToEnd();
                }

                return reciveString;
            }
            catch (Exception e)
            {
                handler.ShowErrorMessage(e.Message);
                return "";
            }
        }
        #endregion
    }
}
