﻿using System.Text;
using YWNetworkEngine.Response;

namespace YWNetworkEngine
{
    public class YWJsonMessageHandler
    {        
        #region 处理接收到的消息
        public virtual void OnMessageResponse(JsonReponse response)
        {

        }
        #endregion

        #region 处理错误消息
        public virtual void ShowErrorMessage(string message)
        {

        }
        #endregion

        #region 处理连接断开错误
        public virtual void OnServerDisconnect()
        {

        }
        #endregion
    }
}
